        NAME    EVENTQUEUE

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 Event Queue                                ;
;                             Event Queue Functions                          ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains functions that are necessary to communicate over serial.
; The functions included are:
;       EventQueueInit      - initializes the EventQueue to a word queue. (PUBLIC)
;       EnqueueEvent        - enqueues an event (EventID in AL, EventArgument
;                             in AH) in the EventQueue. (PUBLIC)
;       DequeueEvent        - dequeues an event from the EventQueue. (PUBLIC)
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision
;     12/9/15   Dennis Shim     updated comments, fixed zf return in Dequeue
;     12/9/15   Dennis Shim     updated to work using a shared folder

; local include files
$INCLUDE(../SHARED/GENERAL.INC) ;contains general constants and addresses
$INCLUDE(../SHARED/QUEUE.INC) 	;contains queue constants

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE    SEGMENT PUBLIC 'CODE'



        ASSUME  CS:CGROUP, DS:DGROUP
        
        ; external functions/tables
        EXTRN   QueueInit:NEAR      ;initializes the queue
        EXTRN   QueueEmpty:NEAR     ;sets zero flag if queue is empty
        EXTRN   QueueFull:NEAR      ;sets zero flag if queue is full
        EXTRN   Dequeue:NEAR        ;dequeues from the queue, returns in AX
        EXTRN   Enqueue:NEAR        ;enqueues value passed in AL
        EXTRN   SetHardResetFlag:NEAR;set if system needs to be reset

; EventQueueInit
;
; Description:        Initializes the EventQueue by calling QueueInit. EventQueue
;                     is initialized as a queue of words with size QUEUE_LENGTH.
;
; Operational         
; Description:        Move address of EventQueue to SI and TRUE to BL to initialize
;                     EventQueue as a word queue.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   EventQueue - DS - word queue of event key and value pairs
;                                       that will be processed
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/19/15  Dennis Shim     initial revision

EventQueueInit      PROC        NEAR
                    PUBLIC      EventQueueInit

        LEA     SI, EventQueue          ;pass the address of the queue in SI
        MOV     BL, TRUE                ;pass TRUE to to make the size words
        CALL    QueueInit               ;initialize EventQueue by calling
                                        ;  QueueInit(SI, BL)

        RET

EventQueueInit      ENDP


; EnqueueEvent
;
; Description:        Enqueues an event to the EventQueue. Events should be passed
;                     as arguments with the Event ID in AH and Event Argument in
;                     AL. If the EventQueue is full, there is a serious problem
;                     so the system is hard reset.
;
; Operational         
; Description:        If the queue is full, hard reset. Otherwise, enqueues an
;                     event in EventQueue. 
;
; Arguments:          EventID - AH - ID of the type of event (constant)
;                     EventArgument - AL - argument associated with the event
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   EventQueue - DS - word queue of event key and value pairs
;                                       that will be processed (w)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     If the EventQueue is full sets the hard reset flag so the
;                     system is reset.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision

EnqueueEvent        PROC        NEAR
                    PUBLIC      EnqueueEvent

        PUSH    SI                      ;preserve register

CheckEventQFull:                        ;if the EventQueue is full, there is a
                                        ;  serious issue, so we should reset system
        LEA     SI, EventQueue          ;pass the address of the queue as argument
        CALL    QueueFull               ;check if queue is full
        ;JZ     EventQFull              ;EventQueue is full, hard reset
        JNZ     EventQHasSpace          ;EventQueue has space, enqueue the event

EventQFull:                             ;need to hard reset because there is no
                                        ;  space in the EventQueue
        CALL    SetHardResetFlag
        JMP     EnqueueEventEnd         ;and return

EventQHasSpace:                         ;there is space in the EventQueue, so
                                        ;  we can enqueue an event
        CALL    Enqueue                 ;call enqueue with arguments EventID and
                                        ;  EventArgument passed in AH and AL
                                        ;NOTE: this call will not block because
                                        ;  we already checked that there is
                                        ;  space in the EventQueue
        ;JMP    EnqueueEventEnd         ;and return

EnqueueEventEnd:
        POP     SI                      ;restore register
 
        RET

EnqueueEvent        ENDP

; DequeueEvent
;
; Description:        Dequeues an event from the EventQueue, and returns the
;                     EventID in AH and EventArgument in AL. Only dequeues if
;                     the EventQueue is not empty (does not block). If the
;                     EventQueue was empty, will return with zero flag set.
;
; Operational         
; Description:        If the queue is not empty, dequeue from EventQueue and
;                     return the EventID and EventArgument.
;
; Arguments:          None.
;
; Return Values:      EventID - AH - ID of the type of event (constant)
;                     EventArgument - AL - argument associated with the event
;                     EventQueueEmpty - zf - set if the queue was empty
; Global Variables:   None.
; Shared Variables:   EventQueue - DS - queue that contains the events that will
;                                       be processed
;                   
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  AX, flags.
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision

DequeueEvent        PROC        NEAR
                    PUBLIC      DequeueEvent

        PUSH    SI                      ;preserve register

CheckEventQEmpty:                       ;if the EventQueue is empty, don't dequeue
                                        ;  an event
        LEA     SI, EventQueue          ;pass the address of the queue as argument
        CALL    QueueEmpty              ;check if queue is empty
        JZ      EnqueueEventEnd         ;EventQueue is empty, just return
        ;JNZ    EventQHasEvent          ;EventQueue has event, dequeue the event

EventQHasEvent:                         ;there is an event in the EventQueue, so
                                        ;  we can dequeue an event
        CALL    Dequeue                 ;call dequeue and get EventID and
                                        ;  EventArgument in AH and AL
                                        ;NOTE: this call will not block because
                                        ;  we already checked that there is
                                        ;  an event in the EventQueue
        XOR     SI, SI                  ;guarantee that zero flag is reset
        ADD     SI, 1

DequeueEventEnd:
        POP     SI                      ;restore register
 
        RET

DequeueEvent        ENDP

CODE    ENDS

;the data segment

DATA    SEGMENT PUBLIC  'DATA'

EventQueue          QUEUE       <>      ;initializes the EventQueue, which will
                                        ;  be a word queue that holds events

DATA    ENDS

        END