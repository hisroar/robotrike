        NAME    INT2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                     Int2                                   ;
;                                Int2 Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains functions that initialize the INT2 and handle the interrupts
; that it generates. The functions included are:
;       Int2EventHandler        - The event handler for the INT 2 interrupt.
;                                 Handles the interrupts sent by the serial chip.
;                                 (PUBLIC)
;       InitInt2                - Initializes INT 2 to send interrupts from the
;                                 serial chip. (PUBLIC)
;       InstallInt2EH           - Writes the address of the Int2EventHandler at
;                                 the INT 2 interrupt in the IVC. (PUBLIC)
;
; Revision History:
;     11/19/15  Dennis Shim      initial revision

; local include file
$INCLUDE(../SHARED/GENERAL.INC) ;contains general constants and addresses
$INCLUDE(../SHARED/INT2.INC)    ;contains the addresses and values necessary to 
                                ;  initialize INT2
$INCLUDE(../SHARED/INTERRPT.INC);contains the addresses and values necessary to 
                                ;  get interrupts from the processor
CGROUP  GROUP   CODE


CODE    SEGMENT PUBLIC 'CODE'

; external functions
        EXTRN   SerialEventHandler:NEAR ;called when an INT2 interrupt comes from
                                        ;  the serial chip

        ASSUME  CS:CGROUP


; Int2EventHandler
;
; Description:       This procedure is the event handler for the timer
;                    interrupt. It calls the SerialEventHandler, which
;                    handles the INT2 interrupt using the serial chip.
;                    It sends a Int2EOI when done.
;
; Operation:         Preserve the registers. Call SerialEventHandler to
;                    handle the INT2 interrupt. Send a INT2 EOI and return.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            An EOI is sent to the interrupt controller.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
;
; Revision History:
;     11/19/15      Dennis Shim     Initial revision.

Int2EventHandler        PROC    NEAR
                        PUBLIC  Int2EventHandler

        PUSH    AX                      ;preserve the registers
        PUSH    DX

        CALL    SerialEventHandler      ;handle the INT2 interrupt

EndInt2EventHandler:                    ;done taking care of the INT2
                                        ;send the EOI to the interrupt controller
        MOV     DX, INTCtrlrEOI         ;get the address of the controller
        MOV     AX, Int2EOI             ;get the value of the EOI specific to INT2
        OUT     DX, AL                  ;output the EOI to the interrupt controller

        POP     DX                      ;restore the registers
        POP     AX


        IRET                            ;and return (Event Handlers end with IRET not RET)

Int2EventHandler         ENDP

; InitInt2
;
; Description:       Initialize the 80188 INT2. INT2 is initialized so that the
;                    serial functions can communicate with the serial chip. The
;                    interrupt controller is also initialized to allow the INT2
;                    interrupts.
;
; Operation:         The appropriate value is written to the INT2 control
;                    register in the PCB. Finally, the interrupt controller is
;                    setup to accept INT2 interrupts and any pending
;                    interrupts are cleared by sending a Int2EOI to the
;                    interrupt controller.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            Outputs an Int2EOI to the interrupt contorller.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: AX, DX
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision
;     11/13/15  Dennis Shim      updated for motor

InitInt2        PROC    NEAR
                PUBLIC  InitInt2
                                ;initialize INT2

        MOV     DX, Int2Ctrl    ;get the address of the Timer0 control register
        MOV     AX, Int2CtrlVal ;setup the control register using the correct
        OUT     DX, AL          ;  control value.

        MOV     DX, INTCtrlrEOI ;get the address of the EOI controller
        MOV     AX, Int2EOI     ;send a INT 2 EOI (to clear out controller)
        OUT     DX, AL


        RET                     ;done so return

InitInt2         ENDP


; InstallInt2EH
;
; Description:       Install the event handler for the INT2 interrupt.
;
; Operation:         Writes the address of the INT2 event handler to the
;                    appropriate interrupt vector.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: flags, AX, ES
;
; Revision History:
;     11/19/15  Dennis Shim      initial revision

InstallInt2EH       PROC    NEAR
                    PUBLIC  InstallInt2EH

        XOR     AX, AX          ;clear ES (interrupt vectors are in segment 0)
        MOV     ES, AX
                                ;store the vector
                                ;the offset goes in the lower two bytes of the
                                ;  vector, and the segment goes in the upper two
                                ;  bytes of the vector.
        MOV     ES: WORD PTR (IVEC_SIZE * Int2Vec), OFFSET(Int2EventHandler)
        MOV     ES: WORD PTR (IVEC_SIZE * Int2Vec + IVEC_HALF_SIZE), SEG(Int2EventHandler)


        RET                     ;all done, return


InstallInt2EH       ENDP

CODE    ENDS

        END
