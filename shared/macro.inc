$NOLIST


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   MACRO.INC                                ;
;                         		  Useful Macros                        		 ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains generally useful macros.  The macros included are:
; 	CLR 			- clears a register
; 	SETBIT 			- sets a bit in a register
; 	CLRBIT 			- clears a bit in a register
; 	COMBIT 			- take the complement of a bit in a register
; 	TESTBIT 		- tests a bit in a register (zf set if 0, otherwise reset)
; 	XLATW 			- looks up a word in a table
; 	READPCB 		- reads a value from a PCB register and returns it in AX
;   WRITEPCB 		- writes a value to a PCB register
;
; Revision History:
;    11/19/15  Dennis Shim 		initial revision
; 	 11/22/15  Dennis Shim 		added additional macros

; local include files
;   none


; CLR
;
; Description:      	This macro clears the register reg (sets it to zero).
; 						Assumed that reg is not a segment register.
;
; Operation:        	Set reg to zero.
;
; Arguments:        	reg - register to clear
;
; Return Values: 		None.
;
; Registers Changed:	reg, flags
;
; Revision History:
;     11/22/15  Dennis Shim     initial revision

%*DEFINE(CLR(reg))   (
 		XOR 	%reg, %reg 			;zeroes the reg
)

; SETBIT
;
; Description:      	This macro sets a bit in the register reg. It is assumed
; 						reg is not a segment register.
;
; Operation:        	Set the bit at bit in reg by ORing with the correct bit
; 						value.
;
; Arguments:        	reg - register to modify
; 						bit - bit to set (0-indexed)
;
; Return Values: 		None.
;
; Registers Changed:	reg, flags
;
; Revision History:
;     11/22/15  Dennis Shim     initial revision

%*DEFINE(SETBIT(reg, bit))   (
 		OR 		%reg, 1 SHL %bit 		;sets the bit at position bit in reg
 										;  by shifting a 1 to the correct position
 										;  and ORing, which turns on that bit
)

; CLRBIT
;
; Description:      	This macro clears a bit in the register reg. It is assumed
; 						reg is not a segment register.
;
; Operation:        	Clear the bit at bit in reg by ANDing with the appropriate
; 						bit mask.
;
; Arguments:        	reg - register to modify
;	 					bit - bit to clear (0-indexed)
;
; Return Values: 		None.
;
; Registers Changed:	reg, flags
;
; Revision History:
;     11/22/15  Dennis Shim     initial revision

%*DEFINE(CLRBIT(reg, bit))   (
		AND 	%reg, NOT(1 SHL %bit)	;clears the bit at position bit in reg
										;  by shifting a 1 to the correct position
										;  and taking its complement, which
										;  preserves all other values in the
										;  register when ANDed
)

; COMBIT
;
; Description:      	This macro complements a bit in register reg. It is assumed
; 					  	reg is not a segment register.
;
; Operation:        	Take the complement of the bit by XORing with the
; 						appropriate bit sequence.
;
; Arguments:        	reg - register to modify
; 						bit - bit to complement (0-indexed)
;
; Return Values: 		None.
;
; Registers Changed:	reg, flags
;
; Revision History:
;     11/22/15  Dennis Shim     initial revision

%*DEFINE(COMBIT(reg, bit))   (
		XOR 	%reg, 1 SHL %bit 		;takes the complement of the bit at
										;  position bit in reg by shifting a 1 to
										;  the correct position and XORing, which
										;  takes the complement of that bit and
										;  leaves all other bits the same
)

; TESTBIT
;
; Description:      	This macro sets zero flag based on a bit in the register
; 						reg. It is assumed that reg is not a segment register.
;
; Operation:        	Test the bit at the register by TESTing with the appropriate
; 						bit sequence.
;
; Arguments:        	reg - register to test
; 						bit - bit to test
;
; Return Values: 		zero flag - set if the bit is 0, reset if the bit is 1
;
; Registers Changed:	flags
;
; Revision History:
;     11/22/15  Dennis Shim     initial revision

%*DEFINE(TESTBIT(reg, bit))   (
		TEST 	%reg, 1 SHL %bit 		;tests the bit at the position bit in reg
										;  by shifting a 1 to the correct position
										;  and TESTing, which results in all other
										;  bits cleared, and the bit at position
										;  bit cleared if it is 0 (zf set)
										;  otherwise zf not set
)


; XLATW
;
; Description:      	This macro looks up a value in a word table.
;
; Operation:        	Multiply the index by two because we have a word table.
; 						Get the value from the table and put it into AX.
;
; Arguments:        	index - AX - index of the item in the table to get.
;                   	table - BX - address of the word table to get the item from.
;
; Return Values: 		value - AX - value of the item at table[index]
;
; Registers Changed:	AX, flags
;
; Revision History:
;     11/19/15  Dennis Shim     initial revision

%*DEFINE(XLATW)   (
		PUSH 	SI 						;preserve register
        MOV     SI, AX 		            ;setup index for lookup
        SHL 	SI, 1 					;multiply index by two because it is a 
        								;  word table
        MOV     AX, WORD PTR [BX + SI] 	;get the item at the index in the table
        POP 	SI
)

; XLATW (override)
;
; Description:      	This macro looks up a value in a word table.
;
; Operation:        	Multiply the index by two because we have a word table.
; 						Get the value from the table and put it into AX.
;
; Arguments:        	index - AX - index of the item in the table to get.
;                   	table - the word table to get the item from (override).
;
; Return Values: 		value - AX - value of the item at table[index]
;
; Registers Changed:	AX, flags
;
; Revision History:
;     11/19/15  Dennis Shim     initial revision

%*DEFINE(XLATW(table))   (
		PUSH 	SI 						;preserve register
        MOV     SI, AX 		            ;setup index for lookup
        SHL 	SI, 1 					;multiply index by two because it is a 
        								;  word table
        MOV     AX, WORD PTR %table[SI]	;get the item at the index in the table
        POP 	SI
)

; READPCB
;
; Description:      	This macro reads the PCB register at address addr and
; 						returns the value in AX.
;
; Operation:        	Put the address in DX. Read from the address, and return
; 						the value in AX.
;
; Arguments:        	addr - PCB address to read from
;
; Return Values: 		val - AX - value read from the address
;
; Registers Changed:	AX
;
; Revision History:
;     11/22/15  Dennis Shim     initial revision

%*DEFINE(READPCB(addr))   (
 		PUSH 	DX 				;preserve register
 		MOV 	DX, %addr 		;prepare to read from addr
 		IN 		AX, DX 			;get the value from addr and put it in AX
 								;  note: the value will be 16-bit
 		POP 	DX 				;restore the register
)

; WRITEPCB
;
; Description:      	This macro writes the 16-bit value val to the PCB
; 						register at address addr.
;
; Operation:        	Put the address in DX and value in AX. Output the value
; 						to the address.
;
; Arguments:        	addr - PCB address to write to
; 						val - value to write to the address
;
; Return Values: 		None.
;
; Registers Changed:	None.
;
; Revision History:
;     11/22/15  Dennis Shim     initial revision

%*DEFINE(WRITEPCB(addr, val))   (
		PUSH	AX 			 	;preserve registers
		PUSH 	DX
		MOV 	DX, %addr 		;prepare to write to addr
		MOV 	AX, %val
		OUT 	DX, AX  		;output to the PCB addr
		POP 	DX 				;restore the registers
		POP 	AX
)


$LIST