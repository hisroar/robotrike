;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    INT2.INC                                ;
;                                INT 2 Functions                             ;
;                                   EE/CS 51                                 ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for the initialization of INT 2
; and for running the Int2EventHandler.
;
; Revision History:
;     11/19/15  Dennis Shim     Initial revision

; Timer Definitions

; Addresses
Int2Ctrl        EQU     0FF3CH          ;address of Int 2 Control Register

; Control Register Values
Int2CtrlVal     EQU     00000H          ;value to write to INT 2 Control Register
                                        ;-----------0----  edge triggering
                                        ;------------0---  enable interrupts
                                        ;-------------000  priority level 0


; Interrupt Vectors
Int2Vec         EQU     14              ;interrupt vector for INT 2
