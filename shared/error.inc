;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  ERROR.INC                             	 ;
;                                   EE/CS 51                                 ;
;						   Error Values Include File 						 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains constants that represent error values.
;
; Revision History:
;     12/4/15   Dennis Shim		initial revision
; 	  12/9/15 	Dennis Shim 	changed NUM_ERRORS

; error constants
NO_ERROR 				EQU 	00000000B;no error

; serial side error values
ERROR_PARITY  			EQU 	00000100B;serial parity error
ERROR_OVERRUN 	 		EQU 	00000010B;serial overrun error
ERROR_FRAMING 			EQU 	00001000B;serial framing error
ERROR_BRK_IRQ 			EQU 	00010000B;serial break interrupt error
ERROR_BUFFER_OVERFLOW 	EQU 	00100000B;serial buffer overflow error
ERROR_SERIAL_PARSE 		EQU		01000000B;serial parsing error

NUM_ERRORS 				EQU 	6 		;number of errors

; motor side error values
MOTOR_ERROR_VAL	 		EQU 	00000001B;OR with this value to distinguish the
										 ;  error as having been from the motor
										 ;note that these errors are all the same
										 ;  but just from the motor side
M_ERROR_PARITY 			EQU 	MOTOR_ERROR_VAL OR ERROR_PARITY
M_ERROR_OVERRUN 		EQU 	MOTOR_ERROR_VAL OR ERROR_OVERRUN
M_ERROR_FRAMING			EQU 	MOTOR_ERROR_VAL OR ERROR_FRAMING
M_ERROR_BRK_IRQ 		EQU 	MOTOR_ERROR_VAL OR ERROR_BRK_IRQ
M_ERROR_BUFFER_OVERFLOW EQU 	MOTOR_ERROR_VAL OR ERROR_BUFFER_OVERFLOW
M_ERROR_SERIAL_PARSE	EQU 	MOTOR_ERROR_VAL OR ERROR_SERIAL_PARSE

