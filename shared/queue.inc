;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   QUEUE.INC                                ;
;                                Queue Functions                             ;
;                                   EE/CS 51                                 ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains constants and a STRUC used for queues.
;
; Revision History:
;     10/15/15  Dennis Shim     initial revision

; constants
                                    ; queue lengths are powers of two in order to
                                    ;   use AND (length - 1) in place of MODULO;
                                    ;   ANDing by (length - 1) cuts off all bits
                                    ;   higher than the power of two, thus MODULO
QUEUE_LENGTH        EQU     256     ; maximum length when initializing the array

; structure for the queue
QUEUE           STRUC
    len         DW      ?           ; length of the queue
    siz         DB      ?           ; size of each element
    head        DW      ?           ; head offset
    tail        DW      ?           ; tail offset
    array       DB      (QUEUE_LENGTH) DUP (?)  ; array of elements
QUEUE           ENDS