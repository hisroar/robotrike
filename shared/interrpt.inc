;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 INTERRPT.INC                               ;
;                             Interrupt Functions                            ;
;                                   EE/CS 51                                 ;
;								  Include File 								 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains constants that are necessary to initialize the interrupt
; vector table, and for the IllegalEventHandler to send its EOI. It also contains
; constants specifically for timer interrupts.
;
; Revision History:
;     10/29/15  Dennis Shim		initial revision
; 	  11/5/15 	Dennis Shim 	updated for keypad

; Interrupt Controller Definitions

; Addresses
INTCtrlrCtrl    EQU     0FF32H          ;address of interrupt controller for timer
INTCtrlrEOI     EQU     0FF22H          ;address of interrupt controller EOI register

; Register Values
Int2EOI       	EQU     0000EH          ;INT 2 EOI command
TimerEOI        EQU     00008H          ;Timer EOI command (same for all timers)
NonSpecEOI      EQU     08000H          ;Non-specific EOI command

; General Definitions

FIRST_RESERVED_VEC		EQU		1		;reserve vectors 1-3
LAST_RESERVED_VEC		EQU		3
NUM_IRQ_VECTORS         EQU     256     ;number of interrupt vectors