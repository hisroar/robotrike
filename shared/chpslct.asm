        NAME    CHPSLCT

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  Chip Select                               ;
;                             Chip Select Functions                          ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains a function that initializes the chip selects for the
; hardware of the RoboTrike. The functions included are:
;       InitCS                  - Initializes the Peripheral Chip Selects.
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision

; local include files
$INCLUDE(../SHARED/CHPSLCT.INC) ; contains addresses and values for initializing
                                ; chip selects

CGROUP  GROUP   CODE

CODE    SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP


; InitCS
;
; Description:       Initialize the Peripheral Chip Selects on the 80188.
;
; Operation:         Write the initial values to the PACS and MPCS registers.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            PACSval is written to address PACSreg and MPCSval is written
;                    to address MPCSreg.
;                       
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: AX, DX
;
; Revision History:
;       10/29/97        Glen George

InitCS  PROC    NEAR
        PUBLIC  InitCS


        MOV     DX, PACSreg     ;setup to write to PACS register
        MOV     AX, PACSval
        OUT     DX, AL          ;write PACSval to PACS

        MOV     DX, MPCSreg     ;setup to write to MPCS register
        MOV     AX, MPCSval
        OUT     DX, AL          ;write MPCSval to MPCS


        RET                     ;done so return


InitCS  ENDP

CODE    ENDS

        END
