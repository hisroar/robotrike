;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  GENERAL.INC                             	 ;
;                                   EE/CS 51                                 ;
;							  General Include File 							 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains constants that are used by many different files.
;
; Revision History:
;     11/5/15  Dennis Shim		initial revision
; 	  11/19/15 Dennis Shim 		added byte and word size

; constants

TRUE 				EQU 	1 		;boolean TRUE
FALSE 				EQU 	0		;boolean FALSE

DEGREES_IN_CIRCLE 	EQU 	360 	;number of degrees in one full circle

ASCII_NULL 			EQU 	0 		;ASCII null character
ASCII_CAR_RETURN 	EQU 	13 		;ASCII value of the carriage return

IVEC_SIZE 			EQU 	4		;size of each interrupt vector
IVEC_HALF_SIZE 		EQU 	2 		;size of half of each interrupt vector
									;  i.e the size of segment or offset

BYTE_SIZE           EQU     1       ; number of bytes in a byte
WORD_SIZE           EQU     2       ; number of bytes in a word

MIN_NEG_16_NUM 		EQU 	-32768  ;smallest possible negative 16-bit number
ABS_MIN_NEG_16_NUM 	EQU 	32768	;absolute value of the smallesg possible
									;  negative 16-bit number
MAX_POS_16_NUM 		EQU 	+32767  ;largest possible positive 16-bit number
MAX_16_UNUM 		EQU 	65535 	;largest possible unsigned 16-bit number