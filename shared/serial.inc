;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  SERIAL.INC                                ;
;                             Serial Chip Functions                          ;
;                                   EE/CS 51                                 ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for communicating with the serial
; chip.
;
; Revision History:
;     11/19/15  Dennis Shim     initial revision
; 	  12/4/15 	Dennis Shim 	fixed error mask and got rid of some redundancy,
; 								changed INIT_ to DEFAULT_
;     12/9/15   Dennis Shim     added SERIAL_PUT_CHAR_MAX

; Serial Chip Unit Definitions

; Addresses
SERIAL_BASE_ADDR 	EQU 	0100H 	;base address of the serial port
SERIAL_RBR_ADDR 	EQU 	SERIAL_BASE_ADDR 	;address of the receiver buffer register
SERIAL_THR_ADDR 	EQU 	SERIAL_BASE_ADDR 	;address of the transmitter holding register
SERIAL_DLL_ADDR 	EQU 	SERIAL_BASE_ADDR 	;address of the divisor latch (LSB) register
SERIAL_IER_ADDR 	EQU 	SERIAL_BASE_ADDR + 1;address of the serial interrupt enable register
SERIAL_DLM_ADDR 	EQU 	SERIAL_BASE_ADDR + 1;address of the latch (MSB) register
SERIAL_IIR_ADDR 	EQU 	SERIAL_BASE_ADDR + 2;address of the serial interrupt identification register
SERIAL_LCR_ADDR 	EQU 	SERIAL_BASE_ADDR + 3;address of the serial control register
SERIAL_MCR_ADDR 	EQU 	SERIAL_BASE_ADDR + 4;address of the modem control register
SERIAL_LSR_ADDR 	EQU 	SERIAL_BASE_ADDR + 5;address of the line status register
SERIAL_MSR_ADDR 	EQU 	SERIAL_BASE_ADDR + 6;address of the modem status register
SERIAL_SCR_ADDR 	EQU		SERIAL_BASE_ADDR + 7;address of the scratch register


; Line Control Register Values
								;OR with these values to construct the LCR value
								;  we want to output
								;don't include parity because we will OR that in
								;  later in SetSerialParity
WORD_LENGTH_5 	EQU		00000000B;------00  word length 5 bits
WORD_LENGTH_6 	EQU 	00000001B;------01 	word length 6 bits
WORD_LENGTH_7 	EQU 	00000010B;------10  word length 7 bits
WORD_LENGTH_8 	EQU 	00000011B;------11  word length 8 bits
STOP_LENGTH_1 	EQU 	00000000B;-----0--  1 stop bit
STOP_LENGTH_2 	EQU 	00000100B;-----1--  2 or 1.5 (word length 5) stop bits 
DISABLE_BREAK 	EQU 	00000000B;-0------  disable break control
FORCE_BREAK 	EQU 	01000000B;-1------  enable break control
DLAB_DISABLE 	EQU 	00000000B;0-------  disable access to divisor latch
DLAB_ENABLE 	EQU 	10000000B;1------- 	enable access to divisor latch

; Interrupt Enable Register Values
ENA_DATA_AVAIL 	EQU 	00000001B;OR with this value to turn on data available interrupt
ENA_TRANS_EMPT 	EQU 	00000010B;OR with this value to turn on transmitter holding
							 	 ;  register empty interrupt
ENA_LINE_STAT  	EQU 	00000100B;OR with this value to turn on line status interrupt
ENA_MODEM_STAT 	EQU 	00001000B;OR with this value to turn on modem status interrupt
DIS_DATA_AVAIL 	EQU 	11111110B;AND with this value to turn off data available interrupt
DIS_TRANS_EMPT 	EQU 	11111101B;AND with this value to turn off transmitter holding
							 	 ;  register empty interrupt
DIS_LINE_STAT 	EQU 	11111011B;AND with this value to turn off line status interrupt
DIS_MODEM_STAT 	EQU 	11110111B;AND with this value to turn off modem status interrupt

; Interrupt Identification Register Values
NO_INT_PENDING 	EQU 	00000001B;no interrupt is pending

; Baud rate indices - go from 0 to 6
 								;use each index to get the corresponding baud
 								;  rate divisor from the SerialBaudTable
BAUD600 			EQU  	0 	;baud rate of 600
BAUD1200 			EQU 	1 	;baud rate of 1200
BAUD2400 			EQU 	2 	;baud rate of 2400
BAUD4800 			EQU 	3 	;baud rate of 4800
BAUD9600 			EQU 	4 	;baud rate of 9600
BAUD19200 			EQU 	5 	;baud rate of 19200
BAUD38400 			EQU 	6 	;baud rate of 38400

; Parity indices - go from 0 to 4
								;use each index to get the corresponding parity
								;  value from the SerialParityTable
PARITY_EVEN 		EQU 	0 	;parity is even
PARITY_ODD 			EQU 	1 	;parity is odd
PARITY_SET 			EQU 	2 	;check parity bit as set
PARITY_CLEAR 		EQU 	3 	;check parity bit as cleared
PARITY_NONE 		EQU 	4 	;no parity

; Constants
DEFAULT_BAUD_RATE 	EQU 	BAUD9600 	;index of initial serial baud rate desired,
										;  should be 9600
DEFAULT_PARITY 	 	EQU 	PARITY_NONE	;index of initial parity desired, should
										;  be set to no parity

PARITY_CTRL_MASK 	EQU 	11000111B 	;AND to mask the parity bits for the
										;  value in the serial control register
BAUD_CTRL_MASK 		EQU 	10000000B	;OR with value in serial control register
 										;  to allow access to to the baud rate
										;  divisor register
ERROR_MASK 			EQU 	00011110B 	;AND with this value to mask out everything
										;  in the line status register except for
										;  the error bits (overrun, parity, framing,
										;  break interrupt)
SERIAL_PUT_CHAR_MAX EQU 	5 		;maximum number of times SerialPutChar can
									;  be called before an error is enqueued
