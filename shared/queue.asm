        NAME    QUEUE

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Queue                                   ;
;                               Queue Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains functions that allow a user to manipulate queues. The
; functions included are:
;       QueueInit               - initializes the queue, which is a structure
;                                 with a length, element size, and array. (PUBLIC)
;       QueueEmpty              - checks if the queue is empty, returns the
;                                 result in the zero flag. (PUBLIC)
;       QueueFull               - checks if the queue is empty, returns the
;                                 result in the zero flag. (PUBLIC)
;       Dequeue                 - blocks until there is an element in the
;                                 queue, then dequeues and returns it. (PUBLIC)
;       Enqueue                 - blocks until there is space in the queue,
;                                 then enqueues a passed argument. (PUBLIC)
;
; Revision History:
;     10/22/15  Dennis Shim      initial revision
;     12/8/15   Dennis Shim      used WORD PTR instead of writing bytes

; local include files
$INCLUDE(../SHARED/QUEUE.INC)
$INCLUDE(../SHARED/GENERAL.INC)

CGROUP  GROUP   CODE


CODE    SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP



; QueueInit
;
; Description:        Initializes a queue of length l and element size s at the
;                     address a. After initialization, the queue will be empty and 
;                     ready to accept values. The passed length is the maximum
;                     number of items that can be stored. The element size s
;                     specifies whether each entry is a byte (8-bits) or a word
;                     (16-bits). If s is true (non-zero), the elements are words;
;                     otherwise, the elements are bytes. The address is passed in SI
;                     (the queue starts at DS:SI), the length is passed in AX, and
;                     the element size is passed in BL. The queue is stored in a 
;                     structure that begins at SI. The structure is documented in
;                     the include file (QUEUE.INC)
;
; Operational         
; Description:        The length (l) of the queue is written to the memory location
;                     [SI]. The size of each element (n) is written to the memory
;                     location [SI + 1]. The size is set to 2 if the argument size
;                     in BL is TRUE, otherwise the size is set to 1. The head and
;                     tail offsets are set to 4, as [SI + 4] will be the location
;                     of the first element that is enqueued.
;
; Arguments:          address - SI - address to write the queue to.
;                     size    - BL - true if elements are words, otherwise elements
;                                      are bytes.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   length - length of queue (maximum number of items).
;                     size   - size of each element (1 if byte, 2 if word).
;                     head   - index of the head of the queue.
;                     tail   - index of the tail of the queue.
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    Initializes a queue, which is a collection values stored using
;                     a first-in-first-out (FIFO) structure.
; Limitations:        Assumes the address is valid. Can only have length that is a 
;                     power of 2.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  none.
; Stack Depth:        0
;
; Author:             Dennis Shim
; Revision History:
;     10/22/15  Dennis Shim      initial revision

QueueInit       PROC        NEAR
                PUBLIC      QueueInit

QueueValueAssign:                   ; initialize queue shared variables
        MOV     [SI].len, QUEUE_LENGTH; set the length of queue at [SI].len
        MOV     [SI].head, 0        ; set the head of queue at [SI].head
        MOV     [SI].tail, 0        ; set the tail of queue at [SI].tail
        CMP     BL, 0               ; check if s == FALSE
        ;JE     SizeByte            ; s is false, size is byte
        JNE     SizeWord            ; s is true, size is word

SizeByte:                           ; set the size to be byte
        MOV     [SI].siz, BYTE_SIZE ; set the byte size at [SI].siz
        RET

SizeWord:                           ; set the size to be word       
        MOV     [SI].siz, WORD_SIZE ; set the word size at [SI].siz
        RET

QueueInit       ENDP




; QueueEmpty
;
; Description:        Function is called with the address of the queue to be 
;                     checked (a) and returns with the zero flag set if the queue
;                     is empty and with the zero flag reset otherwise. The address
;                     (a) is passed in SI by value.
;
; Operational         
; Description:        Compares the head and tail offsets. If they are equal, set
;                     the zero flag; otherwise reset the zero flag.
;
; Arguments:          address - SI - address of the queue.
;
; Return Values:      zero flag - set if empty, reset otherwise
; Global Variables:   none.
; Shared Variables:   head - index of the head of the queue.
;                     tail - index of the tail of the queue.
;
; Local Variables:    none. 
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    Accesses a queue, which is a collection values stored using
;                     a first-in-first-out (FIFO) structure.
; Limitations:        Assumes the address is valid. Can only have length that is a 
;                     power of 2.
; Known Bugs:         none.
; Special Notes:      none.
;
; Registers Changed:  none.
; Stack Depth:        0
;
; Author:             Dennis Shim
; Revision History:
;     10/22/15  Dennis Shim      initial revision

QueueEmpty      PROC        NEAR
                PUBLIC      QueueEmpty

CheckQueueEmpty:                    ; check if queue is empty
        PUSH    BX                  ; preserve register values
        PUSH    CX
                
        MOV     CX, [SI].head       ; AX temporarily contains head
        MOV     BX, [SI].tail       ; BX temporarily contains tail
        CMP     CX, BX              ; check head == tail, if true, then queue is
                                    ;   empty and zero flag is set, otherwise
                                    ;   zero flag is reset                            
        
        POP     CX                  ; retrieve register values
        POP     BX
        RET

QueueEmpty      ENDP


; QueueFull
;
; Description:        Function is called with the address of the queue to be 
;                     checked (a) and returns with the zero flag set if the queue
;                     is full and with the zero flag reset otherwise. The address
;                     (a) is passed in SI by value.
;
; Operational         
; Description:        Compares the head and tail offsets. If they are next to each
;                     other (i.e. h is right after t), set the zero flag; otherwise
;                     reset the zero flag.
;
; Arguments:          address - SI - address of the queue.
;
; Return Values:      zero flag - set if full, reset otherwise
; Global Variables:   none.
; Shared Variables:   length - length of queue (maximum number of items).
;                     size   - size of each element (1 if byte, 2 if word).
;                     head   - index of the head of the queue.
;                     tail   - index of the tail of the queue.
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    Accesses a queue, which is a collection values stored using
;                     a first-in-first-out (FIFO) structure.
; Limitations:        Assumes the address is valid.
; Known Bugs:         none.
; Special Notes:      none.
;
; Registers Changed:  none.
; Stack Depth:        0
;
; Author:             Dennis Shim
; Revision History:
;     10/22/15  Dennis Shim      initial revision

QueueFull       PROC        NEAR
                PUBLIC      QueueFull

CheckQueueFull:                     ; check if queue is full
        PUSHA                       ; preserve register values
        MOV     AX, [SI].head       ; AX temporarily contains the head 
        MOV     BX, [SI].tail       ; BX temporarily contains the tail 
        MOV     DX, [SI].len        ; DX temporarily contains the length of the queue
        MOV     CL, [SI].siz        ; CL temporarily contains the element size
        MOV     CH, 0               ; clear CH
        
        ADD     BX, CX              ; set tail = tail + size to compare to head
        DEC     DX                  ; decrement the length to prepare for AND
        AND     BX, DX              ; perform MODULO length using AND
        CMP     AX, BX              ; check head == (tail + size) MODULO length,
                                    ;   if true, then queue is full and zero flag
                                    ;   is set, otherwise zero flag is reset
        POPA                        ; retrieve register values
        RET

QueueFull       ENDP

; Dequeue
;
; Description:        Removes a 8-bit or 16-bit value (depending on the element
;                     size) from the head of the queue at the passed address (a),
;                     and return it in AL or AX. The value is returned in AL if
;                     the elements are bytes and in AX if they are words. If the
;                     queue is empty, the function waits until the queue has a
;                     value to be dequeued. The address (a) is passed in SI.
;
; Operational         
; Description:        Block until the queue is non-empty. When there is a value to
;                     dequeue, get its value from address [SI + 4 + head * size].
;                     Increment the head offset and take modulo l so that the queue
;                     wraps around its length. Return the dequeued value.
;
; Arguments:          address - SI - address of the queue.
;
; Return Values:      value - AX - value of the element that was dequeued from the head.
; Global Variables:   none.
; Shared Variables:   length - length of queue (maximum number of items).
;                     size   - size of each element (1 if byte, 2 if word).
;                     head   - index of the head of the queue (x2 if word).
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    Modifies a queue, which is a collection values stored using
;                     a first-in-first-out (FIFO) structure.
; Limitations:        Assumes the address is valid.
; Known Bugs:         none.
; Special Notes:      none.
;
; Registers Changed:  AX
; Stack Depth:        0
;
; Author:             Dennis Shim
; Revision History:
;     10/22/15  Dennis Shim      initial revision
;     12/8/15   Dennis Shim      used WORD PTR instead of writing bytes

Dequeue         PROC        NEAR
                PUBLIC      Dequeue

CheckQueueEmptyLoop:                    ; loop until queue is not empty
        CALL    QueueEmpty              ; check if queue is empty, pass address in SI
        JE      CheckQueueEmptyLoop     ; zero flag = 1, queue empty, keep looping
        ;JNE    DequeueValue            ; zero flag = 0, queue non-empty, dequeue

DequeueValue:
        PUSH    BX                      ; preserve register values
        PUSH    CX
        PUSH    DX

        MOV     CL, [SI].siz            ; CL contains the size of each element
        MOV     CH, 0                   ; clear CH
        MOV     BX, [SI].head           ; BX contains the head index
        MOV     DX, [SI].len            ; DX contains the length of the queue
        
        CMP     CL, BYTE_SIZE           ; check if the elements are bytes
        ;JE     DequeueByte             ; elements are bytes, dequeue a byte
        JNE     DequeueWord             ; otherwise, elements are words, dequeue a word

DequeueByte:
        MOV     AL, [SI].array[BX]      ; retrieve value at the head
        MOV     AH, 0                   ; clear AH
        JMP     IncrementHead           ; now increment the head

DequeueWord:
        MOV     AX, WORD PTR [SI].array[BX]; retrieve value at the head
        ;JMP    IncrementHead           ; now increment the head

IncrementHead:
        ADD     BX, CX                  ; increment the head index by element size
        DEC     DX                      ; decrement the length to prepare for AND
        AND     BX, DX                  ; take (head + 1) MODULO length using AND
        MOV     [SI].head, BX           ; set the head to be the new head

        POP     DX                      ; retrieve register values
        POP     CX
        POP     BX
        RET

Dequeue         ENDP


; Enqueue
;
; Description:        Adds the 8-bit or 16-bit value (v) (depending on the value
;                     size) to the tail of the queue at the passed address (a). If
;                     the queue is full, the funciton waits until the queue has an
;                     open space to add the value. It does not return until a value
;                     is added to the queue. The address (a) is passed in SI, and the
;                     value to enqueue (v) is passed by value in AL if the element
;                     size is bytes, and in AX if it is words.
;
; Operational         
; Description:        Block until the queue is non-full. When there is space to
;                     enqueue, put its value to the address [SI + 4 + tail * size].
;                     Increment the head offset and take modulo l so that the queue
;                     wraps around its length. Return the dequeued value.
;
; Arguments:          address - SI    - address of the queue.
;                     value - AL/AX - value to enqueue.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   length - length of queue (maximum number of items).
;                     size   - size of each element (1 if byte, 2 if word).
;                     tail   - index of the tail of the queue.
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    Modifies a queue, which is a collection values stored using
;                     a first-in-first-out (FIFO) structure.
; Limitations:        Assumes the address is valid.
; Known Bugs:         none.
; Special Notes:      none.
;
; Registers Changed:  none.
; Stack Depth:        0
;
; Author:             Dennis Shim
; Revision History:
;     10/22/15  Dennis Shim      initial revision
;     12/8/15   Dennis Shim      used WORD PTR instead of writing bytes

Enqueue         PROC        NEAR
                PUBLIC      Enqueue

CheckQueueFullLoop:                     ; loop until queue is not full
        CALL    QueueFull               ; check if queue is full, pass address in SI
        JE      CheckQueueFullLoop      ; zero flag = 1, queue full, keep looping
        ;JNE    EnqueueValue            ; zero flag = 0, queue non-full, enqueue

EnqueueValue:
        PUSH    BX                      ; preserve register values
        PUSH    CX
        PUSH    DX

        MOV     CL, [SI].siz            ; CL contains the size of each element
        MOV     CH, 0                   ; clear CH
        MOV     BX, [SI].tail           ; BX contains the tail index
        MOV     DX, [SI].len            ; DX contains the length of the queue
        
        CMP     CL, BYTE_SIZE           ; check if the elements are bytes
        ;JE     EnqueueByte             ; elements are bytes, dequeue a byte
        JNE     EnqueueWord             ; otherwise, elements are words, dequeue a word

EnqueueByte:
        MOV     [SI].array[BX], AL      ; put value at the tail
        JMP     IncrementTail           ; now increment the tail

EnqueueWord:
        MOV     WORD PTR [SI].array[BX], AX; put value at the tail
        ;JMP    IncrementTail           ; now increment the tail

IncrementTail:
        ADD     BX, CX                  ; increment the tail index by element size
        DEC     DX                      ; decrement the length to prepare for AND
        AND     BX, DX                  ; take (tail + 1) MODULO length using AND
        MOV     [SI].tail, BX           ; set the tail to be the new tail

        POP     DX                      ; retrieve register values
        POP     CX
        POP     BX
        RET

Enqueue         ENDP


CODE    ENDS



        END
