;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   MOTOR.INC                              	 ;
;                                Motor Functions                             ;
;                                   EE/CS 51                                 ;
;								  Include File 								 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for moving the motors on the
; RoboTrike.
;
; Revision History:
;     11/12/15  Dennis Shim		initial revision
; 	  12/4/15 	Dennis Shim 	added half and 30 perc speeds

; constants
NUM_MOTORS 			EQU  	3 		;number of motors on the RoboTrike
PULSE_COUNTER_MAX 	EQU 	128 	;maximum value of the pulse width counter
									;  note that each value in PulseWidths should
									;  be less than this value
FORWARD 			EQU 	1 		;value in MotorDir if the motor should be
									;  going forward
REVERSE  			EQU 	0 		;value in MotorDir if the motor should be
									;  going backwards

;drive speed constants
DRIVE_SPEED_STOP 	EQU 	0 		;RoboTrike is at a full stop when speed is 0
DRIVE_SPEED_MAX 	EQU 	65534 	;RoboTrike is at full speed when speed is 65534
DRIVE_SPEED_30  	EQU 	9830 	;approximately 30 percent of full speed
DRIVE_SPEED_HALF	EQU 	32767 	;half of full speed
DRIVE_SPEED_UNCHANGED EQU 	65535	;when this is passed as the speed, leave the
									;  DriveSpeed unchanged

;drive angle constants
DRIVE_ANGLE_FORWARD EQU 	0 		;RoboTrike should go forward when angle is 0
DRIVE_ANGLE_RIGHT 	EQU 	90 		;right when angle is 90
DRIVE_ANGLE_REVERSE EQU 	180 	;backwards when angle is 180
DRIVE_ANGLE_LEFT 	EQU 	270 	;left when angle is 270
DRIVE_ANGLE_MAX 	EQU 	359 	;this is the maximum angle DriveAngle can be
DRIVE_ANGLE_UNCHANGED EQU 	-32768 	;when this is passed as the angle, leave the
									;  DriveAngle unchanged

;laser constants
LASER_OFF 			EQU 	0 		;value in LaserStatus when the laser is off
LASER_ON 			EQU 	1 		;value in LaserStatus when the laser is on

; Output masks
LASER_ON_MASK 		EQU 	10000000B ;OR the output with this value to turn the
									  ;  laser on
LASER_OFF_MASK 		EQU 	00000000B ;OR the output with this value to turn the
									  ;  laser off
