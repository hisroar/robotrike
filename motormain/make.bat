asm86 motmain.asm m1 ep db
asm86 ../shared/chpslct.asm m1 ep db
asm86 ../shared/evqueue.asm m1 ep db
asm86 ../shared/int2.asm m1 ep db
asm86 ../shared/interrpt.asm m1 ep db
asm86 motor.asm m1 ep db
asm86 mototab.asm m1 ep db
asm86 parallel.asm m1 ep db
asm86 ../shared/queue.asm m1 ep db
asm86 ../shared/serial.asm m1 ep db
asm86 serparse.asm m1 ep db
asm86 timer.asm m1 ep db
asm86 trigtbl.asm m1 ep db

link86 motmain.obj, ../shared/chpslct.obj, ../shared/evqueue.obj, ../shared/int2.obj, ../shared/interrpt.obj TO tmp1.lnk
link86 motor.obj, mototab.obj, parallel.obj, ../shared/queue.obj, ../shared/serial.obj, serparse.obj TO tmp2.lnk
link86 timer.obj, trigtbl.obj TO tmp3.lnk
link86 tmp1.lnk, tmp2.lnk, tmp3.lnk TO motmain.lnk
loc86 motmain.lnk NOIC AD(SM(CODE(4000H),DATA(400H), STACK(7000H)))