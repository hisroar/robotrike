;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   TIMER.INC                                ;
;                                Timer Functions                             ;
;                                   EE/CS 51                                 ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for the initialization of the timer
; and for running the TimerEventHandler.
;
; Revision History:
;     10/29/15  Dennis Shim     initial revision
;     11/13/15  Dennis Shim     removed interrupt constants

; Timer Definitions

; Addresses
Tmr0Ctrl        EQU     0FF56H          ;address of Timer 0 Control Register
Tmr0MaxCntA     EQU     0FF52H          ;address of Timer 0 Max Count A Register
Tmr0Count       EQU     0FF50H          ;address of Timer 0 Count Register

; Control Register Values
Tmr0CtrlVal     EQU     0E001H          ;value to write to Timer 0 Control Register
                                        ;1---------------  enable timer
                                        ;-1--------------  write to control
                                        ;--1-------------  enable interrupts
                                        ;---0------------  use T0CMPA
                                        ;----000000------  reserved
                                        ;----------0-----  read only
                                        ;-----------0----  TMRIN0 is an enable
                                        ;------------00--  increment at 1/4 CLKOUT
                                        ;--------------0-  single counter mode
                                        ;---------------1  continuous mode
INTCtrlrCVal    EQU     00001H          ;set priority for timers to 1 and enable
                                        ;000000000000----  reserved
                                        ;------------0---  enable timer interrupt
                                        ;-------------001  timer priority

; Interrupt Vectors
Tmr0Vec         EQU     8               ;interrupt vector for Timer 0

; Timing Definitions
COUNTS_PER_PWM_CYCLE    EQU     600     ;number of timer counts for 30 * 2^(bit
                                        ;  resolution) Hz = 3840, with bit
                                        ;  resolution of 7 (assumes 18.432 MHz
                                        ;  clock)
