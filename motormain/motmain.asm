        NAME    MOTORMAIN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  MOTORMAIN                                 ;
;                             Motor Side Main Loop                           ;
;                                  EE/CS  51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains functions that are necessary to parse communication over
; serial. The functions included are:
;       InitMotor           - Calls all initialization functions needed for
;                             the motor. (PRIVATE)
;       SetHardResetFlag    - Sets the HardResetFlag to TRUE. (PUBLIC)
;       HandleEvent         - Attempts to dequeue an event from the EventQueue.
;                             Call the appropriate function to handle the event.
;                             (PRIVATE)
;       HandleError         - Handles an error event by displaying the appropriate
;                             string (specified in ErrorValueTable). (PRIVATE)
;       SendStatus          - Outputs the status of the RoboTrike (speed, direction
;                             and laser status) over serial to the remote board.
;                             (PRIVATE)
;
; Revision History:
;    12/8/15    Dennis Shim         initial revision
;    12/9/15    Dennis Shim         updated comments and init
;    12/9/15   Dennis Shim     updated to work using a shared folder

;local include files
$INCLUDE(MOTMAIN.INC)               ;constants needed for remote main
$INCLUDE(EVENT.INC)                 ;contains event constants
$INCLUDE(../SHARED/ERROR.INC)       ;contains error constants
$INCLUDE(../SHARED/MOTOR.INC)       ;contains motor constants
$INCLUDE(../SHARED/GENERAL.INC)     ;contains general constants

; MotorMainLoop
;
; Description:      Main loop for the motor side of the RoboTrike. Initialize
;                   all of the different parts of the motor. Continuously
;                   check if there is an event in the EventQueue to dequeue and
;                   handle the event depending on what it is. Hard resets if there
;                   is a critical error. When an event is dequeued from the
;                   EventQueue, it is handled by a function depending on the
;                   event type.
;
; Input:            Input is received over serial from the remote board. The
;                   message received over serial will generally be commands
;                   that are sent when the user presses a key. The commands
;                   that can be processed are listed below:
;                   "S#" - set absolute speed
;                   "V#" - set relative speed
;                   "D#" - set relative direction
;                   "F"  - fire the laser
;                   "O"  - turn off the laser
;
; Output:           When the status of the motor changes (speed, direction, or
;                   laser status), the new status of the motor is sent over
;                   serial to the remote board. The status is sent over in the
;                   form "S[2B-MotorSpeed][2B-MotorDirection][1B-LaserStatus]".
;                   When an error occurs, the error value is sent over serial to
;                   the remote board. The error is sent over in the form
;                   "E[1B-ErrorValue]". Errors that are sent over serial to the
;                   remote board are displayed (see RMTMAIN.ASM functional spec
;                   for more details).
;
; User Interface:   See User Interface in RMTMAIN.ASM.
; Error Handling:   See Error Handling in RMTMAIN.ASM.
;
; Known Bugs:       None.
; Limitations:      None.

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA, STACK

CODE	SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP, DS:DGROUP

;external function declarations

        EXTRN   InitCS:NEAR             ;initializes chip selects
        EXTRN   ClrIRQVectors:NEAR      ;clears the interrupt vector table
        EXTRN   InstallTimer0EH:NEAR    ;installs timer 0 event handler
        EXTRN   InitTimer0:NEAR         ;initializes timer 0
        EXTRN   InstallInt2EH:NEAR      ;installs int 2 event handler
        EXTRN   InitInt2:NEAR           ;initializes int 2
        EXTRN   InitParallel:NEAR       ;initializes parallel
        EXTRN   MotorInit:NEAR          ;initializes motor
        EXTRN   SerialInit:NEAR         ;initializes serial
        EXTRN   ParseSerialInit:NEAR    ;initialize the serial parser
        EXTRN   EventQueueInit:NEAR     ;initialize the event queue
        EXTRN   EnqueueEvent:NEAR       ;enqueues an event to EventQueue
        EXTRN   DequeueEvent:NEAR       ;dequeues an event from EventQueue
        EXTRN   ParseSerialChar:NEAR    ;parses a character from serial
        EXTRN   GetMotorSpeed:NEAR      ;get the motor speed
        EXTRN   GetMotorDirection:NEAR  ;get the motor direction
        EXTRN   GetLaser:NEAR           ;get the laser status
        EXTRN   SerialPutStr:NEAR       ;put a string in serial
        EXTRN   TrySerialPutChar:NEAR   ;try to put a char in serial
        
START:  

MAIN:
        MOV     AX, DGROUP              ;initialize the stack pointer
        MOV     SS, AX
        MOV     SP, OFFSET(DGROUP:TopOfStack)

        MOV     AX, DGROUP              ;initialize the data segment
        MOV     DS, AX

HardReset:
        CALL    InitMotorBoard          ;initialize the motor board
        STI                             ;set IF to allow interrupts

Forever:                                ;sit in an infinite loop, keep checking
                                        ;  if there's something in the EventQueue
                                        ;  handle it if there is
        CALL    HandleEvent             ;handle the event if there is one

        CMP     HardResetFlag, TRUE     ;check if we need to hard reset
        ;JE     DoHardReset             ;flag is set, need to hard reset
        JNE     ForeverLoop             ;flag isn't set, don't hard reset

DoHardReset:                            ;hard reset when flag is set
        CLI                             ;turn off interrupts while initializing
                                        ;  everything, lots of critical code
                                        ;  (especially in interrupt code)
        JMP     HardReset               ;NOTE: hard reset flag is cleared

ForeverLoop:
        JMP     Forever                  


        HLT                            ;never executed (hopefully)

; InitMotorBoard
;
; Description:        Initializes chip selects, interrupts, timer 0, INT 2,
;                     motor, parallel chip, and serial parser. HardResetFlag is
;                     reset.
;
; Operational         
; Description:        Call the initialization functions for chip select, interrupts,
;                     timer 0, INT 2, motor, parallel chip, serial, and serial parser.
;                     Reset the HardResetFlag to FALSE.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   HardResetFlag - DS - set if hard reset is required (w).
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision
;     12/9/15  Dennis Shim     moved STI to main loop

InitMotorBoard  PROC    NEAR

        CALL    InitCS                  ;initialize the 80188 chip selects
                                        ;   assumes LCS and UCS already setup

        CALL    ClrIRQVectors           ;clear (initialize) interrupt vector table

        CALL    InstallTimer0EH         ;install the event handler for timer0

        CALL    InitTimer0              ;initialize the internal timer

        CALL    InstallInt2EH           ;install the event handler for INT 2

        CALL    InitInt2                ;initialize the INT 2 interrupt

        CALL    MotorInit               ;initialize the motor variables
        
        CALL    InitParallel            ;initialize the parallel chip

        CALL    SerialInit              ;initialize serial port

        CALL    ParseSerialInit         ;initialize serial parser
        
        CALL    EventQueueInit          ;initialize the event queue

        MOV     HardResetFlag, FALSE    ;no hard reset required yet

        RET

InitMotorBoard ENDP

; SetHardResetFlag
;
; Description:        Sets the HardResetFlag to TRUE.
;
; Operational         
; Description:        Put TRUE in HardResetFlag.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   HardResetFlag - DS - set if hard reset is required (w).
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

SetHardResetFlag    PROC    NEAR
					PUBLIC  SetHardResetFlag

        MOV     HardResetFlag, TRUE

        RET

SetHardResetFlag    ENDP



; HandleEvent
;
; Description:        Attempts to dequeue an event from the EventQueue. If there
;                     was an event, then call the appropriate handler for that
;                     event type (keypress, serial, or error).
;
; Operational         
; Description:        Attempt to dequeue an event. If the zero flag was set, there
;                     was no event so just return. Otherwise, check the EventID.
;                     If it is EVENT_KEYPAD, call HandleKey; if it is EVENT_SERIAL,
;                     call ParseSerialChar; if it is EVENT_ERROR, call HandleError.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   None.
;
; Local Variables:    EventID - AH - ID of the type of event (constant)
;                     EventArgument - AL - argument associated with the event
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

HandleEvent     PROC    NEAR

        PUSH    AX                  ;preserve register
CheckEvent:
        CALL    DequeueEvent        ;attempt to dequeue an event
                                    ;zero flag is set if EventQueue was empty
        JZ      HandleEventEnd      ;there was no event, just finish
        ;JNZ    EventDequeued       ;there was an event, call the right handler

EventDequeued:                      ;check what kind of event was enqueued
        CMP     AH, EVENT_SERIAL
        JE      SerialEventDequeued ;got a serial event

        CMP     AH, EVENT_ERROR
        JE      ErrorEventDequeued  ;got an error event

SerialEventDequeued:                ;got a serial event, call ParseSerialChar
        CALL    ParseSerialChar     ;char argument already in AL from dequeue
        CMP     AX, NO_ERROR        ;check if there was an error
        JE      HandleEventEnd      ;no error occurred, just finish
        ;JNE    SerialEventDequeuedError;an error occurred, enqueue an event

SerialEventDequeuedError:           ;put a serial parsing error in the EventQueue
        MOV     AH, EVENT_ERROR     ;enqueue an error event 
        MOV     AL, ERROR_SERIAL_PARSE;and a serial parsing error
        CALL    EnqueueEvent
        JMP     HandleEventEnd      ;and finish

ErrorEventDequeued:                 ;got an error event, call HandleError
        CALL    HandleError
        JMP     HandleEventEnd      ;and finish
        
HandleEventEnd:
        POP     AX                  ;restore register
        RET

HandleEvent      ENDP

; HandleError
;
; Description:        Handles a error from somewhere by sending the appropriate
;                     string that is associated with the error.
;
; Operational         
; Description:        Loop through the ErrorValueTable until the ErrorValue is
;                     found. When found, get the string associated with the error
;                     and put it in SerialString. Then call SerialPutStr with
;                     SerialString as the argument.
;
; Arguments:          ErrorValue - AL - value of the error received.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   SerialString - DS - the string to send over to serial is
;                                         stored in SerialString (w)
;
; Local Variables:    StringIndex - SI - index of the string buffer and display
;                                        string table to write/read chars from
;
; Inputs:             None.
; Outputs:            Calls SerialPutStr to send the string to serial.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

HandleError     PROC    NEAR

        PUSH    SI                  ;preserve registers

WriteSerialErrorString:
        XOR     SI, SI              ;start StringIndex at first index
                                    ;put CMD_SEND_ERROR as the first character
                                    ;  in the buffer to tell serial that
                                    ;  a motor error is being sent over
        MOV     SerialString[SI], CMD_SEND_ERROR

        INC     SI                  ;write the error value in the next index
        MOV     SerialString[SI], AL;want to send over what kind of error

        INC     SI                  ;write ASCII_NULL to terminate the string
        MOV     SerialString[SI], ASCII_NULL

        PUSH    DS                  ;move DS into ES so SerialPutStr gets the string
        POP     ES                  ;  from DS:SerialString
        LEA     SI, SerialString    ;pass SerialString's address as an argument
                                    ;  to SerialPutStr to send the right string
        
        CALL    SerialPutStr        ;send the error message over serial to the
                                    ;  remote side

HandleErrorEnd:
        POP     SI                  ;restore registers

        RET

HandleError         ENDP

; SendStatus
;
; Description:        Sends the motor status (speed, direction, laser status)
;                     over serial using a string of the format
;                     "CMD_SEND_STATUS|MOTSPD|MOTDIR|LSRSTAT", so CMD_SEND_STATUS 
;                     is 1 byte, MOTSPD is 2 bytes, MOTDIR is 2 bytes, LSRSTAT
;                     is 1 byte.
;
; Operational         
; Description:        Put CMD_SEND_STATUS in the first char of SerialString.
;                     Then put MotorSpeed in the next two bytes, MotorDir in the
;                     next two bytes, and the laser status in the next byte.
;                     Null terminate the string and call SerialPutStr.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   SerialString - DS - the string to send over to serial is
;                                         stored in SerialString (w)
;
; Local Variables:    MotorSpeed, MotorDir, LaserStatus - AX - temporary variables
;
; Inputs:             None.
; Outputs:            Calls SerialPutStr to send the string to serial.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

SendStatus      PROC    NEAR
                PUBLIC  SendStatus

        PUSH    AX                  ;preserve registers
        PUSH    SI

WriteStatusString:
        XOR     SI, SI              ;start StringIndex at first index
                                    ;put CMD_SEND_STATUS as the first character
                                    ;  in the buffer to tell serial that
                                    ;  motor status is being sent over
        MOV     AL, CMD_SEND_STATUS
        CALL    TrySerialPutChar 

WriteSpeed:	
        CALL    GetMotorSpeed       ;MotorSpeed is now in AX
        SHR 	AX, 1				;divide by two so we send signed value
        
        XCHG    AL, AH              ;send the higher byte of MotorSpeed first
        CALL    TrySerialPutChar
        
        XCHG    AL, AH              ;send the lower byte of MotorSpeed
        CALL    TrySerialPutChar
  
 WriteDirection:
        CALL    GetMotorDirection   ;MotorDir is now in AX
        
        XCHG    AL, AH              ;send the higher byte of MotorSpeed first
        CALL    TrySerialPutChar
        
        XCHG    AL, AH              ;send the lower byte of MotorSpeed
        CALL    TrySerialPutChar
 
 WriteLaser:
        CALL    GetLaser            ;LaserStatus is now in AL
        CALL    TrySerialPutChar       ;send the lower byte of LaserStatus (all we need)
        
        MOV     AL, ASCII_CAR_RETURN;finally, terminate with the carriage return
        CALL    TrySerialPutChar

SendStatusEnd:
        POP     AX                  ;restore registers
        POP     SI

        RET

SendStatus      ENDP

CODE 	ENDS

;the data segment

DATA    SEGMENT PUBLIC  'DATA'

SerialString        DB          (SERIAL_STRING_LENGTH)  DUP     (?);buffer to keep
                                                                   ;string to send
                                                                   ;to serial
HardResetFlag       DB          ?   ;set if a hard reset is needed

DATA    ENDS

;the stack

STACK           SEGMENT STACK  'STACK'

                DB      80 DUP ('Stack ')       ;240 words

TopOfStack      LABEL   WORD

STACK   ENDS

        END     START
