;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  SERIAL.INC                                ;
;                             Serial Chip Functions                          ;
;                                   EE/CS 51                                 ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for parsing communications from
; the serial chip.
;
; Revision History:
;     11/25/15  Dennis Shim     initial revision

; State definitions
; 		NOTE: these must match the layout of SerialState

ST_INITIAL 			EQU		0 		;initial state
ST_ABS_SPEED 		EQU  	1 		;cmd char 'S' - set absolute speed state
ST_REL_SPEED 		EQU 	2 		;cmd char 'V' - set relative speed state
ST_DIRECTION 		EQU 	3 		;cmd char 'D' - set direction state
ST_TUR_ANGLE 		EQU 	4 		;cmd char 'T' - rotate turret angle state
ST_TUR_ELEV 		EQU 	5 		;cmd char 'E' - set turret elevation state
ST_LASER_ON 		EQU 	6 		;cmd char 'F' - set laser on state
ST_LASER_OFF 		EQU 	7 		;cmd char 'O' - set laser off state
ST_SIGN 			EQU 	8 		;'+' or '-' - set sign of the number
ST_DIGIT 			EQU 	9 		;0-9 - add a digit 
ST_ERROR 			EQU 	10 		;some kind of error encountered
ST_END 				EQU 	11 		;all done

NUM_STATES 			EQU 	12 		;number of states

; Token definitions
; 		NOTE: these must match the layout of SerialState

TOKEN_CMD_CHAR_S 	EQU 	0 		;cmd char 'S'/'s' - set absolute speed
TOKEN_CMD_CHAR_V 	EQU 	1 		;cmd char 'V'/'v' - set relative speed
TOKEN_CMD_CHAR_D 	EQU 	2 		;cmd char 'D'/'d' - set direction
TOKEN_CMD_CHAR_T 	EQU 	3 		;cmd char 'T'/'t' - rotate turret angle
TOKEN_CMD_CHAR_E 	EQU	 	4 		;cmd char 'E'/'e' - set turret elevation
TOKEN_CMD_CHAR_F 	EQU 	5 		;cmd char 'F'/'f' - set laser on
TOKEN_CMD_CHAR_O 	EQU 	6 		;cmd char 'O'/'o' - set laser off
TOKEN_NEG_SIGN 		EQU 	7 		;'-' - set negative sign
TOKEN_POS_SIGN 		EQU  	8	 	;'+' - set positive sign
TOKEN_DIGIT 		EQU  	9 		;'0' to '9' - add a digit
TOKEN_EOS 			EQU  	10 		;token is end of string - <Return> or <CTRL/M>
TOKEN_BLANK 		EQU 	11 	 	;token is white space (space or tab)
TOKEN_OTHER 		EQU 	12 		;anything else

NUM_TOKEN_TYPES 	EQU 	13 		;number of tokens

; Sign definitions

SIGN_POSITIVE 		EQU 	+1 		;constant to indicate positive sign
SIGN_NEGATIVE 		EQU 	-1 		;constant to indicate negative sign
SIGN_NONE 			EQU 	0 		;constant to indicate no sign (i.e. positive)

; Command definitions

COM_NONE 			EQU 	0FFH 	;no command (initialization value)
COM_ABS_SPEED 		EQU 	0 		;set absolute speed command
COM_REL_SPEED 		EQU 	2 		;set relative speed command
COM_SET_DIR 		EQU 	4 		;set direction command
COM_SET_TUR_ANG 	EQU 	6 		;set turret angle command
COM_SET_TUR_ELV 	EQU 	8 		;set turret elevation command
COM_LASER_ON 		EQU 	10 		;set laser on command
COM_LASER_OFF 		EQU 	12 		;set laser off command

; Other constants

TOKEN_MASK 			EQU 	01111111B;mask high bit of token
HIGH_BIT_MASK       EQU     00000001B;mask all high bits

; Error definitions

ERROR_NONE 			    EQU 	0 		            ;no error

ERROR_TOKEN_CMD_CHAR_S  EQU     TOKEN_CMD_CHAR_S + 1;'S' token error
ERROR_TOKEN_CMD_CHAR_V 	EQU 	TOKEN_CMD_CHAR_V + 1;'V' token error
ERROR_TOKEN_CMD_CHAR_D 	EQU 	TOKEN_CMD_CHAR_D + 1;'D' token error
ERROR_TOKEN_CMD_CHAR_T 	EQU 	TOKEN_CMD_CHAR_T + 1;'T' token error
ERROR_TOKEN_CMD_CHAR_E 	EQU	 	TOKEN_CMD_CHAR_E + 1;'E' token error
ERROR_TOKEN_CMD_CHAR_F 	EQU 	TOKEN_CMD_CHAR_F + 1;'F' token error
ERROR_TOKEN_CMD_CHAR_O 	EQU 	TOKEN_CMD_CHAR_O + 1;'O' token error
ERROR_TOKEN_NEG_SIGN 	EQU 	TOKEN_NEG_SIGN + 1  ;'-' token error
ERROR_TOKEN_POS_SIGN 	EQU  	TOKEN_POS_SIGN + 1  ;'+' token error
ERROR_TOKEN_DIGIT 		EQU  	TOKEN_DIGIT + 1     ;'0'-'9' token error
ERROR_TOKEN_EOS 		EQU  	TOKEN_EOS + 1       ;<Return> token error
ERROR_TOKEN_BLANK 		EQU 	TOKEN_BLANK + 1     ;space or tab token error
ERROR_TOKEN_OTHER 		EQU 	TOKEN_OTHER + 1     ;other token error (invalid char)

ERROR_UNS_OVERFLOW 	    EQU 	14 		            ;unsigned overflow error
ERROR_POS_OVERFLOW 	    EQU 	15 		            ;positive overflow error
ERROR_NEG_OVERFLOW 	    EQU  	16 		            ;negative overflow error
