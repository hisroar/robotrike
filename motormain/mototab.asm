        NAME  MOTORTABLE

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   MOTOTAB                                  ;
;                            Tables of Motors Code                           ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file includes the tables that are used in the motor table.  The tables
; included are:
;    ForceXTable        - Contains the values of the x-direction of each force
;                         vector for the motors.
;    ForceYTable        - Contains the values of the y-direction of each force
;                         vector for the motors.
;    MotorOutputOn      - Contains the values for the output masks for each
;                         motor when the motor is turned on.
;    MotorOutputOff     - Contains the values for the output masks for each
;                         motor when the motor is turned off.
;    MotorOutputForward - Contains the values for the output masks for each
;                         motor when the motor is going forward.
;    MotorOutputReverse - Contains the values for the output masks for each
;                         motor when the motor is going backwards.
;
; Revision History:
;    11/12/15   Dennis Shim     Initial Revision


; local include files
;    none


;setup code group and start the code segment
CGROUP  GROUP   CODE

CODE    SEGMENT PUBLIC 'CODE'

; ForceXTable
;
; Description:      Contains the values for the x-direction of each force
;                   vector for the motors. The ith word in the table is the
;                   magnitude of the x-direction of the ith force vector (F_ix).
;                   The values are signed and fixed point Q0.15.
;
; Notes:            READ ONLY tables should always be in the code segment so
;                   that in a standalone system it will be located in the
;                   ROM with the code.
;
; Revision History:
;    11/12/15   Dennis Shim     Initial Revision

ForceXTable     LABEL   WORD
                PUBLIC  ForceXTable

        DW      07FFFH                           ;F_1x = 1
        DW      0C000H                           ;F_2x = -1/2
        DW      0C000H                           ;F_3x = -1/2


; ForceYTable
;
; Description:      Contains the values for the y-direction of each force
;                   vector for the motors. The ith word in the table is the
;                   magnitude of the y-direction of the ith force vector (F_ix).
;                   The values are signed and fixed point Q0.15.
;
; Notes:            READ ONLY tables should always be in the code segment so
;                   that in a standalone system it will be located in the
;                   ROM with the code.
;
; Revision History:
;    11/12/15   Dennis Shim     Initial Revision

ForceYTable     LABEL   WORD
                PUBLIC  ForceYTable

        DW      00000H                           ;F_1y = 0
        DW      09127H                           ;F_2y = -sqrt(3)/2
        DW      06ED9H                           ;F_3y = sqrt(3)/2


; NOTE: when using the following tables, the output must have initially been
;       cleared before the masks are applied.

; MotorOutputOn
;
; Description:      Contains the values for the masks of each motor. The mask
;                   should be OR'ed with the output in order to get the correct
;                   output. The masks of this table are OR'ed with the output
;                   when the corresponding motor is turned on.
;
; Notes:            READ ONLY tables should always be in the code segment so
;                   that in a standalone system it will be located in the
;                   ROM with the code.
;
; Revision History:
;    11/12/15   Dennis Shim     Initial Revision

MotorOutputOn   LABEL   BYTE
                PUBLIC  MotorOutputOn

        DB      00000010B           ;OR with this value to turn motor 1 on
        DB      00001000B           ;OR with this value to turn motor 2 on
        DB      00100000B           ;OR with this value to turn motor 3 on


; MotorOutputOff
;
; Description:      Contains the values for the masks of each motor. The mask
;                   should be OR'ed with the output in order to get the correct
;                   output. The masks of this table are OR'ed with the output
;                   when the corresponding motor is turned off.
;
; Notes:            READ ONLY tables should always be in the code segment so
;                   that in a standalone system it will be located in the
;                   ROM with the code.
;
; Revision History:
;    11/12/15   Dennis Shim     Initial Revision

MotorOutputOff  LABEL   BYTE
                PUBLIC  MotorOutputOff

        DB      00000000B           ;OR with this value to turn motor 1 off
        DB      00000000B           ;OR with this value to turn motor 2 off
        DB      00000000B           ;OR with this value to turn motor 3 off


; MotorOutputForward
;
; Description:      Contains the values for the masks of each motor. The mask
;                   should be OR'ed with the output in order to get the correct
;                   output. The masks of this table are OR'ed with the output
;                   when the corresponding motor is moving forward.
;
; Notes:            READ ONLY tables should always be in the code segment so
;                   that in a standalone system it will be located in the
;                   ROM with the code.
;
; Revision History:
;    11/12/15   Dennis Shim     Initial Revision

MotorOutputForward  LABEL   BYTE
                    PUBLIC  MotorOutputForward

        DB      00000000B           ;OR with this value to make motor 1 forward
        DB      00000000B           ;OR with this value to make motor 2 forward
        DB      00000000B           ;OR with this value to make motor 3 forward


; MotorOutputReverse
;
; Description:      Contains the values for the masks of each motor. The mask
;                   should be OR'ed with the output in order to get the correct
;                   output. The masks of this table are OR'ed with the output
;                   when the corresponding motor is moving forward.
;
; Notes:            READ ONLY tables should always be in the code segment so
;                   that in a standalone system it will be located in the
;                   ROM with the code.
;
; Revision History:
;    11/12/15   Dennis Shim     Initial Revision

MotorOutputReverse  LABEL   BYTE
                    PUBLIC  MotorOutputReverse

        DB      00000001B           ;OR with this value to make motor 1 reverse
        DB      00000100B           ;OR with this value to make motor 2 reverse
        DB      00010000B           ;OR with this value to make motor 3 reverse

CODE    ENDS



        END
