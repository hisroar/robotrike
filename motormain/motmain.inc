;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  RMTMAIN.INC                               ;
;                           Remote Main Loop Functions                       ;
;                                   EE/CS 51                                 ;
;								  Include File 								 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for the remote side main loop.
;
; Revision History:
;     12/8/15   Dennis Shim		initial revision
;     12/9/15   Dennis Shim     moved SERIAL_PUT_CHAR_MAX to serial.inc

; constants

SERIAL_STRING_LENGTH EQU 	16 		;maximum size of serial string buffer

; Direction and Speed Change
DELTA_SPEED 		EQU 	1000 	;amount to increase or decrease the speed by
DELTA_DIRECTION 	EQU 	5 		;degrees to increase or decrease the angle by

;Serial command chars
CMD_SEND_STATUS 	EQU 	'S' 	;send status
CMD_SEND_ERROR	 	EQU 	'E' 	;send error
