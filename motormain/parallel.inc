;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 PARALLEL.INC                               ;
;                            Parallel Chip Functions                         ;
;                                   EE/CS 51                                 ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for communicating with the parallel
; chip
;
; Revision History:
;     11/12/15  Dennis Shim     initial revision

; Parallel Chip Unit Definitions

; Addresses
PARALLEL_BASE_ADDR      EQU     00180H  ;address of parallel chip

; Group Offsets
GROUPA_OFFSET           EQU     0       ;offset of Group A from the parallel chip
GROUPB_OFFSET           EQU     1       ;offset of Group B from the parallel chip
GROUPC_OFFSET           EQU     2       ;offset of Group C from the parallel chip
CTRL_OFFSET             EQU     3       ;offset of the control register from the
                                        ;  parallel chip

; Control Register Value
PARALLELval     EQU     10000000B       ;Port B is for output
                                        ;1-------  mode set flag active
                                        ;-XXXX---  Group A doesn't matter
                                        ;-----00X  Mode 0, Port B for output
