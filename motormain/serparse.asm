        NAME  SerialParser

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 Serial Parser                              ;
;                            Serial Parsing Functions                         ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains functions that are necessary to parse communication over
; serial. The functions included are:
;       ParseSerialInit       - Initializes the variables used when parsing
;                               the serial characters. (PUBLIC)
;       ParseSerialChar       - Parses a single serial character using a serial
;                               parser finite state machine. (PUBLIC)
;       GetSerialToken        - Gets the token type and value of a char. (PRIVATE)
;       ValidCommandChar      - Sets the CommandFlag to the appropriate value
;                               based on the argument passed. (PRIVATE)
;       ChangeSign            - Sets the SignFlag to the appropriate value based
;                               on the argument passed. (PRIVATE)
;       AddDigit              - Adds a digit to Num. (PRIVATE)
;       CallCommand           - Calls the appropriate function based on
;                               CommandFlag. (PRIVATE)
;       SetError              - Sets ErrorVal to the appropriate value based on
;                               the argument passed. (PRIVATE)
;       DoNOP                 - Does nothing. (PRIVATE)
;       SerialStateTable      - Contains the states of the serial parser finite
;                               state machine. (PRIVATE)
;       SerialTokenTypeTable  - Contains the token types for each char. (PRIVATE)
;       SerialTokenValueTable - Contains the token values for each char. (PRIVATE)
;       SerialCommandTable    - Contains the label that calls the function that
;                               corresponds to the CommandFlag. (PRIVATE)
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision
;     12/8/15   Dennis Shim     added SendStatus and updated comments
;     12/9/15   Dennis Shim     updated to work using a shared folder


; local include files
$INCLUDE(../SHARED/GENERAL.INC) ;contains general constants and addresses
$INCLUDE(SERPARSE.INC)          ;contains constants for the serial parser
$INCLUDE(../SHARED/MOTOR.INC)   ;contains constants for the motor

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE    SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DGROUP

; external action routines used by state machine
;   assumed to affect no registers
    EXTRN  SetMotorSpeed:NEAR           ;sets the speed/direction of the motor
    EXTRN  GetMotorSpeed:NEAR           ;gets the speed of the motor
    EXTRN  GetMotorDirection:NEAR       ;gets the direction of the motor
    EXTRN  SetLaser:NEAR                ;sets the laser status
    EXTRN  GetLaser:NEAR                ;gets the laser status
    EXTRN  SetTurretAngle:NEAR          ;sets the turret angle (not implemented)
    EXTRN  SetRelTurretAngle:NEAR       ;sets relative turret angle (not implemented)
    EXTRN  GetTurretAngle:NEAR          ;gets the turret angle (not implemented)
    EXTRN  SetTurretElevation:NEAR      ;sets the turret elevation (not implemented)
    EXTRN  GetTurretElevation:NEAR      ;gets the turret elevation (not implemented)
    EXTRN  SendStatus:NEAR              ;sends status of the Trike


; ParseSerialInit
;
; Description:        Initializes the variables used for serial parsing. Sets
;                     CurrentState to STATE_START, ErrorVal to ERROR_NONE,
;                     CommandFlag to COM_NONE, SignFlag to SIGN_NONE, and Num to
;                     0 (digits are added to Num later).
;
; Operational         
; Description:        Set CurrentState to STATE_START, ErrorVal to ERROR_NONE,
;                     CommandFlag to COM_NONE, SignFlag to SIGN_NONE, and Num to
;                     0.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   CurrentState - DS - Current state the FSM is in after previous
;                                         iterations of ParseSerialChar. (w)
;                     ErrorVal - DS - ERROR_NONE if there is no error, otherwise
;                                     there is an error identified by the code. (w)
;                     CommandFlag - DS - set to a value corresponding to the
;                                        desired command (COM_ABS_SPEED for setting
;                                        absolute speed, COM_REL_SPEED for setting
;                                        relative speed, COM_SET_DIR for setting
;                                        direction, COM_SET_TUR_ANG for setting
;                                        turret angle, COM_SET_TUR_ELV for setting
;                                        turret elevation, COM_LASER_ON for turning
;                                        the laser on, COM_LASER_OFF for turning
;                                        the laser off) (w)
;                     SignFlag - DS - SIGN_NEGATIVE if Num should be negative when
;                                     passed as an argument, SIGN_POSTIIVE if Num 
;                                     should be positive when passed as an argument,
;                                     SIGN_NONE means Num should be positive when
;                                     passed as an argument. (w)
;                     Num - DS - number that will be called as an argument with
;                                the corresponding command (w)                 
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

ParseSerialInit     PROC    NEAR
                    PUBLIC  ParseSerialInit

        MOV     CurrentState, ST_INITIAL    ;start at the initial state
        MOV     ErrorVal, ERROR_NONE        ;no errors initially
        MOV     CommandFlag, COM_NONE       ;no commands initially
        MOV     SignFlag, SIGN_NONE         ;no sign initially
        MOV     Num, 0                      ;Num starts at 0 initially so that
                                            ;  digits can be added later
        RET

ParseSerialInit     ENDP

; ParseSerialChar
;
; Description:        This procedure parses a passed ASCII character. It uses a
;                     finite state machine to parse multiple characters in sequence
;                     in order to determine what command is being sent. The command
;                     S# sets the absolute speed, V# sets the relative speed, D#
;                     sests the direciton, T# sets the turret angle (not implemented)
;                     E# sets the turret elevation angle (not implemented), F turns
;                     the laser on, and O turns the laser off. # means that a valid
;                     signed number. Returns the value of the error.
;
; Operational         
; Description:        Set ErrorVal to ERROR_NONE. If the CurrentState is either the
;                     end or an error state, do nothing. Otherwise, get the
;                     TokenType and TokenVal by calling GetSerialToken. Then, call
;                     the function that corresponds to CurrentState and TokenType
;                     with the argument TokenVal. Return the error value.
;
; Arguments:          char - AL - ASCII character to be parsed
;
; Return Values:      ErrorVal - AX - ERROR_NONE if there is no error, otherwise 
;                                     there is an error identified by the code.
; Global Variables:   None.
; Shared Variables:   CurrentState - DS - Current state the FSM is in after previous
;                                         iterations of ParseSerialChar. (w)
;                     ErrorVal - DS - ERROR_NONE if there is no error, otherwise
;                                     there is an error identified by the code. (r/w)
;
; Local Variables:    TokenType - type of the token (char) passed
;                     TokenVal - value of the token (char) passed
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

ParseSerialChar     PROC    NEAR
                    PUBLIC  ParseSerialChar

        PUSH    BX                      ;preserve registers
        PUSH    CX
        PUSH    DX

        MOV     ErrorVal, ERROR_NONE    ;no errors yet

CheckParseSerialReset:                  ;check if we are in an end or error state, 
                                        ;  if we are, then re-initialize the
                                        ;  parser
        CMP     CurrentState, ST_END    ;check if we are in the end state
        JE      ParseSerialReset        ;we are in end state, reset serial parser
        CMP     CurrentState, ST_ERROR  ;check if we are in the error state
        JE      ParseSerialReset        ;we are in error state, reset parser
        JMP     LookupSerialToken       ;not in end or error state, get the next
                                        ;  token and parse it

ParseSerialReset:                       ;reset by re-initializing
        CALL    ParseSerialInit
        ;JMP    LookupSerialToken       ;get the next token and parse it 

LookupSerialToken:                      ;get input for state machine
        CALL    GetSerialToken          ;get the token type and value
        MOV     DH, AH                  ;save the token type in DH
        MOV     CH, AL                  ;save the token value in CH

ComputeTransition:                      ;get which transition to do
        MOV     AL, NUM_TOKEN_TYPES     ;get the right row in the table
        MUL     CurrentState            ;AX is the start row index for CurrentState
        ADD     AL, DH                  ;get the actual transition index
        ADC     AH, 0                   ;propagate low byte carry into high byte
        IMUL    BX, AX, SIZE TRANSITION_ENTRY;convert index to table offset

PerformTransition:                      ;change the state to the next state
                                        ;  specified by the table
                                        ;  NOTE: must call the action after doing
                                        ;  the transition because of the case
                                        ;  where an error occurs in the action
                                        ;  (e.g. overflow)
        MOV     CL, CS:SerialStateTable[BX].NEXTSTATE
        MOV     CurrentState, CL
        
PerformAction:                          ;do the actions
        MOV     AL, CH                  ;get token value and pass as argument
        MOV     AH, DH                  ;also pass the token type as an argument
                                        ;  for certain functions (SetError)
        CALL    CS:SerialStateTable[BX].ACTION;do the action specified in table

ParseSerialCharEnd:
        MOV     AL, ErrorVal            ;return the error value in AX
        XOR     AH, AH                  ;extend AL to AX (clear AH)

        POP     DX                      ;restore registers
        POP     CX
        POP     BX

        RET

ParseSerialChar     ENDP

; GetSerialToken
;
; Description:        This procedure performs table lookups for the token type and
;                     value based on the character passed in.
;
; Operational         
; Description:        Lookup the TokenType from the TokenTypeTable using char as
;                     an index. Lookup the TokenValue from the TokenValueTable
;                     using char as an index. Return TokenType and TokenValue.
;
; Arguments:          char - AL - ASCII character to be parsed
;
; Return Values:      TokenType - AH - type of the token (char) passed
;                     TokenVal - AL - value of the token (char) passed
; Global Variables:   None.
; Shared Variables:   None.
; Local Variables:    TokenType - type of the token (char) passed
;                     TokenVal - value of the token (char) passed 
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  AX, flags.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

GetSerialToken      PROC    NEAR

        PUSH    BX          ;preserve register

GetSerialTokenInit:
        AND     AL, TOKEN_MASK          ;mask high bit of token
        MOV     AH, AL                  ;preserve its value in AH

SerialTokenTypeLookup:                  ;get the token type
        MOV     BX, OFFSET(SerialTokenTypeTable);prepare to XLAT from
                                                ;  SerialTokenTypeTable
        XLAT    CS:SerialTokenTypeTable ;call XLAT with token type in AL
        XCHG    AH, AL                  ;token type in AH, character in AL

SerialTokenValueLookup:
        MOV     BX, OFFSET(SerialTokenValueTable);prepare to XLAT from
                                                 ;  SerialTokenValueTable
        XLAT    CS:SerialTokenValueTable;call XLAT with token type in AL

GetSerialTokenEnd:
        POP     BX          ;restore register

        RET

GetSerialToken      ENDP

; ValidCommandChar
;
; Description:        Takes the passed value of the command and sets CommandFlag
;                     according to the value. CommandFlag will be used later to
;                     determine which function is called when the string has been
;                     fully parsed.
;
; Operational         
; Description:        Set CommandFlag to Command (passed argument).
;
; Arguments:          Command - AL -  value that corresponds to the command that
;                                     is being parsed (see CommandFlag for
;                                     possible values)
;
; Return Values:      
; Global Variables:   None.
; Shared Variables:   CommandFlag - DS - set to a value corresponding to the
;                                        desired command (COM_ABS_SPEED for setting
;                                        absolute speed, COM_REL_SPEED for setting
;                                        relative speed, COM_SET_DIR for setting
;                                        direction, COM_SET_TUR_ANG for setting
;                                        turret angle, COM_SET_TUR_ELV for setting
;                                        turret elevation, COM_LASER_ON for turning
;                                        the laser on, COM_LASER_OFF for turning
;                                        the laser off) (w)
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

ValidCommandChar    PROC    NEAR

        MOV     CommandFlag, AL         ;set the command flag according to the
                                        ;  command argument that was passed
        RET

ValidCommandChar    ENDP

; ChangeSign
;
; Description:        Sets SignFlag according to the sign passed, which will changes 
;                     the sign of the number when it is passed as an argument for
;                     the command.
;
; Operational         
; Description:        Change SignFlag to Sign (argument passed).
;
; Arguments:          Sign - AL - SIGN_NEGATIVE if Num should be negative, 
;                                 SIGN_POSITIVE if Num should be positive
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   SignFlag - DS - SIGN_NEGATIVE if Num should be negative when
;                                     passed as an argument, SIGN_POSTIIVE if Num 
;                                     should be positive when passed as an argument,
;                                     SIGN_NONE means Num should be positive when
;                                     passed as an argument. (w)
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

ChangeSign      PROC    NEAR

        MOV     SignFlag, AL        ;set the sign flag according to sign argument
                                    ;  that was passed in AL
        RET

ChangeSign      ENDP

; AddDigit
;
; Description:        Add a digit to the number that will be passed as an argument
;                     with the command. The digit should be passed as an argument
;                     as the value of the digit (not as a character).
;
; Operational         
; Description:        Multiply Num by ten to shift the previous digits left. Then
;                     add the new digit to the one's place.
;
; Arguments:          digit - AL - digit that was passed into the serial parser
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   Num - DS - number that will be called as an argument with
;                                the corresponding command (w)
;                     CommandFlag - DS - set to a value corresponding to the
;                                        desired command (COM_ABS_SPEED for setting
;                                        absolute speed, COM_REL_SPEED for setting
;                                        relative speed, COM_SET_DIR for setting
;                                        direction, COM_SET_TUR_ANG for setting
;                                        turret angle, COM_SET_TUR_ELV for setting
;                                        turret elevation, COM_LASER_ON for turning
;                                        the laser on, COM_LASER_OFF for turning
;                                        the laser off) (r)
; Local Variables:    NumTemp - AX - temporarily stores Num so we can build it
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

AddDigit        PROC    NEAR

        PUSH    AX                  ;preserve registers
        PUSH    BX
        
        MOV     BL, AL              ;preserve the value of digit in BL
AddDigitUnsigned:                   ;add the digit to the number, check for 
                                    ;  overflow later
        XOR     DX, DX              ;clear DX in preparation for multiplication
        MOV     AX, Num             ;prepare to multiply Num by 10
        MOV     CX, 10
        MUL     CX                  ;shift all digits of Num to the left
        CMP     DX, 0               ;if we have overflowed past 16 bits, DX is set
        JNE     UnsignedOverflowError;DX is not zero, we have unsigned overflow
        ADD     AL, BL              ;add the digit to the one's place
        ADC     AH, 0               ;propagate low byte carry into high byte
        JC      UnsignedOverflowError;check if we have carry (unsigned overflow)
                                    ;we have no unsigned overflow, so set absolute
                                    ;  speed is guaranteed to work w/ this arg
                                    
                                    ;NOTE: we don't need this code if we assume
                                    ;  we only want signed values, even for
                                    ;  absolute speed
        ;CMP     CommandFlag, COM_ABS_SPEED;check if we are setting absolute speed
        ;JE      NoOverflow          ;no overflow because absolute speed uses
                                    ;  an unsigned argument
        ;JMP    CheckSignFlagDigit  ;now check if we have signed overflow

CheckSignFlagDigit:                 ;check if we have a sign, if we have a positive
                                    ;  sign or no sign, check for positive overflow, 
                                    ;  and if we have a negative sign, check for 
                                    ;  negative overflow
        CMP     SignFlag, SIGN_NEGATIVE;check if we have a negative sign
        JE      CheckNegativeOverflow;we have a negative sign, check for positive
                                     ;  overflow
        ;JNE    CheckPositiveOverflow;we have a positive sign, check for positive
                                     ;  overflow

CheckPositiveOverflow:              ;we have positive overflow if the number is
                                    ;  greater than MAX_POS_16_NUM
        CMP     AX, MAX_POS_16_NUM
        ;JA     PositiveOverflowError;we have positive overflow, send an error
        JBE     NoOverflow          ;no overflow, just assign the number

PositiveOverflowError:              ;signed number was too large, we got positive
                                    ;  overflow
        MOV     ErrorVal, ERROR_POS_OVERFLOW;update ErrorVal with correct error
        MOV     CurrentState, ST_ERROR;update CurrentState to be the error state
        JMP     AddDigitEnd         ;don't assign number, just finish

CheckNegativeOverflow:              ;we have negative overflow if the number is
                                    ;  greater than ABS_MIN_NEG_16_NUM
        CMP     AX, ABS_MIN_NEG_16_NUM
        ;JA     NegativeOverflowError;we have negative overflow, send an error
        JBE     NoOverflow          ;no overflow, just assign the number

NegativeOverflowError:              ;signed number was too small, we got negative
                                    ;  overflow
        MOV     ErrorVal, ERROR_NEG_OVERFLOW;update ErrorVal with correct error
        MOV     CurrentState, ST_ERROR;update CurrentState to be the error state
        JMP     AddDigitEnd         ;don't assign number, just finish

UnsignedOverflowError:              ;unsigned number was too large, we got
                                    ;  unsigned overflow
        MOV     ErrorVal, ERROR_UNS_OVERFLOW;update ErrorVal with correct error
        MOV     CurrentState, ST_ERROR;update CurrentState to be the error state
        JMP     AddDigitEnd         ;don't assign number, just finish

NoOverflow:                         ;no negative, positive, or unsigned overflow
        MOV     Num, AX             ;set Num to our new number

AddDigitEnd:
        POP     BX                  ;restore registers
        POP     AX

        RET

AddDigit        ENDP

; CallCommand
;
; Description:        Calls the command when the entire string has been parsed and
;                     is a valid command. The function uses the CommandFlag to
;                     determine which function to call and calls the function with
;                     the appropriate argument. Sends new status of Trike over
;                     serial.
;
; Operational         
; Description:        Negate the number if SignFlag is SIGN_NEGATIVE. Call the
;                     appropriate function with appropriate arguments depending on
;                     the CommandFlag. Sends status over serial.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   CommandFlag - DS - set to a value corresponding to the
;                                        desired command (COM_ABS_SPEED for setting
;                                        absolute speed, COM_REL_SPEED for setting
;                                        relative speed, COM_SET_DIR for setting
;                                        direction, COM_SET_TUR_ANG for setting
;                                        turret angle, COM_SET_TUR_ELV for setting
;                                        turret elevation, COM_LASER_ON for turning
;                                        the laser on, COM_LASER_OFF for turning
;                                        the laser off) (r)
;                     SignFlag - DS - SIGN_NEGATIVE if Num should be negative when
;                                     passed as an argument, SIGN_POSTIIVE if Num 
;                                     should be positive when passed as an argument,
;                                     SIGN_NONE means Num should be positive when
;                                     passed as an argument. (r)
;                     Num - DS - number that will be called as an argument with
;                                the corresponding command (r/w)
; Local Variables:    CurSpeed - current speed of the RoboTrike (used when setting
;                                relative speed)
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision
;     12/8/15   Dennis Shim     added SendStatus

CallCommand     PROC    NEAR
        
        PUSH    AX                  ;preserve register
        PUSH    BX 

CheckSignFlagCall:                  ;check if the number should be negative 
        CMP     SignFlag, SIGN_NEGATIVE
        ;JE     NumSignNegative     ;num should be negative
        JNE     LookupCommand       ;num is positive, now lookup the command

NumSignNegative:                    ;make num negative
        NEG     Num
        ;JMP    LookupCommand       ;now lookup the command 

LookupCommand:                      ;lookup the appropriate function based on
                                    ;  CommandFlag - assumes CommandFlag is a
                                    ;  valid command
        MOV     BL, CommandFlag     ;prepare to lookup correct jump point
        XOR     BH, BH              ;clear BH to extend BL to BX
        JMP     CS:SerialCommandTable[BX];jump to the appropriate label based on
                                         ;  which command

;value of the command char was 'S', so we set the absolute speed based on the
;  number that was passed. Pass Num as an argument in AX and DRIVE_ANGLE_UNCHANGED
;  as an argument BX to SetMotorSpeed. This will change the drive speed to Num
;  and leave the angle unchanged.
CallSetAbsoluteSpeed:
        MOV     AX, Num             ;pass Num as speed argument in AX
        MOV     BX, DRIVE_ANGLE_UNCHANGED;pass DRIVE_ANGLE_UNCHANGED as angle
                                         ;  argument in BX so angle doesn't change
        CALL    SetMotorSpeed       ;SetMotorSpeed(Num, DRIVE_ANGLE_UNCHANGED)
        JMP     CallCommandEnd      ;we are done, return

;value of the command char was 'V', so we set the relative speed based on the
;  number that was passed. Pass Num + GetMotorSpeed() as an argument in AX and 
;  DRIVE_ANGLE_UNCHANGED as an argument BX to SetMotorSpeed. This will change the 
;  drive speed to Num + GetMotorSpeed() and leave the angle unchanged. If the
;  speed overflows, then set the speed to the highest possible value. If it 
;  underflows (negative), then set the speed to the stop value.
CallSetRelativeSpeed:
        CALL    GetMotorSpeed       ;get the current motor speed in AX
        MOV     CX, AX              ;save the low bit of AX so we can preserve
                                    ;  all bits of precision
        AND     CX, HIGH_BIT_MASK
        MOV     DX, Num             ;save the low bit of Num so we can preserve
                                    ;  all bits of precision
        AND     DX, HIGH_BIT_MASK
        SHR     AX, 1               ;motor speed is unsigned, SHR 1 to make it
                                    ;  signed so we can perform signed operations
        SAR     Num, 1              ;need to scale down Num because when we make
                                    ;  current speed signed, we scale it down
                                    ;  but keep the sign bit the same
        ADD     AX, Num             ;add the relative motor speed to the current
                                    ;  motor speed to get the new motor speed 
                                    ;NOTE: this operation can only have positive
                                    ;  overflow because the current speed is
                                    ;  only positive (ie [-32768, 32767] + )
        JO      RelativeSpeedOverflow;positive overflow, set speed to maximum
        CMP     AX, 0               ;check if we got a negative result
        JL      RelativeSpeedUnderflow;speed is negative, just set to    stop

        SHL     AX, 1               ;speed is correct, and positive, shift left
                                    ;  to make it unsigned (still positive)
        ADD     AX, CX              ;add in the precision bits that we saved
        ADD     AX, DX              ;  earlier
        JC      RelativeSpeedOverflow;we have a case where after adding the
                                     ;  precision bits (unsigned), the value
                                     ;  was too large (>FFFFH) and wrapped to
                                     ;  zero, which is a positive overflow in
                                     ;  our case

        CMP     AX, DRIVE_SPEED_UNCHANGED;we have a case where the drive speed
                                         ;  we want to pass is exactly
                                         ;  DRIVE_SPEED_UNCHANGED, which is a
                                         ;  positive overflow in our case
        JE      RelativeSpeedOverflow;set the speed to the highest value because
                                     ;  we have overflow
        JMP     CallSetRelativeSpeedCommand;now call the function

RelativeSpeedOverflow:              ;positive overflow, set speed to highest value
        MOV     AX, DRIVE_SPEED_MAX
        JMP     CallSetRelativeSpeedCommand;now call the function

RelativeSpeedUnderflow:             ;positive overflow, set speed to lowest value
        MOV     AX, DRIVE_SPEED_STOP
        ;JMP    CallSetRelativeSpeedCommand;now call the function

CallSetRelativeSpeedCommand:
        MOV     BX, DRIVE_ANGLE_UNCHANGED;don't want angle to change
        CALL    SetMotorSpeed       ;SetMotorSpeed(Num + GetMotorSpeed(),
                                    ;                DRIVE_ANGLE_UNCHANGED)
        JMP     CallCommandEnd      ;we are done, return

;value of the command char was 'D', so we set the direction based on the argument
;  that was passed. Pass Num + GetMotorDirection() as an argument in BX and
;  DRIVE_SPEED_UNCHANGED as an argument in AX to SetMotorSpeed. This sets the
;  direction to Num + GetMotorDirection() and leaves the speed unchanged. If the
;  Num is greater than MAX_POS_16_NUM - 360, which makes it possible to overflow
;  when adding with GetMotorDirection() (returns values from 0-359), then 360 is
;  subtracted from Num to guarantee that the number cannot overflow.
CallSetDirection:
        MOV     BX, Num                 ;prepare to pass the angle argument in BX
        CMP     BX, MAX_POS_16_NUM - 360;check if Num is greater than
                                        ;  MAX_POS_16_NUM - 359, which makes it
                                        ;  possible to overflow
        JG      ReduceToEquivalentAngle ;get the lower equivalent angle to prevent
                                        ;  possible overflow
        JLE     CallSetDirectionCommand ;can't overflow, call the function with
                                        ;  correct arguments

ReduceToEquivalentAngle:                ;possible for angle to overflow, just
                                        ;  subtract 360 to get an equivalent
                                        ;  angle that can't overflow
        SUB     BX, 360
        ;JMP    CallSetDirectionCommand ;call the function with correct arguments

CallSetDirectionCommand:
        CALL    GetMotorDirection       ;puts the motor direction in AX, which
                                        ;  is unsigned but from 0-359
        ADD     BX, AX                  ;add the relative direction to current
                                        ;  motor direction to get the new angle
                                        ;  without possibility of overflow
        CMP     BX, DRIVE_ANGLE_UNCHANGED;check if our number is equal to
                                         ;  DRIVE_ANGLE_UNCHANGED (possible if
                                         ;  angle is 0 and we want to change it
                                         ;  by DRIVE_ANGLE_UNCHANGED)
        JE      IncreaseToEquivalentAngle;it is equal to DRIVE_ANGLE_UNCHANGED,
                                         ;  add 360 to get an equivalent angle
        JNE     SendGoodAngle           ;angle is good, send it
                                         
IncreaseToEquivalentAngle:              ;since it is possible for the result to
                                        ;  be DRIVE_ANGLE_UNCHANGED, just add
                                        ;  360 to eliminate that possibility
        ADD     BX, 360
        ;JNE    SendGoodAngle           ;angle is good, send it

SendGoodAngle:                          ;angle is good, just send it
        MOV     AX, DRIVE_SPEED_UNCHANGED;don't change the drive speed
        CALL    SetMotorSpeed           ;SetMotorSpeed(DRIVE_SPEED_UNCHANGED,
                                        ;                Num + GetMotorDirection())
        JMP     CallCommandEnd          ;we are done, return

;value of the command char was 'T', so we set the turret angle based on the argument
;  that was passed. If no sign was passed (SignFlag = SIGN_NONE), pass Num as
;  an absolute angle argument in SetTurretAngle. If a sign was passed, pass Num
;  as a relative angle argument in SetRelTurretAngle.
CallSetTurretAngle:
        MOV     AX, Num             ;prepare to pass AX as the angle argument
        CMP     SignFlag, SIGN_NONE ;check if we were passed a sign
        ;JE     SetAbsoluteTurretAngle;no sign passed, set absolute angle
        JNE     SetRelativeTurretAngle;sign passed, set relative angle

SetAbsoluteTurretAngle:             ;no sign was passed, call SetTurretAngle
                                    ;  and pass absolute angle in AX
        CALL    SetTurretAngle      ;SetTurretAngle(Num)
        JMP     CallSetTurretAngleEnd;and finish

SetRelativeTurretAngle:             ;sign was passed, call SetTurretAngle and
                                    ;  pass relative angle in AX
        CALL    SetRelTurretAngle   ;SetRelTurretAngle(Num)
        ;JMP    CallSetTurretAngleEnd;and finish

CallSetTurretAngleEnd:
        JMP     CallCommandEnd      ;we are done, return

;value of the command char was 'E', so we set the turret elevation angle based
;  on the argument that was passed. Pass Num as the absolute angle argument to
;  SetTurretElevation in AX.
CallSetTurretElevation:
        MOV     AX, Num             ;pass Num as angle argument in AX
        CALL    SetTurretElevation  ;SetTurretElevation(Num)
        JMP     CallCommandEnd      ;we are done, return

;value of the command char was 'F', so we set the laser on. Pass LASER_ON as the 
;  argument to SetLaser.
CallSetLaserOn:
        MOV     AX, LASER_ON        ;pass LASER_ON as argument in AX
        CALL    SetLaser            ;SetLaser(LASER_ON)
        JMP     CallCommandEnd      ;we are done, return

;value of the command char was 'F', so we set the laser on. Pass LASER_OFF as the 
;  argument to SetLaser.
CallSetLaserOff:
        MOV     AX, LASER_OFF       ;pass LASER_OFF as argument in AX
        CALL    SetLaser            ;SetLaser(LASER_OFF)
        JMP     CallCommandEnd      ;we are done, return

CallCommandEnd:
        CALL    SendStatus          ;after we have changed something, send the
                                    ;  new status over serial

        POP     BX                  ;restore registers
        POP     AX 

        RET


CallCommand     ENDP

; SetError
;
; Description:        Sets ErrorVal depending on which error was received. The
;                     error received is passed as an argument.
;
; Operational         
; Description:        Set ErrorVal to Error (passed argument).
;
; Arguments:          Error - AH - value of the error that occurred
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   ErrorVal - DS - ERROR_NONE if there is no error, otherwise
;                                     there is an error identified by the code. (w)
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

SetError        PROC    NEAR

        PUSH    AX
        
        INC     AH                  ;increment Error to get the correct error value
                                    ;  (we do this because ERROR_NONE is zero,
                                    ;   and TokenValue is zero-indexed, so we
                                    ;   need to TokenValues to be one-indexed)
        MOV     ErrorVal, AH        ;set ErrorVal to the passed argument

        POP     AX
        RET

SetError        ENDP

; DoNOP
;
; Description:        Does nothing.
;
; Operational         
; Description:        Do nothing.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   None.
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

DoNOP           PROC    NEAR
        NOP

        RET

DoNOP           ENDP

; SerialStateTable
;
; Description:      State transition table for the Serial Parser state machine.
;                   Each entry is a struc that contains the next state and the
;                   actions corresponding to that transition. The rows are
;                   associated with the current state and the column with the
;                   input type. Access the table by performing a table lookup
;                   using SerialStateTable[(CurrentState * NUM_TOKEN_TYPES +
;                                            TokenType) * SIZE TRANSITION_ENTRY]
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

TRANSITION_ENTRY        STRUC           ;structure used to define table
    NEXTSTATE   DB      ?               ;the next state for the transition
    ACTION      DW      ?               ;action for the transition
TRANSITION_ENTRY      ENDS


;define a macro to make table a little more readable
;macro just does an offset of the action routine entries to build the STRUC
%*DEFINE(TRANSITION(nxtst, act))  (
    TRANSITION_ENTRY< %nxtst, OFFSET(%act) >
)


SerialStateTable    LABEL   TRANSITION_ENTRY

    ;Current State = ST_INITIAL                         Input Token Type
    %TRANSITION(ST_ABS_SPEED, ValidCommandChar)         ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_REL_SPEED, ValidCommandChar)         ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_DIRECTION, ValidCommandChar)         ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_TUR_ANGLE, ValidCommandChar)         ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_TUR_ELEV, ValidCommandChar)          ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_LASER_ON, ValidCommandChar)          ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_LASER_OFF, ValidCommandChar)         ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_NEG_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_POS_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_DIGIT
    %TRANSITION(ST_INITIAL, DoNOP)                      ;TOKEN_EOS
    %TRANSITION(ST_INITIAL, DoNOP)                      ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_ABS_SPEED                       Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_NEG_SIGN
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_POS_SIGN
    %TRANSITION(ST_DIGIT, AddDigit)                     ;TOKEN_DIGIT
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_EOS
    %TRANSITION(ST_ABS_SPEED, DoNOP)                    ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_REL_SPEED                       Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_NEG_SIGN
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_POS_SIGN
    %TRANSITION(ST_DIGIT, AddDigit)                     ;TOKEN_DIGIT
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_EOS
    %TRANSITION(ST_REL_SPEED, DoNOP)                    ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_DIRECTION                       Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_NEG_SIGN
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_POS_SIGN
    %TRANSITION(ST_DIGIT, AddDigit)                     ;TOKEN_DIGIT
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_EOS
    %TRANSITION(ST_DIRECTION, DoNOP)                    ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_TUR_ANGLE                       Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_NEG_SIGN
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_POS_SIGN
    %TRANSITION(ST_DIGIT, AddDigit)                     ;TOKEN_DIGIT
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_EOS
    %TRANSITION(ST_TUR_ANGLE, DoNOP)                    ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_TUR_ELEV                        Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_NEG_SIGN
    %TRANSITION(ST_SIGN, ChangeSign)                    ;TOKEN_POS_SIGN
    %TRANSITION(ST_DIGIT, AddDigit)                     ;TOKEN_DIGIT
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_EOS
    %TRANSITION(ST_TUR_ELEV, DoNOP)                     ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_LASER_ON                        Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_NEG_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_POS_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_DIGIT
    %TRANSITION(ST_END, CallCommand)                    ;TOKEN_EOS
    %TRANSITION(ST_LASER_ON, DoNOP)                     ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_LASER_OFF                       Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_NEG_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_POS_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_DIGIT
    %TRANSITION(ST_END, CallCommand)                    ;TOKEN_EOS
    %TRANSITION(ST_LASER_OFF, DoNOP)                    ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_SIGN                            Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_NEG_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_POS_SIGN
    %TRANSITION(ST_DIGIT, AddDigit)                     ;TOKEN_DIGIT
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_EOS
    %TRANSITION(ST_SIGN, DoNOP)                         ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_DIGIT                           Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_NEG_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_POS_SIGN
    %TRANSITION(ST_DIGIT, AddDigit)                     ;TOKEN_DIGIT
    %TRANSITION(ST_END, CallCommand)                    ;TOKEN_EOS
    %TRANSITION(ST_DIGIT, DoNOP)                        ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_ERROR                           Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_NEG_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_POS_SIGN
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_DIGIT
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_EOS
    %TRANSITION(ST_ERROR, DoNOP)                        ;TOKEN_BLANK
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_END                             Input Token Type
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_CMD_CHAR_S
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_CMD_CHAR_V
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_CMD_CHAR_D
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_CMD_CHAR_T
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_CMD_CHAR_E
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_CMD_CHAR_F
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_CMD_CHAR_O
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_NEG_SIGN
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_POS_SIGN
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_DIGIT
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_EOS
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_BLANK
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_OTHER


; Serial Token Tables
;
; Description:      This creates the tables of token types and token values
;                   used by the serial parser. Each entry corresponds to the
;                   token type and the token value for a character. Macros are
;                   used to actually build the tables, SerialTokenTypeTable for
;                   token types and SerialTokenValueTable for token values. Each
;                   table can be accessed using an XLAT with the char in AL
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

%*DEFINE(TABLE)  (
        %TABENT(TOKEN_OTHER, 0)     ;<null>
        %TABENT(TOKEN_OTHER, 1)     ;SOH
        %TABENT(TOKEN_OTHER, 2)     ;STX
        %TABENT(TOKEN_OTHER, 3)     ;ETX
        %TABENT(TOKEN_OTHER, 4)     ;EOT
        %TABENT(TOKEN_OTHER, 5)     ;ENQ
        %TABENT(TOKEN_OTHER, 6)     ;ACK
        %TABENT(TOKEN_OTHER, 7)     ;BEL
        %TABENT(TOKEN_OTHER, 8)     ;backspace
        %TABENT(TOKEN_BLANK, 9)     ;TAB (white space)
        %TABENT(TOKEN_OTHER, 10)    ;new line
        %TABENT(TOKEN_OTHER, 11)    ;vertical tab
        %TABENT(TOKEN_OTHER, 12)    ;form feed
        %TABENT(TOKEN_EOS, 0)       ;carriage return
        %TABENT(TOKEN_OTHER, 14)    ;SO
        %TABENT(TOKEN_OTHER, 15)    ;SI
        %TABENT(TOKEN_OTHER, 16)    ;DLE
        %TABENT(TOKEN_OTHER, 17)    ;DC1
        %TABENT(TOKEN_OTHER, 18)    ;DC2
        %TABENT(TOKEN_OTHER, 19)    ;DC3
        %TABENT(TOKEN_OTHER, 20)    ;DC4
        %TABENT(TOKEN_OTHER, 21)    ;NAK
        %TABENT(TOKEN_OTHER, 22)    ;SYN
        %TABENT(TOKEN_OTHER, 23)    ;ETB
        %TABENT(TOKEN_OTHER, 24)    ;CAN
        %TABENT(TOKEN_OTHER, 25)    ;EM
        %TABENT(TOKEN_OTHER, 26)    ;SUB
        %TABENT(TOKEN_OTHER, 27)    ;escape
        %TABENT(TOKEN_OTHER, 28)    ;FS
        %TABENT(TOKEN_OTHER, 29)    ;GS
        %TABENT(TOKEN_OTHER, 30)    ;AS
        %TABENT(TOKEN_OTHER, 31)    ;US
        %TABENT(TOKEN_BLANK, ' ')   ;space (white space)
        %TABENT(TOKEN_OTHER, '!')   ;!
        %TABENT(TOKEN_OTHER, '"')   ;"
        %TABENT(TOKEN_OTHER, '#')   ;#
        %TABENT(TOKEN_OTHER, '$')   ;$
        %TABENT(TOKEN_OTHER, 37)    ;percent
        %TABENT(TOKEN_OTHER, '&')   ;&
        %TABENT(TOKEN_OTHER, 39)    ;'
        %TABENT(TOKEN_OTHER, 40)    ;open paren
        %TABENT(TOKEN_OTHER, 41)    ;close paren
        %TABENT(TOKEN_OTHER, '*')   ;*
        %TABENT(TOKEN_POS_SIGN, SIGN_POSITIVE);+  (positive sign)
        %TABENT(TOKEN_OTHER, 44)    ;,
        %TABENT(TOKEN_NEG_SIGN, SIGN_NEGATIVE);-  (negative sign)
        %TABENT(TOKEN_OTHER, '.')   ;.
        %TABENT(TOKEN_OTHER, '/')   ;/
        %TABENT(TOKEN_DIGIT, 0)     ;0  (digit)
        %TABENT(TOKEN_DIGIT, 1)     ;1  (digit)
        %TABENT(TOKEN_DIGIT, 2)     ;2  (digit)
        %TABENT(TOKEN_DIGIT, 3)     ;3  (digit)
        %TABENT(TOKEN_DIGIT, 4)     ;4  (digit)
        %TABENT(TOKEN_DIGIT, 5)     ;5  (digit)
        %TABENT(TOKEN_DIGIT, 6)     ;6  (digit)
        %TABENT(TOKEN_DIGIT, 7)     ;7  (digit)
        %TABENT(TOKEN_DIGIT, 8)     ;8  (digit)
        %TABENT(TOKEN_DIGIT, 9)     ;9  (digit)
        %TABENT(TOKEN_OTHER, ':')   ;:
        %TABENT(TOKEN_OTHER, ';')   ;;
        %TABENT(TOKEN_OTHER, '<')   ;<
        %TABENT(TOKEN_OTHER, '=')   ;=
        %TABENT(TOKEN_OTHER, '>')   ;>
        %TABENT(TOKEN_OTHER, '?')   ;?
        %TABENT(TOKEN_OTHER, '@')   ;@
        %TABENT(TOKEN_OTHER, 'A')   ;A
        %TABENT(TOKEN_OTHER, 'B')   ;B
        %TABENT(TOKEN_OTHER, 'C')   ;C
        %TABENT(TOKEN_CMD_CHAR_D, COM_SET_DIR);D (set direction state)
        %TABENT(TOKEN_CMD_CHAR_E, COM_SET_TUR_ELV);E (set turret elevation state)
        %TABENT(TOKEN_CMD_CHAR_F, COM_LASER_ON);F (set laser on state)
        %TABENT(TOKEN_OTHER, 'G')   ;G
        %TABENT(TOKEN_OTHER, 'H')   ;H
        %TABENT(TOKEN_OTHER, 'I')   ;I
        %TABENT(TOKEN_OTHER, 'J')   ;J
        %TABENT(TOKEN_OTHER, 'K')   ;K
        %TABENT(TOKEN_OTHER, 'L')   ;L
        %TABENT(TOKEN_OTHER, 'M')   ;M
        %TABENT(TOKEN_OTHER, 'N')   ;N
        %TABENT(TOKEN_CMD_CHAR_O, COM_LASER_OFF);O (set laser off state)
        %TABENT(TOKEN_OTHER, 'P')   ;P
        %TABENT(TOKEN_OTHER, 'Q')   ;Q
        %TABENT(TOKEN_OTHER, 'R')   ;R
        %TABENT(TOKEN_CMD_CHAR_S, COM_ABS_SPEED);S (set absolute speed)
        %TABENT(TOKEN_CMD_CHAR_T, COM_SET_TUR_ANG);T (rotate turret angle state)
        %TABENT(TOKEN_OTHER, 'U')   ;U
        %TABENT(TOKEN_CMD_CHAR_V, COM_REL_SPEED);V (set relative speed state)
        %TABENT(TOKEN_OTHER, 'W')   ;W
        %TABENT(TOKEN_OTHER, 'X')   ;X
        %TABENT(TOKEN_OTHER, 'Y')   ;Y
        %TABENT(TOKEN_OTHER, 'Z')   ;Z
        %TABENT(TOKEN_OTHER, '[')   ;[
        %TABENT(TOKEN_OTHER, '\')   ;\
        %TABENT(TOKEN_OTHER, ']')   ;]
        %TABENT(TOKEN_OTHER, '^')   ;^
        %TABENT(TOKEN_OTHER, '_')   ;_
        %TABENT(TOKEN_OTHER, '`')   ;`
        %TABENT(TOKEN_OTHER, 'a')   ;a
        %TABENT(TOKEN_OTHER, 'b')   ;b
        %TABENT(TOKEN_OTHER, 'c')   ;c
        %TABENT(TOKEN_CMD_CHAR_D, COM_SET_DIR);d (set direction state)
        %TABENT(TOKEN_CMD_CHAR_E, COM_SET_TUR_ELV);e (set turret elevation state)
        %TABENT(TOKEN_CMD_CHAR_F, COM_LASER_ON);f (set laser on state)
        %TABENT(TOKEN_OTHER, 'g')   ;g
        %TABENT(TOKEN_OTHER, 'h')   ;h
        %TABENT(TOKEN_OTHER, 'i')   ;i
        %TABENT(TOKEN_OTHER, 'j')   ;j
        %TABENT(TOKEN_OTHER, 'k')   ;k
        %TABENT(TOKEN_OTHER, 'l')   ;l
        %TABENT(TOKEN_OTHER, 'm')   ;m
        %TABENT(TOKEN_OTHER, 'n')   ;n
        %TABENT(TOKEN_CMD_CHAR_O, COM_LASER_OFF);o (set laser off state)
        %TABENT(TOKEN_OTHER, 'p')   ;p
        %TABENT(TOKEN_OTHER, 'q')   ;q
        %TABENT(TOKEN_OTHER, 'r')   ;r
        %TABENT(TOKEN_CMD_CHAR_S, COM_ABS_SPEED);s (set absolute speed)
        %TABENT(TOKEN_CMD_CHAR_T, COM_SET_TUR_ANG);t (rotate turret angle state)
        %TABENT(TOKEN_OTHER, 'u')   ;u
        %TABENT(TOKEN_CMD_CHAR_V, COM_REL_SPEED);v (set relative speed state)
        %TABENT(TOKEN_OTHER, 'w')   ;w
        %TABENT(TOKEN_OTHER, 'x')   ;x
        %TABENT(TOKEN_OTHER, 'y')   ;y
        %TABENT(TOKEN_OTHER, 'z')   ;z
        %TABENT(TOKEN_OTHER, '{')   ;{
        %TABENT(TOKEN_OTHER, '|')   ;|
        %TABENT(TOKEN_OTHER, '}')   ;}
        %TABENT(TOKEN_OTHER, '~')   ;~
        %TABENT(TOKEN_OTHER, 127)   ;rubout
)

; token type table - uses first byte of macro table entry
%*DEFINE(TABENT(tokentype, tokenvalue))  (
        DB      %tokentype
)

SerialTokenTypeTable    LABEL   BYTE
        %TABLE

; token value table - uses second byte of macro table entry
%*DEFINE(TABENT(tokentype, tokenvalue))  (
        DB      %tokenvalue
)

SerialTokenValueTable   LABEL   BYTE
        %TABLE


; SerialCommandTable
;
; Description:      Table that allows for lookup of a different label that will
;                   send a command. Get the label by accessing the table using
;                   the CommandFlag as an index (since it is a word table, the
;                   index must be multiplied by two). Jump to the label to
;                   execute the code that actually calls the function. All of
;                   the labels and code are in CallCommand.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

SerialCommandTable  LABEL   WORD
        DW      CallSetAbsoluteSpeed    ;CommandFlag = COM_ABS_SPEED
        DW      CallSetRelativeSpeed    ;CommandFlag = COM_REL_SPEED
        DW      CallSetDirection        ;CommandFlag = COM_SET_DIR
        DW      CallSetTurretAngle      ;CommandFlag = COM_SET_TUR_ANG
        DW      CallSetTurretElevation  ;CommandFlag = COM_SET_TUR_ELV
        DW      CallSetLaserOn          ;CommandFlag = COM_LASER_ON
        DW      CallSetLaserOff         ;CommandFlag = COM_LASER_OFF


CODE    ENDS

; the data segment

DATA    SEGMENT PUBLIC  'DATA'

CurrentState        DB          ?   ;current state the serial FSM is in
ErrorVal            DB          ?   ;value of the error (ERROR_NONE if no error)
CommandFlag         DB          ?   ;value corresponds to the desired command
SignFlag            DB          ?   ;value corresponds to the sign of the number
Num                 DW          ?   ;number to pass as an argument

DATA    ENDS


        END