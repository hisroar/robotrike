        NAME    PARALLEL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   Parallel                                 ;
;                            Parallel Chip Functions                         ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains a function that initializes the parallel ports for the
; hardware of the RoboTrike. The functions included are:
;       InitParallel                - Initializes the 8255 parallel chip.
;
; Revision History:
;     11/12/15  Dennis Shim      initial revision

; local include files
$INCLUDE(PARALLEL.INC)          ; contains addresses and values for initializing
                                ; chip selects

CGROUP  GROUP   CODE

CODE    SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP


; InitParallel
;
; Description:       Initialize the Parallel Chip on the 8255.
;
; Operation:         Write the initial values to the Parallel Chip control register.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            PARALLELval is output to (PARALLEL_BASE_ADDR + CTRL_OFFSET).
;                       
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: AX, DX
;
; Revision History:
;     11/12/15  Dennis Shim      initial revision

InitParallel    PROC    NEAR
                PUBLIC  InitParallel


        MOV     DX, PARALLEL_BASE_ADDR + CTRL_OFFSET;setup to write to parallel chip
                                                    ;  CTRL register
        MOV     AX, PARALLELval
        OUT     DX, AL          ;write PARALLELval to parallel chip CTRL register


        RET                     ;done so return

InitParallel    ENDP

CODE    ENDS

        END
