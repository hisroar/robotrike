        NAME    MOTOR

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Motor                                   ;
;                                Motor Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains functions that allow a user to read in inputs from the
; keypad. The functions included are:
;       MotorInit         - Initializes the variables that control motor speed, 
;                           angle, pulse width counter, pulse width, and motor
;                           direction array. (GLOBAL)
;       SetMotorSpeed     - Takes in a speed and an angle and computes pulse
;                           widths for each motor. (GLOBAL)
;       GetMotorSpeed     - Returns the absolute speed of the motor. (GLOBAL)
;       GetMotorDirection - Returns the direction of the RoboTrike, in degrees
;                           from the forward direction. (GLOBAL)
;       SetLaser          - Set the laser status to on or off. (GLOBAL)
;       GetLaser          - Returns the status of the laser (on or off). (GLOBAL)
;       MotorPWM          - Uses pulse width modulation in order to turn the
;                           motors on and off, depending on the pulse widths and
;                           the pulse width counter. (GLOBAL)
;
; Revision History:
;     11/5/15  Dennis Shim      initial revision
;     11/9/15  Dennis Shim      fixed motor PWM (port C garbage, JG vs JA), and
;                               fixed GetMotorSpeed (scaling)
;     12/9/15   Dennis Shim     updated to work using a shared folder

; local include files
$INCLUDE(../SHARED/GENERAL.INC) ;contains general constants and addresses
$INCLUDE(../SHARED/MOTOR.INC)   ;contains constants and addresses specifically
                                ;  for motors
$INCLUDE(PARALLEL.INC)          ;contains constants and addresses for outputting
                                ;  to the parallel chip

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE    SEGMENT PUBLIC 'CODE'



        ASSUME  CS:CGROUP, DS:DGROUP
        
        
        ; external functions/tables
        EXTRN   Sin_Table:WORD              ;table of values of sin at each degree
        EXTRN   Cos_Table:WORD              ;table of values of cos at each degree
        EXTRN   ForceXTable:WORD            ;table of x-components of force vectors
        EXTRN   ForceYTable:WORD            ;table of y-components of force vectors 
        EXTRN   MotorOutputOn:BYTE          ;table of masks for motor on output
        EXTRN   MotorOutputOff:BYTE         ;table of masks for motor off output
        EXTRN   MotorOutputForward:BYTE     ;table of masks for motor forward output
        EXTRN   MotorOutputReverse:BYTE     ;table of masks for motor reverse output

; MotorInit
;
; Description:        This function initializes the variables that control the
;                     motor speed and angle. It initializes the DriveSpeed to
;                     MOTOR_SPEED_STOP, DriveAngle to MOTOR_ANGLE_FORWARD, and
;                     LaserStatus to LASER_OFF. Also initializes the
;                     PulseWidthCounter, PulseWidths, and MotorDir.
;
; Operational         
; Description:        Set the DriveSpeed to MOTOR_SPEED_STOP (speed when RoboTrike)
;                     is stopped. Set the DriveAngle to MOTOR_ANGLE_FORWARD
;                     (straight ahead of the RoboTrike). Set the LaserStatus to
;                     LASER_OFF (off). Set the PulseWidthCounter to zero. Loop
;                     through each motor and set each of the pulse widths to zero
;                     and each direction to forward.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   DriveSpeed  - DS - speed that the motors move the RoboTrike
;                                        (between DRIVE_SPEED_STOP and
;                                        DRIVE_SPEED_MAX). (W)
;                     DriveAngle  - DS - angle relative to straight ahead that the
;                                        motors move the RoboTrike (between
;                                        DRIVE_ANGLE_FORWARD and DRIVE_ANGLE_MAX).
;                                        (W)
;                     LaserStatus - DS - whether the laser is on or off (LASER_OFF
;                                        if off, LASER_ON if on). (W)
;                     PulseWidthCounter - DS - counter that keeps track of when
;                                              the motors should be turned on based
;                                              on the pulse widths. (W)
;                     PulseWidths - DS - array of pulse widths that specify how
;                                        long each drive motor should be turned on.
;                                        (W)
;                     MotorDir    - DS - array that specifies if each motor is
;                                        running forward or in reverse (W)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
; 
; Registers Changed:  None.
;
; Revision History:
;     11/12/15  Dennis Shim     initial revision

MotorInit       PROC        NEAR
                PUBLIC      MotorInit

        PUSH    SI          ;preserve the registers

        MOV     DriveSpeed, DRIVE_SPEED_STOP    ;initialize the Trike to stopped
        MOV     DriveAngle, DRIVE_ANGLE_FORWARD ;initialize the motor direction
                                                ;  to forward
        MOV     LaserStatus, LASER_OFF      ;initialize the laser to off
        MOV     PulseWidthCounter, PULSE_COUNTER_MAX - 1;initialize the pulse width 
                                                        ;  counter to the highest 
                                                        ;  value so we can decrement

        MOV     SI, 0       ;set up the loop index in SI
ClearPulseWidthsLoopBody:   ;loop through PulseWidths MotorDir and clear each value
        MOV     PulseWidths[SI], 0      ;zero the pulse width so the motor does
                                        ;  not move initially
        MOV     MotorDir[SI], FORWARD   ;set each motor to be running in the
                                        ;  forward direction
        INC     SI          ;increment the loop index

ClearPulseWidthsLoop:       ;check if we have reached the end of PulseWidths
        CMP     SI, NUM_MOTORS  ;length of PulseWidths is NUM_MOTORS, check if
                                ;  we have reached it
        JL      ClearPulseWidthsLoopBody;we haven't reached the end, keep looping
        ;JGE    MotorInitEnd    ;we've reached the end, finish looping
             
MotorInitEnd:
        POP     SI          ;restore the registers

        RET

MotorInit       ENDP

; SetMotorSpeed
;
; Description:        Set the drive speed to the speed passed as an argument and
;                     the drive angle to the angle passed as an argument. The
;                     passed speed is the speed at which the RoboTrike will move.
;                     The maximum possible speed is DRIVE_SPEED_MAX. To indicate
;                     the speed should not be changed, pass DRIVE_SPEED_UNCHANGED
;                     as the speed. The speed is passed in as the absolute speed
;                     that the RoboTrike should move, and is unsigned. The angle
;                     is passed in degrees and is signed. DRIVE_ANGLE_FORWARD is
;                     the forward direction relative to the RoboTrike's orientation.
;                     To indicate the angle should not be changed, pass
;                     DRIVE_ANGLE_UNCHANGED as the angle. The procedure changes the
;                     PulseWidths according the speed and angle passed.
;
; Operational         
; Description:        Check if the speed should be changed. If it should be,
;                     set DriveSpeed to the speed divided by two, which allows
;                     for signed arithmetic in the future. Check if the angle
;                     should be changed. If it should be, take the modulo of
;                     DRIVE_ANGLE_MAX + 1 to get the angle in the range
;                     DRIVE_ANGLE_FORWARD and DRIVE_ANGLE_MAX. Compute the 
;                     x-component and y-components of the velocity vector. Loop
;                     through the motors and compute the pulse width by taking
;                     the dot product of the force vector and the velocity vector.
;                     Check if the pulse width is negative. If so, take its
;                     absolute value and set the motor's direction to reverse. 
;                     Otherwise set the motor's direciton to forward. After
;                     computing the pulse width for each motor, return.
;
; Arguments:          Speed - AX - the absolute speed that the RoboTrike runs at.
;                                  Ranges between DRIVE_SPEED_STOP and DRIVE_SPEED_MAX.
;                                  Pass DRIVE_SPEED_UNCHANGED as the speed to leave
;                                  the speed unchanged.
;                     Angle - BX - the angle in degrees that the RoboTrike should
;                                  move at with respect to the forward direction of
;                                  the RoboTrike. Pass DRIVE_ANGLE_UNCHANGED as the
;                                  angle to leave the angle unchanged.
;
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   DriveSpeed  - DS - speed that the motors move the RoboTrike
;                                        (between DRIVE_SPEED_STOP and
;                                        DRIVE_SPEED_MAX). (R)
;                     DriveAngle  - DS - angle relative to straight ahead that the
;                                        motors move the RoboTrike (between
;                                        DRIVE_ANGLE_FORWARD and DRIVE_ANGLE_MAX).
;                                        (R)
;                     PulseWidths - DS - array of pulse widths that specify how
;                                        long each drive motor should be turned on.
;                                        (W)
;                     MotorDir    - DS - array that specifies if each motor is
;                                        running forward or in reverse (W)
;
; Local Variables:    v_x - CX - temporarily stores the x-component of the velocity
;                                vector
;                     v_y - SI - temporarily stores the y-component of the velocity
;                                vector
;                     index - BX - loop and array index for the PulseWidth loop
;                     pulseTemp - DI - temporarily stores the product of the
;                                      x-components of the velocity and force vectors
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
; 
; Registers Changed:  none.
;
; Revision History:
;     11/12/15  Dennis Shim     initial revision

SetMotorSpeed       PROC        NEAR
                    PUBLIC      SetMotorSpeed

        PUSHA               ;preserve registers

CheckSpeedUnchanged:
        CMP     AX, DRIVE_SPEED_UNCHANGED;check if we should change the speed
        JE      CheckAngleUnchanged     ;leave the speed unchanged, now
                                        ;  check if we need to change the angle
        ;JNE    ChangeSpeed             ;speed should be changed

ChangeSpeed:
        SHR     AX, 1                   ;scale the speed down by one bit so we
                                        ;  can use it in signed multiplication
        MOV     DriveSpeed, AX          ;set the speed according to the argument
        ;JMP    CheckAngleUnchanged     ;now check if we need to change the angle

CheckAngleUnchanged:
        CMP     BX, DRIVE_ANGLE_UNCHANGED;check if we should change the angle
        JE      ComputeVelocityVectors  ;leave the angle unchanged, now start to
                                        ;  set the pulse widths
        ;JNE    ChangeAngle             ;angle should be changed

ChangeAngle:                            ;we want the angle to be in the range 0
                                        ;  to DRIVE_ANGLE_MAX, to do this we take
                                        ;  the angle MODULO DRIVE_ANGLE_MAX + 1
        MOV     AX, BX                  ;setup AX to prepare to take Angle
                                        ;  MODULO DRIVE_ANGLE_MAX + 1
        CWD                             ;extend AX to DX|AX so we get the
                                        ;  remainder in DX after the IDIV
        MOV     CX, DRIVE_ANGLE_MAX + 1 ;temporarily store the divisor in CX
        IDIV    CX                      ;divide to get the remainder in DX
        CMP     DX, 0                   ;check if the angle is negative
        ;JL     AngleNegative           ;the angle is negative, we want the
                                        ;  corresponding positive angle
        JGE     SetAngle                ;the angle is positive, just move it to
                                        ;  DriveAngle

AngleNegative:
        ADD     DX, DEGREES_IN_CIRCLE   ;get the equivalent positive angle by
                                        ;  adding DEGREES_IN_CIRCLE
        ;JMP    SetAngle                ;now move the angle to DriveAngle

SetAngle:
        MOV     DriveAngle, DX          ;set the angle to its positive value that
                                        ;  is in the range DRIVE_ANGLE_FORWARD
                                        ;  and DRIVE_ANGLE_MAX
        ;JMP    ComputeVelocityVectors  ;done setting angle argument, now start
                                        ;  to set the pulse widths

ComputeVelocityVectors:                 ;compute values of v_x = v*cos(theta)
                                        ;  and of v_y = v*sin(theta)
                                        ;  these same values are used to calculate
                                        ;  the pulse widths for each motor
                                        ;note: the vectors will have 2 signed bits
                                        ;  due to Q0.15 * Q0.15 multiplication
        MOV     BX, DriveAngle          ;move the drive angle (theta) to BX to 
                                        ;  prepare for lookup of cos(theta) or
                                        ;  sin(theta)
        SHL     BX, 1                   ;multiply by 2 because we have a word
                                        ;  table instead of a byte table

        MOV     AX, DriveSpeed          ;move the drive speed to AX to setup for
                                        ;  IMUL for v*cos(theta)
        IMUL    WORD PTR Cos_Table[BX]  ;multiply v_x = v*cos(theta), in DX|AX
        MOV     CX, DX                  ;truncate v_x to 16 bits and store v_x
                                        ;  (Q0.14) in SI

        MOV     AX, DriveSpeed          ;move the drive speed to AX to setup for
                                        ;  IMUL for v*cos(theta)
        IMUL    WORD PTR Sin_Table[BX]  ;multiply v_y = v*sin(theta), in DX|AX
        MOV     SI, DX                  ;truncate v_y to 16 bits and store v_y
                                        ;  (Q0.14) in SI

        ;JMP    SetPulseWidthLoopInit   ;done computing velocity vectors, now 
                                        ;  just have to compute pulse widths

SetPulseWidthLoopInit:
        XOR     BX, BX                  ;start the loop index at zero

SetPulseWidthLoopBody:
        SHL     BX, 1                   ;multiply the index by two because we are
                                        ;  accessing a table of words
                                        ;first, do the x-component part of the 
                                        ;  dot product
        MOV     AX, WORD PTR ForceXTable[BX];get the x-component of the force vector
                                            ;  and put it in AX to prepare for IMUL
        IMUL    CX                      ;multiply F_x * v_x to get the first
                                        ;  component of the dot product
        MOV     DI, DX                  ;truncate the product to 16 bits and
                                        ;  temporarily store the product in DI
                                        ;  (product is Q0.13)

                                        ;now, do the y-component part of the dot
                                        ;  product
        MOV     AX, WORD PTR ForceYTable[BX];get the y-component of the force vector
                                            ;  and put it in AX to prepare for IMUL
        IMUL    SI                      ;multiply F_x * v_x to get the first
                                        ;  component of the dot product
        
        ADD     DX, DI                  ;truncate the product to 16 bits, so
                                        ;  product is Q0.13, then add the two
                                        ;  products, F_x * v_x + F_y * v_x
        SAL     DX, 2                   ;the sum of the products is currently
                                        ;  Q0.13, so we shift left by two to
                                        ;  remove extra sign bits, use SAL to 
                                        ;  preserve the sign bits when we shift
                                        ;  DH now contains the pulse width

        SHR     BX, 1                   ;divide the index by two to restore it to
                                        ;  its original value so we can access
                                        ;  PulseWidths and MotorDir, arrays of bytes
        CMP     DH, 0                   ;check the sign of the pulse width
        ;JL     PulseWidthNegative      ;the pulse width is negative, take the
                                        ;  absolute value and write REVERSE
                                        ;  to MotorDir
        JGE     PulseWidthPositive      ;the pulse width is positive, write
                                        ;  FORWARD to MotorDir

PulseWidthNegative:                     ;PulseWidth value is negative
        NEG     DH                      ;take the absolute value of the PulseWidth
                                        ;  so we can write it to the array later
        MOV     MotorDir[BX], REVERSE   ;this motor is running in reverse direction
        JMP     SetPulseWidthLoopEnd    ;set the PulseWidths array and increment
                                        ;  the index 

PulseWidthPositive:
        MOV     MotorDir[BX], FORWARD   ;this motor is running in forward direction
        ;JMP    SetPulseWidthLoopEnd    ;set the PulseWidths array and increment
                                        ;  the index 

SetPulseWidthLoopEnd:
        MOV     PulseWidths[BX], DH     ;truncate DX to DH, so the pulse width
                                        ;  is a Q0.7 number (~1 percent precision)
        INC     BX                      ;increment the loop counter, which is
                                        ;  the number of the motor whose 
                                        ;  pulse width we are computing

SetPulseWidthLoop:
        CMP     BX, NUM_MOTORS          ;we want to finish the loop after we get
                                        ;  the pulse width of the last motor
        JL      SetPulseWidthLoopBody   ;we still have more motors, keep looping
        ;JGE    SetMotorSpeedEnd        ;we've finished getting pulse widths,
                                        ;  stop looping

SetMotorSpeedEnd:
        POPA                ;restore the registers

        RET

SetMotorSpeed       ENDP

; GetMotorSpeed
;
; Description:        The function is called with no arguments and returns the
;                     current absolute speed setting of the RoboTrike in AX. The
;                     speed will be in the range DRIVE_SPEED_STOP and DRIVE_SPEED_MAX / 2.
;
; Operational         
; Description:        Return DriveSpeed, which is the current speed setting of the
;                     RoboTrike. It is returned in AX.
;
; Arguments:          None.
;
; Return Values:      DriveSpeed - AX - the current absolute speed of the RoboTrike.
;                                       The speed will be in the range DRIVE_SPEED_STOP
;                                       and DRIVE_SPEED_MAX / 2. 
; Global Variables:   None.
; Shared Variables:   DriveSpeed  - DS - speed that the motors move the RoboTrike
;                                        (between DRIVE_SPEED_STOP and
;                                        DRIVE_SPEED_MAX). (R)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
; 
; Registers Changed:  AX.
;
; Revision History:
;     11/12/15  Dennis Shim     initial revision
;     12/9/15   Dennis Shim     fixed 0-DRIVE_SPEED_MAX (comment)

GetMotorSpeed       PROC        NEAR
                    PUBLIC      GetMotorSpeed

        MOV     AX, DriveSpeed      ;return the DriveSpeed in AX
        SHL     AX, 1               ;DEPRECATED:
                                    ;scale up by 2 because we divided by two to
                                    ;  to make it a Q0.15 number, need to return
                                    ;  a value between 0 and DRIVE_SPEED_MAX
        RET

GetMotorSpeed       ENDP

; GetMotorDirection
;
; Description:        The function is called with no arguments and returns the
;                     current angle of the RoboTrike relative to the forward
;                     direction in AX. The angle will be in the range
;                     DRIVE_ANGLE_FORWARD and DRIVE_ANGLE_MAX.
;
; Operational         
; Description:        Return DriveAngle, which is the current angle setting of the
;                     RoboTrike. The angle will be in the range DRIVE_ANGLE_FORWARD
;                     and DRIVE_ANGLE_MAX.
;
; Arguments:          None.
;
; Return Values:      DriveAngle - AX - the current angle of the RoboTrike relative
;                                       to the forward direction. The angle will
;                                       be in the range DRIVE_ANGLE_FORWARD and
;                                       DRIVE_ANGLE_MAX.
; Global Variables:   None.
; Shared Variables:   DriveAngle - DS - angle relative to straight ahead that the
;                                       motors move the RoboTrike (between
;                                       DRIVE_ANGLE_FORWARD and DRIVE_ANGLE_MAX).
;                                       (R)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
; 
; Registers Changed:  AX.
;
; Revision History:
;     11/12/15  Dennis Shim     initial revision

GetMotorDirection   PROC        NEAR
                    PUBLIC      GetMotorDirection

        MOV     AX, DriveAngle      ;return the DriveAngle in AX
        RET

GetMotorDirection   ENDP

; SetLaser
;
; Description:        The function is called with a single argument that indicates
;                     whether the RoboTrike laser should be turned on or off. If
;                     the argument passed is LASER_OFF, then the laser is turned
;                     off. If the argument passed is not LASER_OFF, then the laser
;                     is turned on.
;
; Operational         
; Description:        If the argument passed is LASER_OFF, then the laser status
;                     is set to LASER_OFF. Otherwise, it is set to LASER_ON.
;
; Arguments:          Laser - AX - the status the laser should be set to. If not
;                                  LASER_OFF, the laser is turned on, if LASER_OFF,
;                                  the laser is turned off.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   LaserStatus - DS - whether the laser is on or off (LASER_OFF
;                                        if off, LASER_ON if on). (W)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
; 
; Registers Changed:  none.
;
; Revision History:
;     11/12/15  Dennis Shim     initial revision

SetLaser        PROC        NEAR
                PUBLIC      SetLaser
        
        CMP     AX, LASER_OFF       ;check if the laser is off
        ;JE     LaserStatusOff      ;the laser is off, set LaserStatus to off
        JNE     LaserStatusOn       ;the laser is on, set LaserStatus to on

LaserStatusOff:
        MOV     LaserStatus, LASER_OFF  ;change the laser status to be off
        JMP     SetLaserEnd             ;  and finish

LaserStatusOn:
        MOV     LaserStatus, LASER_ON   ;change the laser status to be on
        ;JMP    SetLaserEnd             ;  and finish
        
SetLaserEnd:
        RET

SetLaser        ENDP

; GetLaser
;
; Description:        The function is called with no arguments and returns the
;                     current status of the RoboTrike's laser in AX. If the laser
;                     is off, then LASER_OFF is returned. If the laser is on, then
;                     LASER_ON is returned.
;
; Operational         
; Description:        Return LaserStatus in AX. If the laser is off, return
;                     LASER_OFF, otherwise return LASER_ON.
;
; Arguments:          None.
;
; Return Values:      LaserStatus - AX - LASER_OFF if the laser is off, LASER_ON
;                                        if the laser is on
; Global Variables:   None.
; Shared Variables:   LaserStatus - DS - whether the laser is on or off (LASER_OFF
;                                        if off, LASER_ON if on). (R)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
; 
; Registers Changed:  AX.
;
; Revision History:
;     11/12/15  Dennis Shim     initial revision

GetLaser        PROC        NEAR
                PUBLIC      GetLaser

        MOV     AX, LaserStatus     ;return the laser status in AX

        RET

GetLaser        ENDP

; MotorPWM
;
; Description:        The procedure uses pulse width modulation in order to turn
;                     on the motors. It only turns on a motor if the
;                     PulseWidthCounter is less than the PulseWidth of the motor,
;                     then decrements PulseWidthCounter. Depending on whether the
;                     motors are on and whether they are going in forward or reverse
;                     direction, different masks are applied to the output (refer
;                     to tables MotorOutputOn, MotorOutputOff, MotorOutputForward,
;                     MotorOutputReverse). Masks are also applied to the output 
;                     depending on whether the laser is on. The output is sent
;                     to the parallel chip.
;
; Operational
; Description:        Clear OutputTemp to start. Loop through each motor. Check
;                     if the PulseWidthCounter is less than the PulseWidth for
;                     that motor. If it is, the motor is on, otherwise, the
;                     motor is off. If it is on, use the MotorOutputOn table
;                     to get the mask for that motor, and OR that mask with
;                     OutputTemp. If the motor is off, then use the MotorOuputOff
;                     table to get the mask for that motor, and OR that mask
;                     with OutputTemp. If the motor is on, check whether the
;                     RoboTrike is going forward or reverse, and get a mask
;                     from either MotorOutputForward or MotorOutputReverse
;                     accordingly, and OR that mask onto OutputTemp. Check if
;                     the laser is on. Depending if it is on or off, get the mask
;                     and OR it onto OutputTemp. Output OutputTemp to the
;                     Parallel Chip B port. Decrement the PulseWidthCounter, and
;                     wrap if necessary.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   PulseWidthCounter - DS - counter that keeps track of when
;                                              the motors should be turned on based
;                                              on the pulse widths. (W)
;                     PulseWidths - DS - array of pulse widths that specify how
;                                        long each drive motor should be turned on.
;                                        (R)
;                     MotorDir    - DS - array that specifies if each motor is
;                                        running forward or in reverse (R)
;
; Local Variables:    OutputTemp - CX - value to output to the parallel port
;
; Inputs:             None.
; Output:             Sends OutputTemp to the Parallel Chip at Port B. The output
;                     is a number of masks OR'ed together.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
; 
; Registers Changed:  None.
;
; Revision History:
;     11/12/15  Dennis Shim     initial revision
;     12/9/15   Dennis Shim     fixed JG vs JA, fixed writing garbage to port C

MotorPWM        PROC        NEAR
                PUBLIC      MotorPWM
        
        PUSHA               ;preserve the registers

        XOR     CL, CL      ;clear CL so we can use it to output to parallel    
        XOR     SI, SI      ;start with the first motor, set loop index to zero 

                            ;loop through each of the motors and check if it
                            ;  should be turned on (on if counter < width),
                            ;  prepare to write output to the parallel port
MotorPWMLoopBody:
                                    ;check whether we should turn on the motor
                                    ;  if PulseWidthCounter is less than the
                                    ;  pulse width of the motor
        MOV     AL, PulseWidths[SI] ;AL temporarily stores the PulseWidth so we
                                    ;  can do a CMP
        CMP     PulseWidthCounter, AL;check if the motor is on or off
        JA      MotorOff            ;PulseWidthCounter is greater than the pulse
                                    ;  width, so the motor is off
        ;JBE    MotorOn             ;PulseWidthCounter is less than the pulse
                                    ;  width, so the motor is on

MotorOn:                            ;the motor is on, write the correct bits
        MOV     AX, SI              ;prepare to XLAT, get the motor that we want
                                    ;  the mask for
        MOV     BX, OFFSET(MotorOutputOn);get the address of the table for when
                                         ;  we want to turn the motor on
        XLAT    CS:MotorOutputOn    ;lookup the mask that we will use to change
                                    ;  the output so the motor turns on
        OR      CL, AL              ;apply the appropriate mask so the motor
                                    ;  will turn on
        JMP     MotorPWMLoopBodyDirection;the motor is on, write its direction 
                                         ;  bit to the output 

MotorOff:                           ;the motor is off, write the correct bits
        MOV     AX, SI              ;prepare to XLAT, get the motor that we want
                                    ;  the mask for
        MOV     BX, OFFSET(MotorOutputOff);get the address of the table for when
                                          ;  we want to turn the motor off
        XLAT    CS:MotorOutputOff   ;lookup the mask that we will use to change
                                    ;  the output so the motor turns off
        OR      CL, AL              ;apply the appropriate mask so the motor
                                    ;  will turn off
        JMP    MotorPWMLoopBodyEnd  ;finished writing output for this motor,
                                    ;  don't need to write direction bit
                                    ;  check if there are more motors to do

MotorPWMLoopBodyDirection:
        CMP     MotorDir[SI], REVERSE;check if the motor is in reverse
        ;JE     MotorReverse        ;motor is in reverse, write corresponding
                                    ;  bits
        JNE     MotorForward        ;motor is going forward, write corresponding
                                    ;  bits

MotorReverse:
        MOV     AX, SI              ;prepare to XLAT, get the motor that we want
                                    ;  the mask for
        MOV     BX, OFFSET(MotorOutputReverse);get the address of the table for
                                              ;  when we want to reverse the motor
        XLAT    CS:MotorOutputReverse;lookup the mask that we will use to change
                                     ;  the output to reverse the motor
        OR      CL, AL              ;apply the appropriate mask to reverse the
                                    ;  motor
        JMP     MotorPWMLoopBodyEnd ;finished writing output for this motor,
                                    ;  check if there are more motors to do

MotorForward:
        MOV     AX, SI              ;prepare to XLAT, get the motor that we want
                                    ;  the mask for
        MOV     BX, OFFSET(MotorOutputForward);get the address of the table for
                                              ;  when we want to make the motor
                                              ;  go forward
        XLAT    CS:MotorOutputForward;lookup the mask that we will use to change
                                     ;  the output make the motor go forward
        OR      CL, AL              ;apply the appropriate mask to make the motor
                                    ;  go forward
        ;JMP    MotorPWMLoopBodyEnd ;finished writing output for this motor,
                                    ;  check if there are more motors to do

MotorPWMLoopBodyEnd:
        INC     SI                  ;increment the loop index to write output
                                    ;  for the next motor
        ;JMP    MotorPWMLoop        ;check if we have more motors

MotorPWMLoop:
        CMP     SI, NUM_MOTORS      ;check if we've written output for each of
                                    ;  the motors
        JL      MotorPWMLoopBody    ;we still have motors left, keep looping
        ;JGE    DecrementPulseWidthCounter;we've done all the motors, decrement
                                          ;  the counter and finish

DecrementPulseWidthCounter:
        DEC     PulseWidthCounter   ;decrement the counter for the next time the
                                    ;  function is called
        CMP     PulseWidthCounter, 0;check if we need to wrap the counter
        ;JL     WrapCounter         ;the counter is less than zero, so we need
                                    ;  to wrap the counter to the highest value
        JGE     LaserCheck          ;the counter is still valid, don't wrap it

WrapCounter:                        ;the counter went too low, so we need to set
                                    ;  it to the highest value so we can keep
                                    ;  decrementing later
        MOV     PulseWidthCounter, PULSE_COUNTER_MAX - 1

LaserCheck:
        CMP     LaserStatus, LASER_ON;check if the laser is on
        ;JE     TurnLaserOn         ;laser is on, write the correct output
        JNE     TurnLaserOff        ;laser is off, write the correct output

TurnLaserOn:
        OR      CL, LASER_ON_MASK   ;use LASER_ON_MASK to change the output so the
                                    ;  laser turns on
        JMP     MotorPWMEnd         ;output to the parallel chip

TurnLaserOff:
        OR      CL, LASER_OFF_MASK  ;user LASER_OFF_MASK to change the output so
                                    ;  the laser turns off
        JMP     MotorPWMEnd         ;output to the parallel chip

MotorPWMEnd:
        MOV     DX, PARALLEL_BASE_ADDR + GROUPB_OFFSET;get the parallel chip
                                                      ;  group B address
        MOV     AL, CL              ;move the output to AL to prepare for OUT          
        OUT     DX, AL              ;output the output we have been generating 
                                    ;  in AL to the parallel port

        POPA                        ;restore the registers

        RET

MotorPWM        ENDP

; Procedures that are not implemented

SetRelTurretAngle   PROC        NEAR
                    PUBLIC      SetRelTurretAngle
        NOP
        RET
SetRelTurretAngle   ENDP

SetTurretAngle      PROC        NEAR
                    PUBLIC      SetTurretAngle
        NOP
        RET
SetTurretAngle      ENDP

GetTurretAngle      PROC        NEAR
                    PUBLIC      GetTurretAngle
        NOP
        RET
GetTurretAngle      ENDP

SetTurretElevation  PROC        NEAR
                    PUBLIC      SetTurretElevation
        NOP
        RET
SetTurretElevation  ENDP

GetTurretElevation  PROC        NEAR
                    PUBLIC      GetTurretElevation
        NOP
        RET
GetTurretElevation  ENDP

CODE    ENDS


;the data segment

DATA    SEGMENT PUBLIC  'DATA'

DriveSpeed          DW      ?       ;contains the absolute speed of the RoboTrike
DriveAngle          DW      ?       ;contains the angle relative to forward the
                                    ;  RoboTrike should move at
LaserStatus         DW      ?       ;contains whether the laser is on or off
PulseWidthCounter   DB      ?       ;counter used in pulse width modulation
PulseWidths         DB      (NUM_MOTORS)    DUP     (?);contains the duration of
                                                       ;  each motor pulse
MotorDir            DB      (NUM_MOTORS)    DUP     (?);contains the direction of
                                                       ;  each motor

DATA    ENDS

        END
