        NAME    TIMER

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Timer                                   ;
;                               Timer Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains functions that initialize the timer and handle the interrupts
; that it generates. The functions included are:
;       Timer0EventHandler      - The event handler for the timer. Outputs a
;                                 segment on the LED display.
;       InitTimer0              - Initializes Timer0 to send interrupts at a
;                                 frequency of 1 KHz.
;       InstallTimer0EH         - Writes the address of the TimerEventHandler
;                                 at the Timer0 interrupt.
;
; Revision History:
;     11/13/15  Dennis Shim      initial revision
;     12/8/15   Dennis Shim      removed redundant PUSH/POP in EH
;     12/9/15   Dennis Shim      updated to work using a shared folder

; local include file
$INCLUDE(../SHARED/GENERAL.INC) ;contains general constants and addresses
$INCLUDE(TIMER.INC)             ;contains the addresses and values necessary to 
                                ;  initialize the timer
$INCLUDE(../SHARED/INTERRPT.INC);contains the addresses and values necessary to 
                                ;  get interrupts from the processor
CGROUP  GROUP   CODE


CODE    SEGMENT PUBLIC 'CODE'

; external functions
        EXTRN   MotorPWM:NEAR       ;turns the motors on or off according to
                                    ;  pulse width modulation.

        ASSUME  CS:CGROUP


; Timer0EventHandler
;
; Description:       This procedure is the event handler for the timer
;                    interrupt. It calls the MotorPWM function, which
;                    uses pulse width modulation to turn the motors on
;                    and off. Timer0EventHandler is called at 30 *
;                    2^(bit resolution) KHz. It sends a TimerEOI when
;                    done.
;
; Operation:         Preserve the registers. Call MotorPWM to determine
;                    whether or not to turn on the motors. Send a timer
;                    EOI and return.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            An EOI is sent to the interrupt controller.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision
;     11/5/15   Dennis Shim      updated for keypad
;     12/8/15   Dennis Shim      removed redundant PUSH/POP

Timer0EventHandler      PROC    NEAR
                        PUBLIC  Timer0EventHandler

        PUSH    AX                      ;preserve the registers
        PUSH    DX

        CALL    MotorPWM                ;turn the motors on or off

EndTimer0EventHandler:                  ;done taking care of the timer
                                        ;send the EOI to the interrupt controller
        MOV     DX, INTCtrlrEOI         ;get the address of the controller
        MOV     AX, TimerEOI            ;get the value of the EOI specific to timers
        OUT     DX, AL                  ;output the EOI to the interrupt controller

        POP     DX                      ;restore the registers
        POP     AX


        IRET                            ;and return (Event Handlers end with IRET not RET)

Timer0EventHandler       ENDP

; InitTimer0
;
; Description:       Initialize the 80188 Timer #0. Timer 0 is initialized
;                    to generate interrupts every COUNTS_PER_MS milliseconds.
;                    The interrupt controller is also initialized to allow the
;                    timer interrupts. Timer #0 counts COUNTS_PER_MS to generate
;                    the interrupts at 1 KHz.
;
; Operation:         The appropriate values are written to the timer control
;                    registers in the PCB. Also, the timer count register
;                    is reset to zero.  Finally, the interrupt controller is
;                    setup to accept timer interrupts and any pending
;                    interrupts are cleared by sending a TimerEOI to the
;                    interrupt controller.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: AX, DX
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision
;     11/13/15  Dennis Shim      updated for motor

InitTimer0      PROC    NEAR
                PUBLIC  InitTimer0
                                ;initialize Timer #0 for COUNTS_PER_MS ms interrupts
        MOV     DX, Tmr0Count   ;get the address of the timer counter
        XOR     AX, AX          ;initialize the count register to 0
        OUT     DX, AL          ;write the count to the timer counter

        MOV     DX, Tmr0MaxCntA ;get the address for the max count A for Timer0
        MOV     AX, COUNTS_PER_PWM_CYCLE;setup max count for milliseconds per segment
        OUT     DX, AL          ;  count so the motor PWM is called at 
                                ;  30 * 2^(bit resolution) Hz 

        MOV     DX, Tmr0Ctrl    ;get the address of the Timer0 control register
        MOV     AX, Tmr0CtrlVal ;setup the control register using the correct
        OUT     DX, AL          ;  control value.

                                ;initialize interrupt controller for timers
        MOV     DX, INTCtrlrCtrl;get the address of the controller
        MOV     AX, INTCtrlrCVal;setup the interrupt control register by setting
        OUT     DX, AL          ;  the control register with the correct value

        MOV     DX, INTCtrlrEOI ;get the address of the EOI controller
        MOV     AX, TimerEOI    ;send a timer EOI (to clear out controller)
        OUT     DX, AL


        RET                     ;done so return


InitTimer0       ENDP


; InstallTimer0EH
;
; Description:       Install the event handler for the timer 0 interrupt.
;
; Operation:         Writes the address of the timer 0 event handler to the
;                    appropriate interrupt vector.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: flags, AX, ES
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision
;     11/13/15  Dennis Shim      updated for motor

InstallTimer0EH     PROC    NEAR
                    PUBLIC  InstallTimer0EH

        XOR     AX, AX          ;clear ES (interrupt vectors are in segment 0)
        MOV     ES, AX
                                ;store the vector
                                ;the offset goes in the lower two bytes of the
                                ;  vector, and the segment goes in the upper two
                                ;  bytes of the vector.
        MOV     ES: WORD PTR (IVEC_SIZE * Tmr0Vec), OFFSET(Timer0EventHandler)
        MOV     ES: WORD PTR (IVEC_SIZE * Tmr0Vec + IVEC_HALF_SIZE), SEG(Timer0EventHandler)


        RET                     ;all done, return


InstallTimer0EH  ENDP

CODE    ENDS

        END
