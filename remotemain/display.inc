;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  DISPLAY.INC                               ;
;                               Display Functions                            ;
;                                   EE/CS 51                                 ;
;								  Include File 								 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for displaying segments on the
; LED display.
;
; Revision History:
;     10/29/15  Dennis Shim		initial revision

; constants

BUFFER_SIZE 		EQU 	16 	    ; segBuffer has max size 16 bytes
BLANKseg 			EQU 	00000000B 		; blank segment

LED_ADDR 			EQU 	0000H 	; LED display address
LED_14ADDR          EQU     0008H   ; address for the 14-segment LED display
NUM_DIGITS 			EQU 	8 		; number of digits in display
DISP_STRING_LENGTH  EQU 	NUM_DIGITS + 1;length of display string passed to display