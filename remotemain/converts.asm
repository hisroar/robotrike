        NAME    CONVERTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   CONVERTS                                 ;
;                             Conversion Functions                           ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains functions for converting 16-bit binary values to strings.
; The functions included are:
;       Dec2String          - convert the value to a string containing its
;                             decimal value. (PUBLIC)
;       Hex2String          - convert the value to a string containing its
;                             hexadecimal value. (PUBLIC)
;
; Revision History:
;       10/15/15    Dennis Shim     initial revision
;       10/29/15    Dennis Shim     updated comments and code
;       12/4/15     Dennis Shim     added handling for case when n = 
;       12/9/15     Dennis Shim     updated to work using a shared folder


; local include files
$INCLUDE(../SHARED/GENERAL.INC)     ;contains general constants and addresses
$INCLUDE(CONVERTS.INC)              ; contains constants for ASCII null and
                                    ; the largest possible power of 10 and 16

CGROUP  GROUP   CODE

CODE    SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP


; Dec2String
;
; Description:          This function converts the 16-bit binary value (n) passed
;                       to it to decimal (signed) and store it as a string. The
;                       string will contain the <null> terminated decimal
;                       representation of the value in ASCII. The string
;                       is stored at the address SI. The number is passed
;                       through AX by value. The string will be less than or
;                       equal to 5 character digits plus a sign (at most 6
;                       digits), and is not zero-padded.
;       
; Operation:            The function first checks if the number (n) is negative.
;                       If it is, it writes a negative sign at the address (a).
;                       Otherwise, it writes a positive sign at the address.
;                       Then, the function loops until the power of 10 is zero.
;                       It takes the first digit by dividing by the highest power
;                       of 10 (pwr10) and writes that digit. It only writes the
;                       digit if other digits have been written, or that digit
;                       is greater than zero. It takes the rest of the number by
;                       taking the modulo pwr10, and then decreases pwr10 by a
;                       factor of 10. The string is then null terminated.
;
; Arguments:            n - AX - binary value to convert to decimal string.
;                       a - SI - address of the first location to write the string.
; Return Values:        none.
;
; Global Variables:     none.
; Shared Variables:     none.
; Local Variables:      n (DI)      - binary value to convert.
;                       a (SI)      - address to write string to.
;                       digit (AX)  - computed digit.
;                       first (BX)  - 0 if first digit hasn't been written, otherwise not zero
;                       pwr10 (CX)  - current power of 10 being computed.
;                                               
; Input:                none.
; Output:               none.
; Error Handling:       none.
; Algorithms:           Algorithm to extract decimal digits by dividing by the highest
;                       power of 10 possible was used.
; Data Structures:      none.
;
; Registers Changed:    none.
;
; Revision History:
;       10/15/15    Dennis Shim     initial revision
;       10/29/15    Dennis Shim     updated comments and code
;       10/31/15    Dennis Shim     removed zero padding
;       12/4/15     Dennis Shim     added n = 0 handling

Dec2String      PROC        NEAR
                PUBLIC      Dec2String


Dec2StringInit:                             ;initialization
        PUSHA                               ;preserve the registers
		
		CMP 	AX, 0						;if n is zero, just write a zero and
											;  return because otherwise code will
											;  not write a zero
		;JE		DecNumIsZero 				;n is zero, write zero and return
		JNE 	DecNumIsntZero 				;n isn't zero, do normal algorithm

DecNumIsZero: 								;n is zero, so just write an ASCII
											;  '0' to the string and increment
											;  SI to the next string index so
											;  the ASCII_NULL can be written
		MOV 	BYTE PTR ES:[SI], '0'
		INC 	SI
		JMP  	TerminateString 			;and finish

DecNumIsntZero:			
        MOV     DI, AX                      ;DI = n
        MOV     BX, 0                       ;first digit hasn't been written
        MOV     CX, MAXpwr10                ;start with MAXpwr10
        ;JMP    AssignSign                  ;now check if n is negative

AssignSign:                                 ;check if n is negative
        CMP     DI, 0                       ;check if n < 0
        ;JL     IsNegative                  ;otherwise add the sign
        JGE     Dec2StringLoopBody          ;if n >= 0 don't add the sign

IsNegative:                                 ;n is negative
        MOV     BYTE PTR ES:[SI], '-'       ;write '-' to the address in SI
        NEG     DI                          ;take the absolute value of n
        INC     SI                          ;increment the offset to the next address
        ;JMP     Dec2StringLoopBody         ;begin getting digits

Dec2StringLoopBody:                         ;get a digit
        MOV     AX, DI                      ;set up for n/pwr10
        MOV     DX, 0                       ;
        DIV     CX                          ;digit (AX) = n/pwr10
        CMP     AX, 0                       ;write the digit if it's greater than zero
        JG      WriteDecDigit               ;write the digit
        CMP     BX, 0                       ;check if we've written the first digit
        JE      Dec2StringLoopDecr          ;we haven't, don't write a digit
        ;JNE    WriteDecDigit               ;we've already written a digit, write the next
        
WriteDecDigit:                              ;write the digit
        ADD     AL, '0'                     ;set AL to digit's ASCII value
        MOV     BYTE PTR ES:[SI], AL        ;write (digit + '0') to the address
        INC     SI                          ;increment the offset to the next address
        MOV     BX, 1                       ;we've written a digit, first = 1  
        ;JMP    Dec2StringLoopDecr          ;setup for the next iteration

Dec2StringLoopDecr:                         ;setup by changing n and pwr10
        MOV     DI, DX                      ;now change n to be n MODULO pwr10
        MOV     AX, CX                      ;setup for pwr10 = pwr10/10
        MOV     CX, 10                      ;
        MOV     DX, 0                       ;
        DIV     CX                          ;AX = pwr10/10
        MOV     CX, AX                      ;pwr10 is now updated to pwr10/10
        ;JMP    EndDec2StringLoop           ;done getting this digit

EndDec2StringLoop:                          ;loop getting digits of n
        CMP     CX, 0                       ;check if pwr10 > 0
        JG      Dec2StringLoopBody          ;if so, get the next digit
        ;JLE    TerminateString             ;otherwise, no more digits, terminate string

TerminateString:                            ;terminate the string and return
        MOV     BYTE PTR ES:[SI], ASCII_NULL;put the null character at the address
        POPA                                ;restore the registers
        RET                                 ;and return

Dec2String  ENDP




; Hex2String
;
; Description:          This function converts the 16-bit binary value (n) passed
;                       to it to hexadecimal (unsigned) and store it as a string.
;                       The string will contain the <null> terminated hexadecimal
;                       representation of the value in ASCII. The string is stored
;                       at the address SI. The number is passed through AX by
;                       value. The string will be less than or equal to 4 digits
;                       and will not be zero-padded (at most 4 digits).
;       
; Operation:            The function loops until the power of 16 is zero.
;                       It takes the first digit by dividing by the highest power
;                       of 16 (pwr16) and writes that digit. It only writes the
;                       digit if other digits have been written, or that digit
;                       is greater than zero. It takes the rest of the number by
;                       taking the modulo pwr16, and then decreases pwr16 by a
;                       factor of 16. The string is then null terminated.
;
; Arguments:            n - AX - binary value to convert to hexadecimal string.
;                       a - SI - address of the first location to write the string.
; Return Values:        none.
;
; Global Variables:     none.
; Shared Variables:     none.
; Local Variables:      n (DI)      - binary value to convert.
;                       a (SI)      - address to write string to.
;                       first (BX)  - 1 if first digit has been written, 0 otherwise
;                       digit (AX)  - computed digit.
;                       pwr16 (CX)  - current power of 16 being computed.
;
; Input:                none.
; Output:               none.
; Error Handling:       none.
; Algorithms:           Algorithm to extract hexadecimal digits by dividing by the highest
;                       power of 16 possible was used.
; Data Structures:      none.
;
; Registers Changed:    none.
;
; Revision History:
;       10/15/15    Dennis Shim     initial revision
;       10/29/15    Dennis Shim     updated comments and code
;       10/31/15    Dennis Shim     removed zero padding
;       12/4/15     Dennis Shim     added n = 0 handling

Hex2String      PROC        NEAR
                PUBLIC      Hex2String

Hex2StringInit:                             ;initialization
        PUSHA                               ;preserve the registers
        
        CMP 	AX, 0						;if n is zero, just write a zero and
											;  return because otherwise code will
											;  not write a zero
		;JE		HexNumIsZero 				;n is zero, write zero and return
		JNE 	HexNumIsntZero 				;n isn't zero, do normal algorithm

HexNumIsZero: 								;n is zero, so just write an ASCII
											;  '0' to the string and increment
											;  SI to the next string index so
											;  the ASCII_NULL can be written
		MOV 	BYTE PTR ES:[SI], '0'
		INC 	SI
		JMP  	TerminateStringHex 			;and finish

HexNumIsntZero:	
        
        MOV     DI, AX                      ;BX = n
        MOV     BX, 0                       ;first digit hasn't been written
        MOV     CX, MAXpwr16                ;start with MAXpwr16
        ;JMP    Hex2StringLoopBodyDigit     ;now begin the loop

Hex2StringLoopBodyDigit:                    ;get a digit
        MOV     DX, 0                       ;set up for n/pwr16
        MOV     AX, DI                      ;
        DIV     CX                          ;digit (AX) = n/pwr16
        CMP     AX, 0                       ;write the digit if it's greater than zero
        JG      WriteHexDigit               ;write the digit
        CMP     BX, 0                       ;check if we've written the first digit
        JE      Hex2StringLoopDecr          ;we haven't, don't write a digit
        ;JNE    WriteHexDigit               ;we've already written a digit, write the next

WriteHexDigit:
        MOV     BX, 1                       ;we're writing a digit, first = 1  
        CMP     AX, 10                      ;check if digit < 10
        ;JL     DigitLessThanTen            ;digit is less than 10
        JGE     DigitGreaterThanTen         ;digit is greater or equal to 10

DigitLessThanTen:                           ;digit < 10, write by adding '0'
        ADD     AL, '0'                     ;set AL to digit's ASCII value
        JMP     Hex2StringLoopBodyWrite     ;continue execution of loop

DigitGreaterThanTen:                        ;digit >=10, write by adding 'A'-10
        ADD     AL, 'A' - 10                ;set AL to digit's ASCII value
        ;JMP    Hex2StringLoopBodyWrite     ;continue execution of loop

Hex2StringLoopBodyWrite:                    ;actually write correct digit
        MOV     BYTE PTR ES:[SI], AL        ;write the digit to the address
        INC     SI                          ;increment the offset to the next address
        ;JMP    Hex2StringLoopDecr          ;setup for the next iteration

Hex2StringLoopDecr:                         ;setup by changing n and pwr16
        MOV     DI, DX                      ;now change n to be n MODULO pwr16
        SHR     CX, 4                       ;pwr16 = pwr16 / 16
        ;JMP    EndHex2StringLoop           ;done getting this digit

EndHex2StringLoop:                          ;loop getting digits of n
        CMP     CX, 0                       ;check if pwr16 > 0
        JG      Hex2StringLoopBodyDigit     ;if so, get the next digit
        ;JLE    TerminateStringHex          ;otherwise, no more digits, terminate string

TerminateStringHex:                         ;terminate the string and return
        MOV     BYTE PTR ES:[SI], ASCII_NULL;put the null character at the address
        POPA                                ; restore the registers
        RET                                 ;and return


Hex2String  ENDP



CODE    ENDS

        END