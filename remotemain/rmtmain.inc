;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  RMTMAIN.INC                               ;
;                           Remote Main Loop Functions                       ;
;                                   EE/CS 51                                 ;
;								  Include File 								 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for the remote side main loop.
;
; Revision History:
;     12/3/15   Dennis Shim		initial revision
;     12/9/15   Dennis Shim     moved SERIAL_PUT_CHAR_MAX to serial.inc

; constants

NOARG 				EQU 	0 		;no argument to be passed
SERIAL_STRING_LENGTH EQU 	16 		;maximum size of serial string buffer

; Direction and Speed Change
DELTA_SPEED 		EQU 	1000 	;amount to increase or decrease the speed by
DELTA_DIRECTION 	EQU 	5 		;degrees to increase or decrease the angle by

;Serial command chars
CMD_SET_REL_SPD 	EQU 	'V' 	;set relative speed command
CMD_SET_ABS_SPD 	EQU 	'S' 	;set absolute speed command
CMD_SET_DIR 		EQU 	'D' 	;set direction command
CMD_LASER_ON 		EQU 	'F' 	;turn laser on command
CMD_LASER_OFF 		EQU 	'O' 	;turn laser off command

;String lengths
STR_LEN_SPEED 		EQU 	3 		;'Spd' has length 3
STR_LEN_DIR 		EQU 	4 		;'Dir ' has length 4
STR_LEN_LASER 		EQU 	5 		;'Lasr ' has length 5