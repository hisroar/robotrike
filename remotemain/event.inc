;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  EVENT.INC                             	 ;
;                                   EE/CS 51                                 ;
;						   Event Values Include File 						 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains constants that represent event values.
;
; Revision History:
;     12/4/15  Dennis Shim		initial revision

; event types
								;enqueue this value in the EventQueue as AH
								;  depending on what kind of event is desired
EVENT_KEYPAD 	EQU 	0 		;keypad event
EVENT_SERIAL 	EQU		1 		;serial data event
EVENT_ERROR 	EQU 	2 	 	;error event