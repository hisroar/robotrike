;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 CONVERTS.INC                               ;
;                             Conversion Functions                           ;
;                                   EE/CS 51                                 ;
;								  Include File 								 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the definitions for the constants used by the conversion
; functions.
;
; Revision History:
;       10/15/15  Dennis Shim		initial revision
;       10/29/15  Dennis Shim      	updated comments and code
;       11/5/15   Dennis Shim       moved constants to general include file

;constants

MAXpwr10 	EQU 	10000 			; pwr10's starting value
MAXpwr16 	EQU 	1000H 			; pwr16's starting value