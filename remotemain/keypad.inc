;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  KEYPAD.INC                              	 ;
;                               Keypad Functions                             ;
;                                   EE/CS 51                                 ;
;								  Include File 								 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for reading keypresses from the
; keypad.
;
; Revision History:
;     11/5/15  	Dennis Shim 	initial revision
; 	  12/5/15  	Dennis Shim 	removed redundant constants, added KeyValues

; constants
NUM_COLS 			EQU 	4		;number of rows of keypads (each row has a
									;  separate address)
NUM_ROWS 			EQU 	4 		;number of columns of keypads (each column
									;  has separate bits)
KEEP_LOWER_4		EQU 	0FH 	;AND with this number to keep only the lower
									;  4 bits of a number
NO_KEY 				EQU 	0FH 	;value in KeyValue when there is no key

; Keypad address
KEYPAD_BASE_ADDR 	EQU 	0080H 	;Address for the first keypad (there are 4)
									;  To get the address of the ith keypad row,
									;  use (KEYPAD_ADDR + i)

; Keypad Bit Patterns
									;these bit patterns go into the four lower
									;  bits of the KeyValue
ZERO_KEYS 			EQU 	1111B 	;bit pattern if none of the keys in a row are
									;  pressed at a keypad address
FIRST_KEY 			EQU 	1110B 	;bit pattern if the first key in a row is
									;  pressed at a keypad address
SECOND_KEY 			EQU 	1101B 	;bit pattern if the second key in a row is
									;  pressed at a keypad address
THIRD_KEY 			EQU 	1011B 	;bit pattern if the third key in a row is
									;  pressed at a keypad address
FOURTH_KEY 			EQU 	0111B 	;bit pattern if the fourth key in a row is
									;  pressed at a keypad address

; Keypad Row Patterns
									;add bit patterns to these to get KeyValues
FIRST_ROW 			EQU 	00000000B;bit pattern for first row keys (0 SHL 4)
SECOND_ROW 			EQU 	00010000B;bit pattern for second row keys (1 SHL 4)
THIRD_ROW 			EQU 	00100000B;bit pattern for third row keys (2 SHL 4)
FOURTH_ROW 			EQU 	00110000B;bit pattern for fourth row keys (3 SHL 4)

; KeyValues
									;compute KeyValues by adding the keypad bit
									;  pattern and the keypad row pattern
KEY_11 				EQU 	FIRST_ROW + FIRST_KEY 	;first row, first column
KEY_12 				EQU 	FIRST_ROW + SECOND_KEY  ;first row, second column
KEY_13 				EQU 	FIRST_ROW + THIRD_KEY 	;first row, third column
KEY_14 				EQU 	FIRST_ROW + FOURTH_KEY 	;first row, fourth column
KEY_21 				EQU 	SECOND_ROW + FIRST_KEY 	;second row, first column
KEY_22 				EQU 	SECOND_ROW + SECOND_KEY	;second row, second column
KEY_23 				EQU 	SECOND_ROW + THIRD_KEY	;second row, third column
KEY_24 				EQU 	SECOND_ROW + FOURTH_KEY	;second row, fourth column
KEY_31 				EQU 	THIRD_ROW + FIRST_KEY	;third row, first column
KEY_32 				EQU 	THIRD_ROW + SECOND_KEY 	;third row, second column
KEY_33 				EQU 	THIRD_ROW + THIRD_KEY	;third row, third column
KEY_34 				EQU 	THIRD_ROW + FOURTH_KEY	;third row, fourth column
KEY_41 				EQU 	FOURTH_ROW + FIRST_KEY 	;fourth row, first column
KEY_42 				EQU 	FOURTH_ROW + SECOND_KEY	;fourth row, second column
KEY_43 				EQU 	FOURTH_ROW + THIRD_KEY	;fourth row, third column
KEY_44 				EQU 	FOURTH_ROW + FOURTH_KEY	;fourth row, fourth column

NUM_KEYS 			EQU  	NUM_COLS * NUM_ROWS;number of keys

; Timings
DEBOUNCE_TIME 		EQU 	25 		;time in ms that a key must be held down for
									;  it to be considered debounced/pressed
REPEAT_TIME 		EQU 	3000 	;time in ms that a key must be held down for
									;  it to be considered repeated

