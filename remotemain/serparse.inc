;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 SERPARSE.INC                               ;
;                          Serial Parsing Include File                       ;
;                                   EE/CS 51                                 ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the constants necessary for parsing communications from
; the serial chip.
;
; Revision History:
;     12/4/15  Dennis Shim     initial revision

; State definitions
; 		NOTE: these must match the layout of SerialState

ST_INITIAL 			EQU		0 		;initial state
ST_GET_STATUS 		EQU 	1 		;'S' - get status state
ST_GET_ERROR 	 	EQU  	2 		;'E' - get error state
ST_GET_STATUS_B0    EQU     3       ;first bit of status
ST_GET_STATUS_B1    EQU     4       ;second bit of status
ST_GET_STATUS_B2    EQU     5       ;third bit of status
ST_GET_STATUS_B3    EQU     6       ;fourth bit of status
ST_GET_STATUS_B4    EQU     7       ;fifth bit of status
ST_ERROR_RECEIVED 	EQU 	8 		;error value received
ST_ERROR 			EQU 	9 		;some kind of error encountered
ST_END 				EQU 	10 		;all done

NUM_STATES 			EQU 	11		;number of states

; Token definitions
; 		NOTE: these must match the layout of SerialState

TOKEN_GET_STATUS 	EQU 	0 		;'S' - receive motor/laser status
TOKEN_GET_ERROR 	EQU 	1 		;'E' - receive an error
TOKEN_EOS 			EQU  	2 		;token is end of string - <Return> or <CTRL/M>
TOKEN_OTHER 		EQU 	3 		;anything else

NUM_TOKEN_TYPES 	EQU 	4 		;number of tokens


; Other constants

TOKEN_MASK 			EQU 	01111111B;mask high bit of token
NUM_STATUS_BYTES 	EQU 	5 		;2 bytes for speed, 2 for direction, 1 for laser
