        NAME    KEYPAD

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Keypad                                  ;
;                               Keypad Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains functions that allow a user to read in inputs from the
; keypad. The functions included are:
;       KeypadInit      - Initializes KeyValue, DebounceCntr, and ScannerIndex
;                         to their proper values so other functions can use them.
;                         (PUBLIC)
;       KeypadScanner   - Scans one row of the keypad to see if a key is pressed.
;                         Calls KeypadDebouncer if a key is being debounced.
;                         (PUBLIC)
;       KeypadDebouncer - Debounces a key to determine whether it is being pressed.
;                         Also uses auto-repeat for the key if it is pressed.
;                         (PUBLIC)
;       GetKeyValue     - Sets KeyValue if a key in the row is pressed, or set it
;                         to NO_KEY if there is no key pressed. (PRIVATE)
;
; Revision History:
;     11/5/15   Dennis Shim     initial revision
;     12/5/15   Dennis Shim     updated to work with event.inc
;     12/9/15   Dennis Shim     updated to work using a shared folder


; local include files
$INCLUDE(../SHARED/GENERAL.INC) ;contains general constants and addresses
$INCLUDE(KEYPAD.INC)            ;contains constants and addresses for keypad
$INCLUDE(EVENT.INC)             ;contains the event codes

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE    SEGMENT PUBLIC 'CODE'



        ASSUME  CS:CGROUP, DS:DGROUP
		
		
		; external functions/tables
        EXTRN   EnqueueEvent:NEAR         ; stores the events and key values

; KeypadInit
;
; Description:        This function initializes the variables used by in order to
;                     successfully determine when a key is being pressed on the
;                     keypad. The KeyValue is initialized to NOKEY and the
;                     DebounceCntr is set to the maximum time (DEBOUNCE_TIME).
;                     ScannerIndex is set to the very last index (NUM_ROWS - 1).
;
; Operational         
; Description:        The KeyValue is initialized to NOKEY, which means that no
;                     key is currently being pressed. Then, the DebounceCntr is
;                     set to DEBOUNCE_TIME (the maximum time). This keeps track
;                     of whether the key is debounced or not. The ScannerIndex is
;                     set to (NUM_ROWS - 1), which is the 
;
; Arguments:          none.
;
; Return Values:      
; Global Variables:   
; Shared Variables:   KeyValue - DS - contains the value of the key that is pressed.
;                                     The top four bits contain the row index of
;                                     the key, and the bottom four bits contain
;                                     the bit pattern that was at the address.
;                                     (refer to keypad.inc for more information)
;                                     (CHANGED)
;                     DebounceCntr - DS - counter to check if a key is being
;                                         debounced. (CHANGED)
;                     ScannerIndex - DS - index of the address used by scanner when
;                                    checking if any keys are pressed. (CHANGED)
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    none.
; Limitations:        none.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  none.
;
; Revision History:
;     11/2/15  Dennis Shim       initial revision

KeypadInit      PROC        NEAR
                PUBLIC      KeypadInit

        MOV     KeyValue, NO_KEY        ;none of the keys have been seen yet
        MOV     DebounceCntr, DEBOUNCE_TIME;we want the debounce time to be at
                                           ;  its maximum so we can decrement later
        MOV     ScannerIndex, NUM_ROWS - 1;set the scanner index to the last
                                          ;  index so we can decrement later
        RET

KeypadInit      ENDP


; KeypadScanner
;
; Description:        This procedure scans one address of the keypad and checks
;                     if any of the keys are pressed. If a key was previously
;                     pressed, the KeypadDebouncer function is called to debounce
;                     the function. The KeyValue variable is updated depending on
;                     the row that the ScannerIndex is currently on.
;
; Operational         
; Description:        The previous KeyValue is first saved, and the new KeyValue
;                     for the row is retrieved by calling GetKeyValue with the
;                     argument PreviousKey (CL). Then, if there was previously
;                     a KeyValue, KeypadDebouncer is called to debounce that key.
;                     If there is currently a KeyValue, then the ScannerIndex is
;                     not changed so we can debonce this KeyValue at the next
;                     iteration. If there is no KeyValue, then we decrement
;                     ScannerIndex, wrap if necessary, and return.
;
; Arguments:          none.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   KeyValue - DS - contains the value of the key that is pressed.
;                                    The top four bits contain the row index of
;                                    the key, and the bottom four bits contain
;                                    the bit pattern that was at the address.
;                                    (refer to keypad.inc for more information)
;                                    (CHANGED)
;                     ScannerIndex - DS - index of the address used by scanner when
;                                    checking if any keys are pressed. (CHANGED)
;
; Local Variables:    PreviousKey - CL - contains the value of the previous key
;                                        that was being pressed.
;                  
; Inputs:             The procedure reads in the status of one row of the keypad to
;                     check if any of the keys are being pressed. If the key is
;                     active low, that means it is pressed.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    none.
; Limitations:        Can only read one row at a time.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  none.
;
; Revision History:
;     11/2/15  Dennis Shim       initial revision

KeypadScanner       PROC        NEAR
                    PUBLIC      KeypadScanner

        PUSH    BX                  ;preserve the registers
		PUSH 	CX

SavePreviousKey:
		MOV 	CH, 0	 			;clear CH so CX just contains the previous key
		MOV 	CL, KeyValue 		;save the previous key value
		
RetrieveNewKeyValue:
        MOV     BH, 0               ;clear BH to prepare to move ScannerIndex
        MOV     BL, ScannerIndex    ;set the AddrIndex argument for GetKeyValue
        CALL    GetKeyValue         ;get the KeyValue at the address, if there
                                    ;  is one

CheckPreviousKey: 					;debounce if there was a previous key (keep
									;  ScannerIndex the same)
		CMP 	CL, NO_KEY 			;check if a key had already been read
		JE 		CheckCurrentKey     ;no key was already read, check if we should
									;  decrement the index so the next row of
									;  keys can be read
		;JNE 	CallDebounce 		;a key was previously read, so we should
									;  debounce it

CallDebounce:
		CALL 	KeypadDebouncer		;check if the key is debounced by calling
									;  KeypadDebouncer with argument PreviousKey (CX)
		JMP 	KeypadScannerEnd 	;don't decrement the scanner index so we can
									;  continue to debounce the same key

CheckCurrentKey:
		CMP 	KeyValue, NO_KEY 	;check if we have just read a key
		;JE 	DecrementScannerIndex;we haven't, move to the next row and check
									 ;  if there is a key pressed there
		JNE 	KeypadScannerEnd 	;we have, don't decrement the index so we can
									;  debounce next time
									
DecrementScannerIndex:
        DEC     ScannerIndex        ;update the scanner index to the next address
        CMP     ScannerIndex, 0     ;check if we need to wrap
        ;JL     WrapScannerIndex    ;scanner index is out of bounds, wrap
        JGE     KeypadScannerEnd    ;scanner index is in bounds, don't need to wrap

WrapScannerIndex:                   ;wrap the scanner index to the highest index
        MOV     ScannerIndex, NUM_ROWS - 1
        ;JMP    KeypadScannerEnd

KeypadScannerEnd:
        POP     CX                  ;restore the registers
		POP 	BX
        RET

KeypadScanner       ENDP

; KeypadDebouncer
;
; Description:        This procedure checks whether or not a key is being debounced.
;                     Each time it is called, if the same key is pressed, then
;                     the debounce counter is decremented. When the debounce counter
;                     reaches zero, then the key is not being debounced and is pressed.
;                     If a key is pressed, then an event key (a constant depending
;                     on the key) is enqueued with along with the key value in the
;                     EventQueue. If the key is pressed, then the debounce counter
;                     is reset to the REPEAT_RATE so the key auto-repeats. The
;                     previous KeyValue is passed as an argument to this function.
;
; Operational         
; Description:        Decrement the debounce counter if the previous KeyValue
;                     and the current KeyValue are equal. If the debounce counter
;                     is zero, then enqueue an event to the EventQueue and reset
;                     the debounce counter to REPEAT_RATE to make the keypresses
;                     auto-repeat. If the KeyValues were not equal, then reset
;                     the debounce counter to DEBOUNCE_TIME.
;
; Arguments:          PreviousKey - CL - contains the value of the previous key
;                                        that was being pressed.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   KeyValue - DS - contains the value of the key that is pressed.
;                                     The top four bits contain the row index of
;                                     the key, and the bottom four bits contain
;                                     the bit pattern that was at the address.
;                                     (refer to keypad.inc for more information)
;                                     (CHANGED)
;                     DebounceCntr - DS - counter to check if a key is being
;                                         debounced. (CHANGED)
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    An EventQueue (FIFO) is used to enqueue the KeyValues and
;                     EVENT_VALUE.
; Limitations:        none.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  none.
;
; Revision History:
;     11/2/15  Dennis Shim       initial revision
;     12/5/15  Dennis Shim       now enqueues a EVENT_KEYPAD

KeypadDebouncer     PROC        NEAR

        PUSH    AX          ;preserve the registers
        PUSH    BX
        PUSH    CX

								;NOTE: KeypadScanner calls GetKeyValue so the
								;  KeyValue should be updated
KeyValueComparison:             ;check PreviousKey == KeyValue to determine if 
        CMP     CL, KeyValue    ;  we should continue debounce the key
        ;JE     SameKeyPressed  ;our the same key is pressed, keep debouncing
        JNE     DiffKeyPressed  ;the same key is not pressed, don't keep debouncing

SameKeyPressed:                 ;since the same key is pressed, keep debouncing
        DEC     DebounceCntr    ;decrement the debounce counter so we know how
                                ;  much longer until we say the key is pressed
        CMP     DebounceCntr, 0 ;check if we consider the key pressed yet
        ;JLE    KeyDebounced    ;the key has been pressed for DEBOUNCE_TIME ms,
                                ;  we now consider it pressed
        JG      KeypadDebouncerEnd;the key hasn't yet been pressed for enough time

KeyDebounced:                   ;key is considered pressed, so we enqueue into 
                                ;  the EventQueue and we want to check if we
                                ;  should repeat the event
        MOV     AL, KeyValue    ;we want to enqueue the KeyValue and EVENT_KEY
        MOV     AH, EVENT_KEYPAD;  so we pass the arguments to EnqueueEvent
        CALL    EnqueueEvent

        MOV     DebounceCntr, REPEAT_TIME ;now see if we should repeat the event

        JMP     KeypadDebouncerEnd;finished debouncing

DiffKeyPressed:                 ;a different key was pressed, so we reset the
                                ;  debounce counter
        MOV     DebounceCntr, DEBOUNCE_TIME
        ;JMP    KeypadDebouncerEnd

KeypadDebouncerEnd:
        POP     CX              ;restore the registers
        POP     BX
        POP     AX
        RET

KeypadDebouncer     ENDP

; GetKeyValue
;
; Description:        This procedure sets the KeyValue to the value at the address
;                     of the keypad. The index of the keypad row to check is
;                     passed in through AddrIndex. If there was no keys pressed,
;                     KeyValue is set to NO_KEY. Otherwise, the higher four bits
;                     of KeyValue will contain the index of the row of the key
;                     pressed, and the lower four bits will contain the bit
;                     pattern read from the keypad row address.
;
; Operational         
; Description:        The row address is computed by adding KEYPAD_BASE_ADDR and
;                     the row index (AddrIndex). Then, data is read from that
;                     keypad row. Only the lower four bits of the data are kept.
;                     If none of the keys were pressed (data == ZERO_KEYS), then
;                     KeyValue is set to NO_KEY and the function returns. If
;                     a key was pressed, then the row index is written to the first
;                     four bits of KeyIndex, and the bit pattern for the row is
;                     written to the next four bits of KeyIndex.
;
; Arguments:          AddrIndex - BL - index of the address of the keypad to check
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   KeyValue - DS - contains the value of the key that is pressed.
;                                     The top four bits contain the row index of
;                                     the key, and the bottom four bits contain
;                                     the bit pattern that was at the address.
;                                     (refer to keypad.inc for more information)
;                                     (CHANGED)
;
; Local Variables:    KeypStatus - AL - temporary variable that stores the status
;                                       of the keypad (i.e. if any keys are pressed)
;
; Inputs:             The procedure reads in the status of one row of the keypad to
;                     check if any of the keys are being pressed. If the key is
;                     active low, that means it is pressed.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    none.
; Limitations:        Can only read one key press.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  none.
;
; Revision History:
;     11/2/15  Dennis Shim       initial revision

GetKeyValue         PROC        NEAR

        PUSH    AX                  ;preserve the registers
        PUSH    BX
        PUSH    DX

GetKeyValueInit:
        MOV     BH, 0               ;clear BH in case it hasn't been cleared
        MOV     DX, BX              ;put the address in DX in preparation for input
        ADD     DX, KEYPAD_BASE_ADDR;to get the address of the keypad row we want,
                                    ;  add the index to the base address

GetKeypadRowInput:                  ;get input from the corresponding keypad row
        IN      AL, DX              ;read the data at address DX into AL

        AND     AL, KEEP_LOWER_4    ;only keep the four lowest bits
        CMP     AL, ZERO_KEYS       ;check if any of the keys are pressed
        JE      NoKeysPressed       ;none of the keys were pressed, KeyValue = NO_KEY
        ;JNE    KeyPressed          ;a key was pressed, write it to KeyValue

KeyPressed:                         ;a key was pressed, write the address index
        MOV     KeyValue, BL        ;  in the upper four bits and the bit pattern
        SHL     KeyValue, 4         ;  for the key press in the lower four bits
        OR      KeyValue, AL        ;  of KeyValue
        JMP     GetKeyValueEnd      ;we've written the KeyValue, now we're done

NoKeysPressed:                      ;none of the keys were pressed, so put 
        MOV     KeyValue, NO_KEY    ;  NO_KEY as the KeyValue
        ;JMP    GetKeyValueEnd

GetKeyValueEnd:
        POP     DX                  ;restore the registers
        POP     BX
        POP     AX

        RET

GetKeyValue         ENDP


CODE    ENDS

;the data segment

DATA    SEGMENT PUBLIC  'DATA'

KeyValue            DB      ?           ;contains the value of the pressed key
DebounceCntr        DW      ?           ;counter that checks if the key is debounced
ScannerIndex        DB      ?           ;index of the address used by scanner when
                                        ;  checking if any keys are pressed

DATA    ENDS

        END
