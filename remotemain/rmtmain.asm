        NAME    REMOTEMAIN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 REMOTEMAIN                                 ;
;                            Remote Side Main Loop                           ;
;                                  EE/CS  51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains functions that are necessary to parse communication over
; serial. The functions included are:
;       InitRemote          - Calls all initialization functions needed for
;                             the remote. (PRIVATE)
;       SetHardResetFlag    - Sets the HardResetFlag to TRUE. (PUBLIC)
;       UpdateStatus        - Updates the speed, direction, and laser status.
;                             (PUBLIC)
;       SerialPutStr        - Calls SerialPutChar for each char in a string.
;                             (PRIVATE)
;       HandleEvent         - Attempts to dequeue an event from the EventQueue.
;                             Call the appropriate function to handle the event.
;                             (PRIVATE)
;       HandleError         - Handles an error event by displaying the appropriate
;                             string (specified in ErrorValueTable). (PRIVATE)
;       HandleKey           - Handles a key event by calling the corresponding
;                             function with arguments and displaying a string
;                             (specified in the KeyValueTable). (PRIVATE)
;       ChangeSpeed         - Sends a Set Relative Speed command over serial.
;                             (PRIVATE)
;       SetSpeed            - Sends an Set Absolute Speed and Set Direction
;                             commands over serial. (PRIVATE)
;       SetLaser            - Sends a Fire Laser or Laser Off command over serial
;                             depending on the argument passed. (PRIVATE)
;       ChangeDirection     - Sends a Set Direction command over serial. (PRIVATE)
;       GetSpeed            - Displays the speed of the RoboTrike. (PRIVATE)
;       GetDirection        - Displays the direction of the RoboTrike. (PRIVATE)
;       GetLaser            - Displays the laser status of the RoboTrike. (PRIVATE)
;       KeyValueTable       - A table that contains, for each keypress, a STRUC
;                             with KeyValue, function to call, two arguments,
;                             and the string to display.
;       ErrorValueTable     - A table that contains, for each error, a STRUC
;                             with ErrorValue, and string to display.
;
; Revision History:
;    12/3/15    Dennis Shim         initial revision
;    12/9/15    Dennis Shim         moved SerialPutStr to serial
;    12/9/15    Dennis Shim         updated comments and init
;    12/9/15    Dennis Shim         updated to work using a shared folder

;local include files
$INCLUDE(RMTMAIN.INC)               ;constants needed for remote main
$INCLUDE(KEYPAD.INC)                ;constants needed for keypad
$INCLUDE(EVENT.INC)                 ;contains event constants
$INCLUDE(../SHARED/ERROR.INC)       ;contains error constants
$INCLUDE(../SHARED/MOTOR.INC)       ;contains motor constants
$INCLUDE(DISPLAY.INC)               ;contains display constants
$INCLUDE(../SHARED/GENERAL.INC)     ;contains general constants

; RemoteMainLoop
;
; Description:      Main loop for the remote side of the RoboTrike. Initialize
;                   all of the different parts of the remote. Continuously
;                   check if there is an event in the EventQueue to dequeue and
;                   handle the event depending on what it is. Hard resets if there
;                   is a critical error. When an event is dequeued from the
;                   EventQueue, it is handled by a function depending on the
;                   event type.
;
; Input:            Input is received from the keypad. A full description of the
;                   keypresses can be found in the User Interface section. The
;                   remote board also reads in input over serial. This input
;                   is from the motor board, and will either be the status of
;                   the motor or an error that occurred on the motor board.
;
; Output:           For certain keypresses (described in User Interface), a
;                   string containing a command is output over serial to the
;                   motor board. For every keypress, a string is outputted to
;                   the LCD display (described in User Interface). If an error
;                   occurs either on the remote board or the motor board, an
;                   error message is outputted on the LCD display.
;
; User Interface:   The keypad is used to display values to the LCD display or
;                   to output commands over serial. The layout of the keypad
;                   is described below (KEY_ROWCOL):
;                   KEY_11: Increases the speed of the RoboTrike by 1000, and
;                           displays the string "Inc Spd" on the display.
;                   KEY_12: Causes the RoboTrike to go forward (angle of 0 relative
;                           to forward) at maximum speed, and displays "Forward".
;                   KEY_13: Decreases the speed of the RoboTrike by 1000, and
;                           displays the string "Dec Spd" on the display.
;                   KEY_14: Displays the current speed of the RoboTrike in the
;                           format "Spd#####".
;                   KEY_21: Causes the RoboTrike to go left (angle of 270 relative
;                           to forward) at maximum speed, and displays "Left".
;                   KEY_22: Causes the RoboTrike to go forward (angle of 0 relative
;                           to forward) at half speed, and displays "Spd HALF".
;                   KEY_23: Causes the RoboTrike to go right (angle of 90 relative
;                           to forward) at maximum speed, and displays "Right".
;                   KEY_24: Displays the current direction of the RoboTrike in the
;                           format "Dir ###".
;                   KEY_31: Rotates the direction of the RoboTrike to the left
;                           by 5 degrees, and displays "Rotate L".
;                   KEY_32: Causes the RoboTrike to go backward (angle of 180 
;                           relative to forward) at maximum speed, and displays
;                           "Backward".
;                   KEY_33: Rotates the direction of the RoboTrike to the right
;                           by 5 degrees, and displays "Rotate R".
;                   KEY_34: Displays the current laser status of the RoboTrike 
;                           in the format "Lasr ON" or "Lasr OFF".
;                   KEY_41: Turns the laser on and displays "Lasr ON".
;                   KEY_42: Turns the laser off and displays "Lasr OFF".
;                   KEY_43: Stops the RoboTrike by setting speed to zero and
;                           direction to forward (angle of 0 relative to forward)
;                           and displays "Stop".
;                   KEY_44: Resets the whole remote board and displays "Reset"
;                           (very briefly, will generally not be seen).
;                   
;                   When errors occur, they are displayed on the LCD display.
;
; Error Handling:   If an error is received, it is displayed on the LCD display.
;                   If the error originates from the remote board, the first
;                   two characters displayed will be "ER". If the error originated
;                   from the motor board, the first two characters displayed are
;                   "EM". The strings that are displayed for each type of error
;                   are listed below:
;                   serial parity           - "E Parity"
;                   serial overrun          - "E Overrn"
;                   serial framing          - "E Framin"
;                   serial break interrupt  - "E BrkIRQ"
;                   serial buffer overflow  - "E Buffer"
;                   serial parsing          - "E Parse"
;
; Known Bugs:       None.
; Limitations:      None.

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA, STACK

CODE	SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP, DS:DGROUP

;external function declarations

        EXTRN   InitCS:NEAR             ;initializes chip selects
        EXTRN   ClrIRQVectors:NEAR      ;clears the interrupt vector table
        EXTRN   InstallTimer0EH:NEAR    ;installs timer 0 event handler
        EXTRN   InitTimer0:NEAR         ;initializes timer 0
        EXTRN   InstallInt2EH:NEAR      ;installs int 2 event handler
        EXTRN   InitInt2:NEAR           ;initializes int 2
        EXTRN   KeypadInit:NEAR         ;initializes keypad
        EXTRN   DisplayInit:NEAR        ;initializes display
        EXTRN   SerialInit:NEAR         ;initializes serial
        EXTRN   ParseSerialInit:NEAR    ;initialize the serial parser
        EXTRN   EventQueueInit:NEAR     ;initialize the event queue
        EXTRN   SerialPutChar:NEAR      ;outputs a character to serial
        EXTRN   EnqueueEvent:NEAR       ;enqueues an event to EventQueue
        EXTRN   DequeueEvent:NEAR       ;dequeues an event from EventQueue
        EXTRN   ParseSerialChar:NEAR    ;parses a character from serial
        EXTRN   Display:NEAR            ;displays a string
        EXTRN   Dec2String:NEAR         ;converts a decimal number to string
        EXTRN   SerialPutStr:NEAR       ;put a string in serial
        
START:  

MAIN:
        MOV     AX, DGROUP              ;initialize the stack pointer
        MOV     SS, AX
        MOV     SP, OFFSET(DGROUP:TopOfStack)

        MOV     AX, DGROUP              ;initialize the data segment
        MOV     DS, AX

HardReset:
        CALL    InitRemote              ;initialize the remote
        STI                             ;set IF to allow interrupts

Forever:                                ;sit in an infinite loop, keep checking
                                        ;  if there's something in the EventQueue
                                        ;  handle it if there is
        CALL    HandleEvent             ;handle the event if there is one

        CMP     HardResetFlag, TRUE     ;check if we need to hard reset
        ;JE     DoHardReset             ;flag is set, need to hard reset
        JNE     ForeverLoop             ;flag isn't set, don't hard reset

DoHardReset:                            ;hard reset when flag is set
        CLI                             ;turn off interrupts while initializing
                                        ;  everything, lots of critical code
                                        ;  (especially in interrupt code)
        JMP     HardReset               ;NOTE: hard reset flag is cleared

ForeverLoop:
        JMP     Forever                  


        HLT                            ;never executed (hopefully)

; InitRemote
;
; Description:        Initializes chip selects, interrupts, timer 0, INT 2,
;                     display, keypad, serial, and serial parser. Initializes
;                     the MotorSpeed to DRIVE_SPEED_STOP, MotorDirection to
;                     DRIVE_ANGLE_FORWARD, and LaserStatus to LASER_OFF.
;
; Operational         
; Description:        Call the initialization functions for chip select, interrupts,
;                     timer 0, INT 2, display, keypad, serial, and serial parser.
;                     Initialize MotorSpeed to DRIVE_SPEED_STOP, MotorDirection
;                     to DRIVE_ANGLE_FORWARD, and LaserStatus to LASER_OFF. Reset
;                     the HardResetFlag.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   HardResetFlag - DS - set if hard reset is required (w).
; 					  MotorSpeed - DS - current speed of the motor (w)
;                     MotorDirection - DS - current direction of the motor (w)
;                     LaserStatus - DS - current status of the laser (LASER_ON
;                                        if on, LASER_OFF if off) (w)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision
;     12/9/15  Dennis Shim     moved STI to main loop

InitRemote      PROC    NEAR

        CALL    InitCS                  ;initialize the 80188 chip selects
                                        ;   assumes LCS and UCS already setup

        CALL    ClrIRQVectors           ;clear (initialize) interrupt vector table

        CALL    InstallTimer0EH         ;install the event handler for timer0

        CALL    InitTimer0              ;initialize the internal timer

        CALL    InstallInt2EH           ;install the event handler for INT 2

        CALL    InitInt2                ;initialize the INT 2 interrupt

        CALL    KeypadInit              ;initialize the keypad variables
        
        CALL    DisplayInit             ;initialize the display
                                        ;   clear segBuffer and start at last digit

        CALL    SerialInit              ;initialize serial port

        CALL    ParseSerialInit         ;initialize serial parser
        
        CALL    EventQueueInit          ;initialize the event queue

        MOV     MotorSpeed, DRIVE_SPEED_STOP;RoboTrike not moving yet
        MOV     MotorDirection, DRIVE_ANGLE_FORWARD;RoboTrike going straight
        MOV     LaserStatus, LASER_OFF  ;laser is initially off

        MOV     HardResetFlag, FALSE    ;no hard reset required yet

        RET

InitRemote      ENDP

; SetHardResetFlag
;
; Description:        Sets the HardResetFlag to TRUE.
;
; Operational         
; Description:        Put TRUE in HardResetFlag.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   HardResetFlag - DS - set if hard reset is required (w).
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

SetHardResetFlag    PROC    NEAR
					PUBLIC  SetHardResetFlag

        MOV     HardResetFlag, TRUE

        RET

SetHardResetFlag    ENDP

; UpdateStatus
;
; Description:        Updates the status of the motor unit so the most current
;                     values for speed, direction, and laser status are stored.
;
; Operational         
; Description:        Moves the arguments into MotorSpeed, MotorDirection, and
; 				   	  LaserStatus.
;
; Arguments:          MotorSpeed - AX - speed of the motor received over serial
;                     MotorDirection - BX - direction of the motor received over
;                                           serial
;                     LaserStatus - CL - status of the laser (LASER_ON if on,
;                                        LASER_OFF if off)
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   MotorSpeed - DS - current speed of the motor (w)
;                     MotorDirection - DS - current direction of the motor (w)
;                     LaserStatus - DS - current status of the laser (LASER_ON
;                                        if on, LASER_OFF if off) (w)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

UpdateStatus    PROC    NEAR
                PUBLIC  UpdateStatus

                                        ;update values according to arguments
        MOV     MotorSpeed, AX
        MOV     MotorDirection, BX
        MOV     LaserStatus, CL

        RET

UpdateStatus    ENDP

; HandleEvent
;
; Description:        Attempts to dequeue an event from the EventQueue. If there
;                     was an event, then call the appropriate handler for that
;                     event type (keypress, serial, or error).
;
; Operational         
; Description:        Attempt to dequeue an event. If the zero flag was set, there
;                     was no event so just return. Otherwise, check the EventID.
;                     If it is EVENT_KEYPAD, call HandleKey; if it is EVENT_SERIAL,
;                     call ParseSerialChar; if it is EVENT_ERROR, call HandleError.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   None.
;
; Local Variables:    EventID - AH - ID of the type of event (constant)
;                     EventArgument - AL - argument associated with the event
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

HandleEvent     PROC    NEAR

        PUSH    AX                  ;preserve register
CheckEvent:
        CALL    DequeueEvent        ;attempt to dequeue an event
                                    ;zero flag is set if EventQueue was empty
        JZ      HandleEventEnd      ;there was no event, just finish
        ;JNZ    EventDequeued       ;there was an event, call the right handler

EventDequeued:                      ;check what kind of event was enqueued
        CMP     AH, EVENT_KEYPAD
        JE      KeypadEventDequeued ;got a keypad event

        CMP     AH, EVENT_SERIAL
        JE      SerialEventDequeued ;got a serial event

        CMP     AH, EVENT_ERROR
        JE      ErrorEventDequeued  ;got an error event

KeypadEventDequeued:                ;got a keypad event, call HandleKey
        CALL    HandleKey
        JMP     HandleEventEnd      ;and finish

SerialEventDequeued:                ;got a serial event, call ParseSerialChar
        CALL    ParseSerialChar     ;char argument already in AL from dequeue
        CMP     AX, NO_ERROR        ;check if there was an error
        JE      HandleEventEnd      ;no error occured, just finish
        ;JNE    SerialEventDequeuedError;an error occured, enqueue an event

SerialEventDequeuedError:           ;put a serial parsing error in the EventQueue
        MOV     AH, EVENT_ERROR     ;enqueue an error event
        MOV     AL, ERROR_SERIAL_PARSE;and a serial parsing error
        CALL    EnqueueEvent
        JMP     HandleEventEnd      ;and finish

ErrorEventDequeued:                 ;got an error event, call HandleError
        CALL    HandleError
        JMP     HandleEventEnd      ;and finish

HandleEventEnd:
        POP     AX                  ;restore register
        RET

HandleEvent      ENDP

; HandleError
;
; Description:        Handles a error from somewhere by displaying the appropriate
;                     string that is associated with the error.
;
; Operational         
; Description:        Loop through the ErrorValueTable until the ErrorValue is
;                     found. When found, get the string associated with the error
;                     and put it in StringBuffer. Then call Display with
;                     StringBuffer as the argument.
;
; Arguments:          ErrorValue - AL - value of the error received.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   StringBuffer - DS - the DisplayString is stored in the
;                                         StringBuffer to be passed to Display (w)
;
; Local Variables:    ErrorIndex - SI - index of the key table when looping through
;                                       to find the correct ErrorValue
;                     StringIndex - SI - index of the string buffer and display
;                                        string table to write/read chars from
;
; Inputs:             None.
; Outputs:            Displays the error string on the LCD display.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

HandleError     PROC    NEAR

        PUSHA                       ;preserve registers

        XOR     SI, SI              ;start ErrorIndex at 0

CheckMotorError:                    ;check if the error originated from the motor
                                    ;  board, if it is then TESTing with 
                                    ;  MOTOR_ERROR_VAL will return a non-zero value
        TEST    AL, MOTOR_ERROR_VAL
        ;JNZ    MotorErrorReceived  ;the error is from the motor board
        JZ      RemoteErrorReceived ;the error is from the remote board

MotorErrorReceived:                 ;error is from motor board, start searching
                                    ;  table from correct index
        MOV     BX, NUM_ERRORS * SIZE ERTable;start right after remote errors
        JMP     DetermineErrorKeyIndexLoop;start looping to find correct error 

RemoteErrorReceived:                ;error is from remote board, start searching
                                    ;  table from first index
        XOR     BX, BX              ;start at beginning of table
        ;JMP    DetermineErrorKeyIndexLoop;start looping to find correct error 

DetermineErrorKeyIndexLoop:         ;increment SI instead of BX to loop through
                                    ;  the ErrorValueTable
        TEST    AL, CS:ErrorValueTable[BX + SI].ErrorValue;check if the error at
                                                          ;  the index is the same
        JNZ     FoundErrorKey       ;it is, display string
        CMP     SI, NUM_ERRORS * SIZE ERTable;it isn't, check if we've reached 
											 ;  the end of the table
        JGE     HandleErrorEnd      ;we've reached the end, the error is invalid,
                                    ;  do nothing
        ADD     SI, SIZE ERTable    ;still not at the end, put ErrorIndex at next
                                    ;  index and keep searching for the ErrorValue
        JMP     DetermineErrorKeyIndexLoop;keep looping through the table

FoundErrorKey:                      ;the key is in the table, index of the value
                                    ;  is stored in BX, need to get string and
                                    ;  display it after putting it in StringBuffer
        ADD     BX, SI              ;store the corrected index, which is necessary
                                    ;  because the index depends on whether it was
                                    ;  a remote or motor error, and we need SI
                                    ;  as an index to store the string in a buffer
        XOR     SI, SI              ;prepare to loop through each character in
                                    ;  the table and put it in StringBuffer

ErrorStringTableLoopBody:         ;put a char from the table to the StringBuffer
        MOV     AL, CS:ErrorValueTable[BX + SI].ERDispString;get the char at the index
        MOV     StringBuffer[SI], AL;store the char in the StringBuffer
        INC     SI                  ;move the StringIndex to the next char

ErrorStringTableLoop:               ;check if we've reached the end of the string
        CMP     SI, DISP_STRING_LENGTH
        ;JGE    DisplayErrorString  ;we have, display the string
        JL      ErrorStringTableLoopBody;we haven't, keep getting chars

DisplayErrorString:
        MOV     StringBuffer[SI], ASCII_NULL;null terminate the string after we 
                                            ;  are done filling in characters
        PUSH    DS                  ;move DS into ES so display gets the string
        POP     ES                  ;  from DS:StringBuffer
        LEA     SI, StringBuffer    ;pass StringBuffer's address as an argument
                                    ;  to display the right string
        CALL    Display             ;display the string

        ;JMP    HandleKeyEnd        ;done displaying and calling, return

HandleErrorEnd:
        POPA                        ;restore registers

        RET

HandleError         ENDP

; HandleKey
;
; Description:        Handles a keypress from the Keypad by calling the
;                     corresponding function with appropriate arguments, and
;                     displaying the appropriate string.
;
; Operational         
; Description:        Loop through the KeyValueTable until the KeyValue is found.
;                     When found, get the function and the two arguments. Also
;                     get the string and call Display, passing the string as
;                     an argument. Then call the function with the two arguments.
;
; Arguments:          KeyValue - AL - value of the keypress received.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   StringBuffer - DS - the DisplayString is stored in the
;                                         StringBuffer to be passed to Display (w)
;
; Local Variables:    KeyIndex - BX - index of the key table when looping through
;                                     to find the correct KeyValue
;                     StringIndex - SI - index of the string buffer and display
;                                        string table to write/read chars from
;
; Inputs:             None.
; Outputs:            Displays a string on the LCD display.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

HandleKey       PROC    NEAR

        PUSHA                       ;preserve registers

        XOR     BX, BX              ;start KeyIndex at 0

DetermineKeyIndexLoop:
        CMP     AL, CS:KeyValueTable[BX].KeyValue;check if the key at the index is the same
        JE      FoundKey            ;it is, perform appropriate actions
        CMP     BX, NUM_KEYS * SIZE UITable;it isn't, check if we've reached the
                                    ;  end of the table
        JGE     HandleKeyEnd        ;we've reached the end, the key press is
                                    ;  invalid, do nothing
        ADD     BX, SIZE UITable    ;still not at the end, put BX at next index 
                                    ;  and keep searching for the KeyValue
        JMP     DetermineKeyIndexLoop

FoundKey:                           ;the key is in the table, index of the values
                                    ;  is stored in BX, need to call function with
                                    ;  arg1 and arg2, and then display the
                                    ;  DisplayString
        MOV     CX, CS:KeyValueTable[BX].Function;store the function offset in CX
        MOV     DX, CS:KeyValueTable[BX].Arg1;store the first argument in DX
        MOV     DI, CS:KeyValueTable[BX].Arg2;store the second argument in DI

        XOR     SI, SI              ;prepare to loop through each character in
                                    ;  the table and put it in StringBuffer

DisplayStringTableLoopBody:         ;put a char from the table to the StringBuffer
        MOV     AL, CS:KeyValueTable[BX + SI].DispString;get the char at the index
        MOV     StringBuffer[SI], AL;store the char in the StringBuffer
        INC     SI                  ;move the StringIndex to the next char

DisplayStringTableLoop:             ;check if we've reached the end of the string
        CMP     SI, DISP_STRING_LENGTH
        ;JGE    DisplayCallKey      ;we have, display the string and call the
                                    ;  function
        JL      DisplayStringTableLoopBody;we haven't, keep getting chars

DisplayCallKey:
        MOV     StringBuffer[SI], ASCII_NULL;null terminate the string after we are
                                            ;  done filling in characters
        PUSH    DS                  ;move DS into ES so display gets the string
        POP     ES                  ;  from DS:StringBuffer
        LEA     SI, StringBuffer    ;pass StringBuffer's address as an argument
                                    ;  to display to display the right string
        CALL    Display             ;display the string

        MOV     AX, DX              ;pass the first argument in AX
        MOV     BX, DI              ;pass the second argument in BX
        CALL    CX                  ;call the function with the correct arguments

        ;JMP    HandleKeyEnd        ;done displaying and calling, return

HandleKeyEnd:
        POPA                        ;restore registers

        RET

HandleKey       ENDP

; ChangeSpeed
;
; Description:        Send a Set Relative Speed (V#) over serial based on the
;                     argument passed.
;
; Operational         
; Description:        Put the command char in the first index of SerialString.
;                     Call Dec2String to fill in the numbers based on argument
;                     passed. Call SerialPutStr to send the string over serial.
;
; Arguments:          DeltaSpeed - AX - change in speed desired
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   SerialString - DS - the string to send to serial is stored 
;                                         in the SerialString (w)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            A string is sent over serial with the desired command.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

ChangeSpeed     PROC    NEAR

        PUSH    BX                  ;preserve registers
        PUSH    SI

        XOR     BX, BX              ;access the first index of the serial string
        MOV     SerialString[BX], CMD_SET_REL_SPD;send the CMD_SET_REL_SPD char
                                                 ;  to set relative speed
        LEA     SI, SerialString    ;put the address of the serial string in
                                    ;  SI to pass to Dec2String
        INC     SI                  ;start at the next index because we have
                                    ;  already written to the first one
        PUSH    DS                  ;move DS into ES so Dec2String writes to 
        POP     ES                  ;  DS:SerialString
        CALL    Dec2String          ;fill in the numbers in the SerialString to
                                    ;  prepare to send over serial
                                    ;  SerialString is null terminated
        LEA     SI, SerialString    ;put the address of the serial string in
                                    ;  SI to pass to SerialPutStr
        CALL    SerialPutStr        ;pass the SerialString to send to serial

        POP     SI                  ;restore registers
        POP     BX
        RET

ChangeSpeed     ENDP

; SetSpeed
;
; Description:        Send a Set Absolute Speed (S#) and a Set Direction (D#)
;                     over serial based on the argument passed. The function
;                     handles the case in which the speed is greater than
;                     
;
; Operational         
; Description:        Check if the AbsoluteSpeed is greater than half the max
;                     speed. If it isn't, just send AbsoluteSpeed as an argument
;                     after the CMD_SET_ABS_SPD in SerialString. Otherwise,
;                     send half the max speed, then send the difference by
;                     subtracting half the max speed from AbsoluteSpeed, and
;                     calling ChangeSpeed with the appropriate argument. Then
;                     send the angle using CMD_SET_DIR as the command in
;                     SerialString.
;
; Arguments:          AbsoluteSpeed - AX - absolute speed desired
;                     Direction - BX - angle of RoboTrike desired
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   SerialString - DS - the string to send to serial is stored 
;                                         in the SerialString (w)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            A string is sent over serial with the desired command.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

SetSpeed        PROC    NEAR

        PUSHA                       ;preserve registers

        MOV     DI, AX              ;preserve the arguments in DI and DX
        MOV     DX, BX

                                    ;prepare to send an set absolute speed command
                                    ;  (we will send one no matter what)
        XOR     BX, BX              ;access the first index of the serial string
        MOV     SerialString[BX], CMD_SET_ABS_SPD;send the CMD_SET_ABS_SPD char
                                                 ;  to set absolute speed
        LEA     SI, SerialString    ;put the address of the serial string in
                                    ;  SI to pass to Dec2String
        INC     SI                  ;need to write the number to the next index

CheckDriveSpeedHalf:                ;we cannot send above half the max speed as
                                    ;  the number over serial, so if the desired
                                    ;  speed is greater than half the speed,
                                    ;  we first have to send half the speed,
                                    ;  using the absolute speed set, and then the
                                    ;  rest of the speed using the relative speed
        CMP     AX, DRIVE_SPEED_HALF;check if the desired speed is greater than
                                    ;  half the max speed
        MOV     CX, OFFSET(SendDirection);if the speed is lower, then we will
                                         ;  just jump to direction after sending
                                         ;  the absolute speed command
        ;JA     DriveSpeedAbove     ;desired speed is higher than half speed,
                                    ;  send half then send the difference
        JB      SendAbsoluteSpeed   ;just need to send the drive speed then return

DriveSpeedAbove:
        MOV     AX, DRIVE_SPEED_HALF;first set the speed to half
        MOV     CX, OFFSET(SendRelativeDiff);need to send the relative difference

SendAbsoluteSpeed:
        PUSH    DS                  ;move DS into ES so Dec2String writes to 
        POP     ES                  ;  DS:SerialString
        CALL    Dec2String          ;fill in the numbers in the SerialString to
                                    ;  prepare to send over serial
                                    ;  SerialString is null terminated
        LEA     SI, SerialString    ;put the address of the serial string in
                                    ;  SI to pass to SerialPutStr
        CALL    SerialPutStr        ;pass the SerialString to send to serial
        
        JMP     CX                  ;jump to the label depending on which
                                    ;  conditions were met above

SendRelativeDiff:                   ;argument was too big, send relative difference
        MOV     AX, DI              ;recover the original argument
        SUB     AX, DRIVE_SPEED_HALF;get the difference to send
        CALL    ChangeSpeed         ;send the relative difference by passing
                                    ;  the difference in AX to ChangeSpeed
                                    ;  NOTE: AX should always be positive
        ;JMP    SendDirection       ;now send the direction

SendDirection:                      ;send the desired direction
        MOV     AX, DX              ;recover the angle argument from DX
        SUB     AX, MotorDirection  ;set the absolute direction (not relative)
        CALL    ChangeDirection     ;send the direction by calling ChangeDirection
                                    ;  and passing the angle as an argument in
                                    ;  AX

SetSpeedEnd:
        POPA                        ;restore registers

        RET

SetSpeed        ENDP

; SetLaser
;
; Description:        Send either a Fire Laser (F) or a Laser Off (O) command
; 					  over serial depending on what argument is passed.
;
; Operational         
; Description:        Check if the laser should be turned on or off. If the laser
;                     should be turned on, write CMD_LASER_ON to the SerialString
;                     otherwise write CMD_LASER_OFF. Null terminate the string
;                     and call SerialPutStr.
;
; Arguments:          LaserStatus - AX - desired laser status (LASER_ON if on,
; 										 LASER_OFF if off)
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   SerialString - DS - the string to send to serial is stored 
;                                         in the SerialString (w)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            A string is sent over serial with the desired command.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

SetLaser        PROC    NEAR

		PUSH 	BX 					;preserve register

		XOR 	BX, BX 				;write to the first index of SerialString
        CMP 	AX, LASER_ON 		;check if the desired status is on
		;JE 	TurnLaserOn 		;want laser to be on, send 'F' over serial
		JNE 	TurnLaserOff 		;want laser to be off, send 'O' over serial

TurnLaserOn:
		MOV 	SerialString[BX], CMD_LASER_ON;turn laser on command
		JMP 	TerminateLaserString;now terminate the string
		
TurnLaserOff:
		MOV 	SerialString[BX], CMD_LASER_OFF;turn laser off command
		;JMP 	TerminateLaserString;now terminate the string

TerminateLaserString:
		INC 	BX 					;terminate string in next char
		MOV 	SerialString[BX], ASCII_NULL
		
		LEA     SI, SerialString    ;put the address of the serial string in
                                    ;  SI to pass to SerialPutStr
        CALL    SerialPutStr        ;pass the SerialString to send to serial

		POP 	BX 					;restore register
		
        RET

SetLaser        ENDP

; ChangeDirection
;
; Description:        Send a Set Relative Direction (D#) over serial based on the
;                     argument passed.
;
; Operational         
; Description:        Put the command char in the first index of SerialString.
;                     Call Dec2String to fill in the numbers based on argument
;                     passed. Call SerialPutStr to send the string over serial.
;
; Arguments:          DeltaAngle - AX - change in angle desired
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   SerialString - DS - the string to send to serial is stored 
;                                         in the SerialString (w)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            A string is sent over serial with the desired command.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

ChangeDirection     PROC    NEAR

        PUSH    BX                  ;preserve registers
        PUSH    SI

        XOR     BX, BX              ;access the first index of the serial string
        MOV     SerialString[BX], CMD_SET_DIR;send the CMD_SET_DIR char to set
                                             ;  the direction
        LEA     SI, SerialString    ;put the address of the serial string in
                                    ;  SI to pass to Dec2String
        INC     SI                  ;need to write the number to the next index
        PUSH    DS                  ;move DS into ES so Dec2String writes to 
        POP     ES                  ;  DS:SerialString
        CALL    Dec2String          ;fill in the numbers in the SerialString to
                                    ;  prepare to send over serial
                                    ;  SerialString is null terminated    
        LEA     SI, SerialString    ;put the address of the serial string in
                                    ;  SI to pass to SerialPutStr
        CALL    SerialPutStr        ;pass the SerialString to send to serial

        POP     SI                  ;restore registers
        POP     BX
        RET

ChangeDirection     ENDP

; GetSpeed
;
; Description:        Displays the current speed of the RoboTrike.
;
; Operational         
; Description:        Append the number as a string by calling Dec2String. Call
;                     Display with the StringBuffer as the argument.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   StringBuffer - DS - the DisplayString is stored in the
;                                         StringBuffer to be passed to Display (w)
;                     MotorSpeed - DS - current speed of the motor (r)
;
; Local Variables:    None.
;
; Inputs:             A string with the speed is output to the LCD display.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

GetSpeed        PROC    NEAR
        
        PUSH    AX                  ;preserve registers
        PUSH    SI

        MOV     AX, MotorSpeed      ;prepare to call Dec2String with
                                    ;  MotorSpeed in AX
        LEA     SI, StringBuffer    ;get the address of the StringBuffer in SI
                                    ;  to prepare to pass as an argument to
                                    ;  Dec2String
        ADD     SI, STR_LEN_SPEED   ;we want to write after the first part of
                                    ;  the StringBuffer, which contains a string
                                    ;  beforehand
        PUSH    DS                  ;move DS into ES so Dec2String writes to 
        POP     ES                  ;  DS:SerialString
        CALL    Dec2String          ;append the number to the StringBuffer

        LEA     SI, StringBuffer    ;get the address of the StringBuffer in SI
                                    ;  to prepare to pass as an argument to
                                    ;  Display
                                    ;NOTE: ES already contains DS
        CALL    Display             ;display the StringBuffer

        POP     SI                  ;restore registers
        POP     AX

        RET

GetSpeed        ENDP

; GetDirection
;
; Description:        Displays the current direction of the RoboTrike.
;
; Operational         
; Description:        Append the number as a string by calling Dec2String. Call
;                     Display with the StringBuffer as the argument.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   StringBuffer - DS - the DisplayString is stored in the
;                                         StringBuffer to be passed to Display (w)
;                     MotorDirection - DS - current direction of the motor (r)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            A string with the direction is output to the LCD display.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

GetDirection        PROC    NEAR
        
        PUSH    AX                  ;preserve registers
        PUSH    SI

        MOV     AX, MotorDirection  ;prepare to call Dec2String with
                                    ;  MotorDirection in AX
        LEA     SI, StringBuffer    ;get the address of the StringBuffer in SI
                                    ;  to prepare to pass as an argument to
                                    ;  Dec2String
        ADD     SI, STR_LEN_DIR     ;we want to write after the first part of
                                    ;  the StringBuffer, which contains a string
                                    ;  beforehand
        PUSH    DS                  ;move DS into ES so Dec2String writes to 
        POP     ES                  ;  DS:SerialString
        CALL    Dec2String          ;append the number to the StringBuffer

        LEA     SI, StringBuffer    ;get the address of the StringBuffer in SI
                                    ;  to prepare to pass as an argument to
                                    ;  Display
                                    ;NOTE: ES already contains DS
        CALL    Display             ;display the StringBuffer
        
        POP     SI                  ;restore registers
        POP     AX

        RET

GetDirection        ENDP

; GetLaser
;
; Description:        Displays the current laser status of the RoboTrike.
;
; Operational         
; Description:        Append 'ON' or 'OFF' depending on the LaserStatus. Null
;                     terminate the StringBuffer and then Display it.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   StringBuffer - DS - the DisplayString is stored in the
;                                         StringBuffer to be passed to Display (w)
;                     LaserStatus - DS - current status of the laser (LASER_ON
;                                        if on, LASER_OFF if off) (r)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            A string with the laser status is output to the LCD display.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/3/15  Dennis Shim     initial revision

GetLaser        PROC    NEAR

        PUSH    BX                  ;preserve registers
        PUSH    SI 
		
		MOV     BX, STR_LEN_LASER   ;start writing chars at index BX of StringBuffer
		
        CMP     LaserStatus, LASER_ON;check if the laser is on
        ;JE     StringLaserOn       ;laser is on, write 'ON'
        JNE     StringLaserOff      ;laser is off, write 'OFF'

StringLaserOn:                      ;write 'ON' after the initial string
        MOV     StringBuffer[BX], 'O'
        INC     BX                  ;write to the next char
        MOV     StringBuffer[BX], 'N'
        INC     BX                  ;write to the next char
        JMP     DisplayLaserStatus  ;done writing, display the laser status

StringLaserOff:                     ;write 'OFF' after the initial string
        MOV     StringBuffer[BX], 'O'
        INC     BX                  ;write to the next char
        MOV     StringBuffer[BX], 'F'
        INC     BX                  ;write to the next char
        MOV     StringBuffer[BX], 'F'
        INC     BX                  ;write to the next char
        ;JMP    DisplayLaserStatus  ;done writing, display the laser status

DisplayLaserStatus:                 ;add ASCII_NULL and display
        MOV     StringBuffer[BX], ASCII_NULL;put ASCII_NULL at the end of the string
                                            ;  to terminate it
        PUSH    DS                  ;move DS into ES so Dec2String writes to 
        POP     ES                  ;  DS:SerialString
        LEA     SI, StringBuffer    ;get the address of the StringBuffer in SI
                                    ;  to prepare to pass as an argument to
                                    ;  Display
                                    ;NOTE: ES already contains DS
        CALL    Display             ;display the StringBuffer
        
        POP     SI                  ;restore registers
        POP     BX

        RET

GetLaser        ENDP

; KeyValueTable
;
; Description:      This is a table of UITable STRUCs that contain the KeyValue
;                   of a keypress, the function to be called, two arguments
;                   to be called with the function, and the string that should
;                   be displayed on the LCD display when the key is pressed.
;                   To lookup the correct table entry, loop through the table by,
;                   incrementing by the size of the UITable STRUC, and check the
;                   KeyValue. Call the function with the two arguments if the
;                   KeyValue matches, and display the string.
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision

UITable     STRUC
        KeyValue    	DB  ? 			;KeyValue of the keypress
        Function    	DW  ? 			;function to be called
        Arg1        	DW  ? 			;first argument of function 
        Arg2        	DW  ? 			;second argument of function
        DispString  	DB  '        ' 	;string to display (8 characters)
UITable     ENDS

;define a macro to make table a little more readable
;macro just does an offset of the action routine entries to build the STRUC
%*DEFINE(TABENT(keyval, func, arg1, arg2, str))  (
    UITable< %keyval, OFFSET(%func), %arg1, %arg2, %str >
)

%*DEFINE(TABLE)  (
        ;first row
        %TABENT(KEY_11, ChangeSpeed,    DELTA_SPEED,     NOARG,              'Inc Spd ') ;first column
        %TABENT(KEY_12, SetSpeed,       DRIVE_SPEED_MAX, DRIVE_ANGLE_FORWARD,'Forward ') ;second column
        %TABENT(KEY_13, ChangeSpeed,    -DELTA_SPEED,    NOARG,              'Dec Spd ') ;third column
        %TABENT(KEY_14, GetSpeed,       NOARG,           NOARG,              'Spd     ') ;fourth column
        ;second row
        %TABENT(KEY_21, SetSpeed,       DRIVE_SPEED_MAX, DRIVE_ANGLE_LEFT,   'Left    ') ;first column
        %TABENT(KEY_22, SetSpeed,       DRIVE_SPEED_HALF,DRIVE_ANGLE_FORWARD,'Spd HALF') ;second column
        %TABENT(KEY_23, SetSpeed,       DRIVE_SPEED_MAX, DRIVE_ANGLE_RIGHT,  'Right   ') ;third column
        %TABENT(KEY_24, GetDirection,   NOARG,           NOARG,              'Dir     ') ;fourth column
        ;third row
        %TABENT(KEY_31, ChangeDirection,-DELTA_DIRECTION,NOARG,              'Rotate L') ;first column
        %TABENT(KEY_32, SetSpeed,       DRIVE_SPEED_MAX, DRIVE_ANGLE_REVERSE,'Backward') ;second column
        %TABENT(KEY_33, ChangeDirection,DELTA_DIRECTION, NOARG,              'Rotate R') ;third column
        %TABENT(KEY_34, GetLaser,       NOARG,           NOARG,              'Lasr    ') ;fourth column
        ;fourth row
        %TABENT(KEY_41, SetLaser,       LASER_ON,        NOARG,              'Lasr ON ') ;first column
        %TABENT(KEY_42, SetLaser,       LASER_OFF,       NOARG,              'Lasr OFF') ;second column
        %TABENT(KEY_43, SetSpeed,       DRIVE_SPEED_STOP,DRIVE_ANGLE_FORWARD,'Stop    ') ;third column
        %TABENT(KEY_44, SetHardResetFlag,NOARG,          NOARG,              'Reset   ') ;fourth column
)

KeyValueTable    LABEL   BYTE
        %TABLE

; ErrorValueTable
;
; Description:      This is a table of ERTable STRUCs that contain the ErrorValue
;                   of an error, and the string that should be displayed on the
;                   LCD display when the error occurs. To lookup the correct table
;                   entry, loop through the table by, incrementing by the size
;                   of the ERTable STRUC, and check the ErrorValue. Display the
;                   string if the ErrorValue matches, and display the string.
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision

ERTable     STRUC
        ErrorValue		DB  ?
        ERDispString  	DB  '        '
ERTable     ENDS

;define a macro to make table a little more readable
;macro just does an offset of the action routine entries to build the STRUC
%*DEFINE(TABENT(errval, str))  (
    ERTable< %errval, %str >
)

;NOTE: all strings are length 8
%*DEFINE(TABLE)  (
        %TABENT(ERROR_PARITY,           'ERParity');remote - serial parity error
        %TABENT(ERROR_OVERRUN,          'EROverrn');remote - serial overrun error
        %TABENT(ERROR_FRAMING,          'ERFramin');remote - serial framing error
        %TABENT(ERROR_BRK_IRQ,          'ERBrkIRQ');remote - serial break interrupt error
        %TABENT(ERROR_BUFFER_OVERFLOW,  'ERBuffer');remote - serial buffer overflow error
        %TABENT(ERROR_SERIAL_PARSE,     'ERParse ');remote - serial parsing error

        %TABENT(ERROR_PARITY,         'EMParity')  ;motor  - serial parity error
        %TABENT(ERROR_OVERRUN,        'EMOverrn')  ;motor  - serial overrun error
        %TABENT(ERROR_FRAMING,        'EMFramin')  ;motor  - serial framing error
        %TABENT(ERROR_BRK_IRQ,        'EMBrkIRQ')  ;motor  - serial break interrupt error
        %TABENT(ERROR_BUFFER_OVERFLOW,'EMBuffer')  ;motor  - serial buffer overflow error
        %TABENT(ERROR_SERIAL_PARSE,   'EMParse ')  ;motor  - serial parsing error
)

ErrorValueTable    	LABEL   BYTE
        %TABLE

CODE 	ENDS

;the data segment

DATA    SEGMENT PUBLIC  'DATA'

MotorSpeed          DW          ?   ;current speed of the motor
MotorDirection      DW          ?   ;current direction of the motor
LaserStatus         DB          ?   ;current status of the laser
StringBuffer        DB          (DISP_STRING_LENGTH+1)    DUP   (?);buffer to keep
                                                                   ;display string
SerialString        DB          (SERIAL_STRING_LENGTH)  DUP     (?);buffer to keep
                                                                   ;string to send
                                                                   ;to serial
HardResetFlag       DB          ?   ;set if a hard reset is needed

DATA    ENDS

;the stack

STACK           SEGMENT STACK  'STACK'

                DB      80 DUP ('Stack ')       ;240 words

TopOfStack      LABEL   WORD

STACK   ENDS

        END     START
