asm86 rmtmain.asm m1 ep db
asm86 ../shared/chpslct.asm m1 ep db
asm86 converts.asm m1 ep db
asm86 display.asm m1 ep db
asm86 ../shared/evqueue.asm m1 ep db
asm86 ../shared/int2.asm m1 ep db
asm86 ../shared/interrpt.asm m1 ep db
asm86 keypad.asm m1 ep db
asm86 ../shared/queue.asm m1 ep db
asm86 segtab14.asm m1 ep db
asm86 ../shared/serial.asm m1 ep db
asm86 serparse.asm m1 ep db
asm86 timer.asm m1 ep db

link86 rmtmain.obj, ../shared/chpslct.obj, converts.obj, display.obj, ../shared/evqueue.obj, ../shared/int2.obj TO tmp1.lnk
link86 ../shared/interrpt.obj, keypad.obj, ../shared/queue.obj, segtab14.obj, ../shared/serial.obj TO tmp2.lnk
link86 serparse.obj, timer.obj TO tmp3.lnk
link86 tmp1.lnk, tmp2.lnk, tmp3.lnk TO rmtmain.lnk
loc86 rmtmain.lnk NOIC AD(SM(CODE(4000H),DATA(400H), STACK(7000H)))