        NAME  SerialParser

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 Serial Parser                              ;
;                            Serial Parsing Functions                         ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains functions that are necessary to parse communication over
; serial. The functions included are:
;       ParseSerialInit       - Initializes the variables used when parsing
;                               the serial characters. (PUBLIC)
;       ParseSerialChar       - Parses a single serial character using a serial
;                               parser finite state machine. (PUBLIC)
;       GetSerialToken        - Gets the token type and value of a char. (PRIVATE)
;       GetStatus             - Gets the status of the RoboTrike and stores it
;                               in a buffer with speed, direction, and laser
;                               status. (PRIVATE)
;       GetError              - Gets an error from the motor side and enqueues it
;                               in the EventQueue. (PRIVATE)
;       SetError              - Sets ErrorVal to the appropriate value based on
;                               the argument passed. (PRIVATE)
;       DoNOP                 - Does nothing. (PRIVATE)
;       SerialStateTable      - Contains the states of the serial parser finite
;                               state machine. (PRIVATE)
;       SerialTokenTypeTable  - Contains the token types for each char. (PRIVATE)
;       SerialTokenValueTable - Contains the token values for each char. (PRIVATE)
;       SerialCommandTable    - Contains the label that calls the function that
;                               corresponds to the CommandFlag. (PRIVATE)
;
; Revision History:
;     12/5/15   Dennis Shim     initial revision
;     12/8/15   Dennis Shim     separated SetStatus and GetStatus and added more
;                               states (this is now a FSM)
;     12/9/15   Dennis Shim     fixed bug where high bit of TokenVal would be
;                               masked
;     12/9/15   Dennis Shim     updated to work using a shared folder


; local include files
$INCLUDE(../SHARED/GENERAL.INC) ;contains general constants and addresses
$INCLUDE(SERPARSE.INC)          ;contains constants for the serial parser
$INCLUDE(../SHARED/ERROR.INC)   ;contains error constants
$INCLUDE(EVENT.INC) 			;contains event constants

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE    SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DGROUP

; external action routines used by state machine
;   assumed to affect no registers
    EXTRN   UpdateStatus:NEAR       ;update motor speed/direction and laser status
    EXTRN   EnqueueEvent:NEAR       ;enqueues an event to the EventQueue


; ParseSerialInit
;
; Description:        Initializes the variables used for serial parsing. Sets
;                     CurrentState to STATE_START, ErrorVal to NO_ERROR,
;                     CommandFlag to COM_NONE, SignFlag to SIGN_NONE, and Num to
;                     0 (digits are added to Num later).
;
; Operational         
; Description:        Set CurrentState to STATE_START, ErrorVal to NO_ERROR,
;                     CommandFlag to COM_NONE, SignFlag to SIGN_NONE, and Num to
;                     0.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   CurrentState - DS - Current state the FSM is in after previous
;                                         iterations of ParseSerialChar. (w)
;                     ErrorVal - DS - NO_ERROR if there is no error, otherwise
;                                     there is an error identified by the code. (w)
;                     StatusIndex - DS - index of the StatusBuffer that the
;                                        state machine is currently writing. (w)
;                     MotorError - DS - value of the serial error passed. (w)
;
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

ParseSerialInit     PROC    NEAR
                    PUBLIC  ParseSerialInit

        MOV     CurrentState, ST_INITIAL    ;start at the initial state
        MOV     ErrorVal, NO_ERROR          ;no errors initially
        MOV     StatusIndex, 0              ;start at first StatusBuffer index
        MOV     MotorError, NO_ERROR        ;initially no MotorError
        RET

ParseSerialInit     ENDP

; ParseSerialChar
;
; Description:        This procedure parses a passed ASCII character. It uses a
;                     finite state machine to parse multiple characters in sequence
;                     in order to determine what command is being sent. The command
;                     S# sets the absolute speed, V# sets the relative speed, D#
;                     sests the direciton, T# sets the turret angle (not implemented)
;                     E# sets the turret elevation angle (not implemented), F turns
;                     the laser on, and O turns the laser off. # means that a valid
;                     signed number. Returns the value of the error.
;
; Operational         
; Description:        Set ErrorVal to ERROR_NONE. If the CurrentState is either the
;                     end or an error state, do nothing. Otherwise, get the
;                     TokenType and TokenVal by calling GetSerialToken. Then, call
;                     the function that corresponds to CurrentState and TokenType
;                     with the argument TokenVal. Return the error value.
;
; Arguments:          char - AL - ASCII character to be parsed
;
; Return Values:      ErrorVal - AX - ERROR_NONE if there is no error, otherwise 
;                                     there is an error identified by the code.
; Global Variables:   None.
; Shared Variables:   CurrentState - DS - Current state the FSM is in after previous
;                                         iterations of ParseSerialChar. (w)
;                     ErrorVal - DS - ERROR_NONE if there is no error, otherwise
;                                     there is an error identified by the code. (r/w)
;
; Local Variables:    TokenType - type of the token (char) passed
;                     TokenVal - value of the token (char) passed
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

ParseSerialChar     PROC    NEAR
                    PUBLIC  ParseSerialChar

        PUSH    BX                      ;preserve registers
        PUSH    CX
        PUSH    DX

        MOV     ErrorVal, NO_ERROR      ;no errors yet

CheckParseSerialReset:                  ;check if we are in an end or error state, 
                                        ;  if we are, then re-initialize the
                                        ;  parser
        CMP     CurrentState, ST_END    ;check if we are in the end state
        JE      ParseSerialReset        ;we are in end state, reset serial parser
        CMP     CurrentState, ST_ERROR  ;check if we are in the error state
        JE      ParseSerialReset        ;we are in error state, reset parser
        JMP     LookupSerialToken       ;not in end or error state, get the next
                                        ;  token and parse it

ParseSerialReset:                       ;reset by re-initializing
        CALL    ParseSerialInit
        ;JMP    LookupSerialToken       ;get the next token and parse it 

LookupSerialToken:                      ;get input for state machine
        CALL    GetSerialToken          ;get the token type and value
        MOV     DH, AH                  ;save the token type in DH
        MOV     CH, AL                  ;save the token value in CH

ComputeTransition:                      ;get which transition to do
        MOV     AL, NUM_TOKEN_TYPES     ;get the right row in the table
        MUL     CurrentState            ;AX is the start row index for CurrentState
        ADD     AL, DH                  ;get the actual transition index
        ADC     AH, 0                   ;propagate low byte carry into high byte
        IMUL    BX, AX, SIZE TRANSITION_ENTRY;convert index to table offset

PerformTransition:                      ;change the state to the next state
                                        ;  specified by the table
                                        ;  NOTE: must call the action after doing
                                        ;  the transition because of the case
                                        ;  where an error occurs in the action
                                        ;  (e.g. overflow)
        MOV     CL, CS:SerialStateTable[BX].NEXTSTATE
        MOV     CurrentState, CL
        
PerformAction:                          ;do the actions
        MOV     AL, CH                  ;get token value and pass as argument
        MOV     AH, DH                  ;also pass the token type as an argument
                                        ;  for certain functions (SetError)
        CALL    CS:SerialStateTable[BX].ACTION;do the action specified in table

ParseSerialCharEnd:
        MOV     AL, ErrorVal            ;return the error value in AX
        XOR     AH, AH                  ;extend AL to AX (clear AH)

        POP     DX                      ;restore registers
        POP     CX
        POP     BX

        RET

ParseSerialChar     ENDP

; GetSerialToken
;
; Description:        This procedure performs table lookups for the token type and
;                     value based on the character passed in.
;
; Operational         
; Description:        Lookup the TokenType from the TokenTypeTable using char as
;                     an index. Lookup the TokenValue from the TokenValueTable
;                     using char as an index. Return TokenType and TokenValue.
;
; Arguments:          char - AL - ASCII character to be parsed
;
; Return Values:      TokenType - AH - type of the token (char) passed
;                     TokenVal - AL - value of the token (char) passed
; Global Variables:   None.
; Shared Variables:   None.
; Local Variables:    TokenType - type of the token (char) passed
;                     TokenVal - value of the token (char) passed 
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  AX, flags.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision
;     12/9/15   Dennis Shim     fixed bug where high bit of TokenVal would be masked

GetSerialToken      PROC    NEAR

        PUSH    BX                      ;preserve registers

GetSerialTokenInit:
        MOV     AH, AL                  ;preserve the original TokenVal
        AND     AL, TOKEN_MASK          ;mask high bit of token

SerialTokenTypeLookup:                  ;get the token type
        MOV     BX, OFFSET(SerialTokenTypeTable);prepare to XLAT from
                                                ;  SerialTokenTypeTable
        XLAT    CS:SerialTokenTypeTable ;call XLAT with token type in AL
        XCHG    AH, AL                  ;token type in AH, token val in AL

GetSerialTokenEnd:
        POP     BX                      ;restore registers

        RET

GetSerialToken      ENDP

; GetStatus
;
; Description:        Gets the next byte of the status and puts it in the
;                     StatusBuffer. Increments the StatusIndex.
;
; Operational         
; Description:        Puts the received StatusByte in the StatusBuffer at the
;                     StatusIndex. Increment StatusIndex.
;
; Arguments:          StatusByte - AL - value of the status byte received
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   StatusBuffer - DS - buffer of length NUM_STATUS_BYTES that
;                                         holds motor speed, direction, and
;                                         laser status.
;                     StatusIndex - DS - index of the StatusBuffer that the
;                                        state machine is currently writing
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision
;     12/8/15   Dennis Shim     separated GetStatus and SetStatus

GetStatus       PROC    NEAR

        PUSH    SI                  ;preserve register

        MOV     SI, StatusIndex     ;index of the StatusBuffer to write to
        MOV     StatusBuffer[SI], AL;put the argument (status byte) in the buffer
        
        INC     StatusIndex         ;go to the next status index
        
GetStatusEnd:
        POP     SI                  ;restore register

        RET

GetStatus       ENDP

; SetStatus
;
; Description:        Call UpdateStatus with arguments speed, direction, and
;                     laser status.
;
; Operational         
; Description:        Put speed in AX, direction in BX, and laser status in CL.
;                     Call UpdateStatus.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   StatusBuffer - DS - buffer of length NUM_STATUS_BYTES that
;                                         holds motor speed, direction, and
;                                         laser status.
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  flags.
;
; Revision History:
;     12/8/15   Dennis Shim     separated GetStatus and SetStatus

SetStatus       PROC    NEAR

        PUSH    SI                  ;preserve register

        XOR     SI, SI              ;start at the beginning of the StatusBuffer
                                    ;put the speed in AX
        MOV     AH, StatusBuffer[SI];write the high byte first
        INC     SI
        MOV     AL, StatusBuffer[SI];then write the low byte
        INC     SI
                                    ;put the direction in BX
        MOV     BH, StatusBuffer[SI];write the high byte first
        INC     SI
        MOV     BL, StatusBuffer[SI];then write the low byte
        INC     SI
        
        MOV     CL, StatusBuffer[SI];put the laser status in CL
        CALL    UpdateStatus        ;now update the status of the RoboTrike
        
SetStatusEnd:
        POP     SI                  ;restore register

        RET

SetStatus       ENDP

; GetError
;
; Description:        Get the value of the error and puts it in the MotorError
;                     variable.
;
; Operational         
; Description:        Puts the argument into MotorError.
;
; Arguments:          ErrorByte - AL - value of the error byte received
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   MotorError - DS - value of the serial error passed
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision

GetError        PROC    NEAR

        MOV     MotorError, AL          ;store ErrorByte (argument) as the error
                                        ;  value from the motor side
        RET

GetError        ENDP

; EnqueueError
;
; Description:        Enqueues an error event with MotorError, the event received
;                     over serial
;
; Operational         
; Description:        Calls EnqueueEvent with MotorError in AL and EVENT_ERROR in
;                     AX
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   MotorError - DS - value of the serial error passed
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision

EnqueueError    PROC    NEAR

        PUSH    AX                      ;preserve register

        MOV     AL, MotorError          ;put MotorError as the argument for
                                        ;  EnqueueEvent
        OR      AL, MOTOR_ERROR_VAL     ;distinguish the error as being from
                                        ;  the motor side
        MOV     AH, EVENT_ERROR         ;specify that this is an EVENT_ERROR
        CALL    EnqueueEvent            ;call EnqueueEvent(EVENT_ERROR, MotorError)

        POP     AX                      ;restore register
        
        RET

EnqueueError    ENDP

; SetError
;
; Description:        Sets ErrorVal depending on which error was received. The
;                     error received is passed as an argument.
;
; Operational         
; Description:        Set ErrorVal to Error (passed argument).
;
; Arguments:          Error - AH - value of the error that occurred (range 2-127)
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   ErrorVal - DS - ERROR_NONE if there is no error, otherwise
;                                     there is an error identified by the code. (w)
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

SetError        PROC    NEAR

        PUSH    AX
        
        INC     AH                  ;increment Error to get the correct error value
                                    ;  (we do this because ERROR_NONE is zero,
                                    ;   and TokenValue is zero-indexed, so we
                                    ;   need to TokenValues to be one-indexed)
        MOV     ErrorVal, AH        ;set ErrorVal to the passed argument

        POP     AX
        RET

SetError        ENDP

; DoNOP
;
; Description:        Does nothing.
;
; Operational         
; Description:        Do nothing.
;
; Arguments:          None.
;
; Return Values:      None.
; Global Variables:   None.
; Shared Variables:   None.
; Local Variables:    None.
;
; Inputs:             None.
; Outputs:            None.
; Error Handling:     None.
; Algorithms:         None.
; Data Structures:    None.
; Limitations:        None.
; Known Bugs:         None.
; Special Notes:      None.
;
; Registers Changed:  None.
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

DoNOP           PROC    NEAR

        RET

DoNOP           ENDP

; SerialStateTable
;
; Description:      State transition table for the Serial Parser state machine.
;                   Each entry is a struc that contains the next state and the
;                   actions corresponding to that transition. The rows are
;                   associated with the current state and the column with the
;                   input type. Access the table by performing a table lookup
;                   using SerialStateTable[(CurrentState * NUM_TOKEN_TYPES +
;                                            TokenType) * SIZE TRANSITION_ENTRY]
;
; Revision History:
;     11/24/15  Dennis Shim     initial revision

TRANSITION_ENTRY        STRUC           ;structure used to define table
    NEXTSTATE   DB      ?               ;the next state for the transition
    ACTION      DW      ?               ;action for the transition
TRANSITION_ENTRY      ENDS


;define a macro to make table a little more readable
;macro just does an offset of the action routine entries to build the STRUC
%*DEFINE(TRANSITION(nxtst, act))  (
    TRANSITION_ENTRY< %nxtst, OFFSET(%act) >
)


SerialStateTable    LABEL   TRANSITION_ENTRY

    ;Current State = ST_INITIAL                         Input Token Type
    %TRANSITION(ST_GET_STATUS, DoNOP)                   ;TOKEN_GET_STATUS
    %TRANSITION(ST_GET_ERROR, DoNOP)                    ;TOKEN_GET_ERROR
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_EOS
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_GET_STATUS                      Input Token Type
    %TRANSITION(ST_GET_STATUS_B0, GetStatus)            ;TOKEN_GET_STATUS
    %TRANSITION(ST_GET_STATUS_B0, GetStatus)            ;TOKEN_GET_ERROR
    %TRANSITION(ST_GET_STATUS_B0, GetStatus)            ;TOKEN_EOS
    %TRANSITION(ST_GET_STATUS_B0, GetStatus)            ;TOKEN_OTHER
    
    ;Current State = ST_GET_ERROR                       Input Token Type
    %TRANSITION(ST_ERROR_RECEIVED, GetError)            ;TOKEN_GET_STATUS
    %TRANSITION(ST_ERROR_RECEIVED, GetError)            ;TOKEN_GET_ERROR
    %TRANSITION(ST_ERROR_RECEIVED, GetError)            ;TOKEN_EOS
    %TRANSITION(ST_ERROR_RECEIVED, GetError)            ;TOKEN_OTHER
    
    ;Current State = ST_GET_STATUS_B0                   Input Token Type
    %TRANSITION(ST_GET_STATUS_B1, GetStatus)            ;TOKEN_GET_STATUS
    %TRANSITION(ST_GET_STATUS_B1, GetStatus)            ;TOKEN_GET_ERROR
    %TRANSITION(ST_GET_STATUS_B1, GetStatus)            ;TOKEN_EOS
    %TRANSITION(ST_GET_STATUS_B1, GetStatus)            ;TOKEN_OTHER
    
    ;Current State = ST_GET_STATUS_B1                   Input Token Type
    %TRANSITION(ST_GET_STATUS_B2, GetStatus)            ;TOKEN_GET_STATUS
    %TRANSITION(ST_GET_STATUS_B2, GetStatus)            ;TOKEN_GET_ERROR
    %TRANSITION(ST_GET_STATUS_B2, GetStatus)            ;TOKEN_EOS
    %TRANSITION(ST_GET_STATUS_B2, GetStatus)            ;TOKEN_OTHER
    
    ;Current State = ST_GET_STATUS_B2                   Input Token Type
    %TRANSITION(ST_GET_STATUS_B3, GetStatus)            ;TOKEN_GET_STATUS
    %TRANSITION(ST_GET_STATUS_B3, GetStatus)            ;TOKEN_GET_ERROR
    %TRANSITION(ST_GET_STATUS_B3, GetStatus)            ;TOKEN_EOS
    %TRANSITION(ST_GET_STATUS_B3, GetStatus)            ;TOKEN_OTHER
    
    ;Current State = ST_GET_STATUS_B3                   Input Token Type
    %TRANSITION(ST_GET_STATUS_B4, GetStatus)            ;TOKEN_GET_STATUS
    %TRANSITION(ST_GET_STATUS_B4, GetStatus)            ;TOKEN_GET_ERROR
    %TRANSITION(ST_GET_STATUS_B4, GetStatus)            ;TOKEN_EOS
    %TRANSITION(ST_GET_STATUS_B4, GetStatus)            ;TOKEN_OTHER
    
    ;Current State = ST_GET_STATUS_B4                   Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_GET_STATUS
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_GET_ERROR
    %TRANSITION(ST_END, SetStatus)                      ;TOKEN_EOS
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_ERROR_RECEIVED                  Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_GET_STATUS
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_GET_ERROR
    %TRANSITION(ST_END, EnqueueError)                   ;TOKEN_EOS
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_ERROR                           Input Token Type
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_GET_STATUS
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_GET_ERROR
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_EOS
    %TRANSITION(ST_ERROR, SetError)                     ;TOKEN_OTHER

    ;Current State = ST_END                             Input Token Type
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_GET_STATUS
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_GET_ERROR
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_EOS
    %TRANSITION(ST_END, DoNOP)                          ;TOKEN_OTHER


; Serial Token Tables
;
; Description:      This creates the tables of token types and token values
;                   used by the serial parser. Each entry corresponds to the
;                   token type and the token value for a character. Macros are
;                   used to actually build the tables, SerialTokenTypeTable for
;                   token types and SerialTokenValueTable for token values. Each
;                   table can be accessed using an XLAT with the char in AL.
;
; Revision History:
;     12/2/15   Dennis Shim     initial revision

%*DEFINE(TABLE)  (
        %TABENT(TOKEN_OTHER, 0)         ;<null>
        %TABENT(TOKEN_OTHER, 1)         ;SOH
        %TABENT(TOKEN_OTHER, 2)         ;STX
        %TABENT(TOKEN_OTHER, 3)         ;ETX
        %TABENT(TOKEN_OTHER, 4)         ;EOT
        %TABENT(TOKEN_OTHER, 5)         ;ENQ
        %TABENT(TOKEN_OTHER, 6)         ;ACK
        %TABENT(TOKEN_OTHER, 7)         ;BEL
        %TABENT(TOKEN_OTHER, 8)         ;backspace
        %TABENT(TOKEN_OTHER, 9)         ;TAB (white space)
        %TABENT(TOKEN_OTHER, 10)        ;new line
        %TABENT(TOKEN_OTHER, 11)        ;vertical tab
        %TABENT(TOKEN_OTHER, 12)        ;form feed
        %TABENT(TOKEN_EOS, 13)          ;carriage return
        %TABENT(TOKEN_OTHER, 14)        ;SO
        %TABENT(TOKEN_OTHER, 15)        ;SI
        %TABENT(TOKEN_OTHER, 16)        ;DLE
        %TABENT(TOKEN_OTHER, 17)        ;DC1
        %TABENT(TOKEN_OTHER, 18)        ;DC2
        %TABENT(TOKEN_OTHER, 19)        ;DC3
        %TABENT(TOKEN_OTHER, 20)        ;DC4
        %TABENT(TOKEN_OTHER, 21)        ;NAK
        %TABENT(TOKEN_OTHER, 22)        ;SYN
        %TABENT(TOKEN_OTHER, 23)        ;ETB
        %TABENT(TOKEN_OTHER, 24)        ;CAN
        %TABENT(TOKEN_OTHER, 25)        ;EM
        %TABENT(TOKEN_OTHER, 26)        ;SUB
        %TABENT(TOKEN_OTHER, 27)        ;escape
        %TABENT(TOKEN_OTHER, 28)        ;FS
        %TABENT(TOKEN_OTHER, 29)        ;GS
        %TABENT(TOKEN_OTHER, 30)        ;AS
        %TABENT(TOKEN_OTHER, 31)        ;US
        %TABENT(TOKEN_OTHER, ' ')       ;space (white space)
        %TABENT(TOKEN_OTHER, '!')       ;!
        %TABENT(TOKEN_OTHER, '"')       ;"
        %TABENT(TOKEN_OTHER, '#')       ;#
        %TABENT(TOKEN_OTHER, '$')       ;$
        %TABENT(TOKEN_OTHER, 37)        ;percent
        %TABENT(TOKEN_OTHER, '&')       ;&
        %TABENT(TOKEN_OTHER, 39)        ;'
        %TABENT(TOKEN_OTHER, 40)        ;open paren
        %TABENT(TOKEN_OTHER, 41)        ;close paren
        %TABENT(TOKEN_OTHER, '*')       ;*
        %TABENT(TOKEN_OTHER, '-')       ;+
        %TABENT(TOKEN_OTHER, 44)        ;,
        %TABENT(TOKEN_OTHER, '+')       ;-
        %TABENT(TOKEN_OTHER, '.')       ;.
        %TABENT(TOKEN_OTHER, '/')       ;/
        %TABENT(TOKEN_OTHER, '0')       ;0
        %TABENT(TOKEN_OTHER, '1')       ;1
        %TABENT(TOKEN_OTHER, '2')       ;2
        %TABENT(TOKEN_OTHER, '3')       ;3
        %TABENT(TOKEN_OTHER, '4')       ;4
        %TABENT(TOKEN_OTHER, '5')       ;5
        %TABENT(TOKEN_OTHER, '6')       ;6
        %TABENT(TOKEN_OTHER, '7')       ;7
        %TABENT(TOKEN_OTHER, '8')       ;8
        %TABENT(TOKEN_OTHER, '9')       ;9
        %TABENT(TOKEN_OTHER, ':')       ;:
        %TABENT(TOKEN_OTHER, ';')       ;;
        %TABENT(TOKEN_OTHER, '<')       ;<
        %TABENT(TOKEN_OTHER, '=')       ;=
        %TABENT(TOKEN_OTHER, '>')       ;>
        %TABENT(TOKEN_OTHER, '?')       ;?
        %TABENT(TOKEN_OTHER, '@')       ;@
        %TABENT(TOKEN_OTHER, 'A')       ;A
        %TABENT(TOKEN_OTHER, 'B')       ;B
        %TABENT(TOKEN_OTHER, 'C')       ;C
        %TABENT(TOKEN_OTHER, 'D')       ;D
        %TABENT(TOKEN_GET_ERROR, 'E')   ;E (get error state)
        %TABENT(TOKEN_OTHER, 'F')       ;F
        %TABENT(TOKEN_OTHER, 'G')       ;G
        %TABENT(TOKEN_OTHER, 'H')       ;H
        %TABENT(TOKEN_OTHER, 'I')       ;I
        %TABENT(TOKEN_OTHER, 'J')       ;J
        %TABENT(TOKEN_OTHER, 'K')       ;K
        %TABENT(TOKEN_OTHER, 'L')       ;L
        %TABENT(TOKEN_OTHER, 'M')       ;M
        %TABENT(TOKEN_OTHER, 'N')       ;N
        %TABENT(TOKEN_OTHER, 'O')       ;O 
        %TABENT(TOKEN_OTHER, 'P')       ;P
        %TABENT(TOKEN_OTHER, 'Q')       ;Q
        %TABENT(TOKEN_OTHER, 'R')       ;R
        %TABENT(TOKEN_GET_STATUS, 'S')  ;S (get status state)
        %TABENT(TOKEN_OTHER, 'T')       ;T
        %TABENT(TOKEN_OTHER, 'U')       ;U
        %TABENT(TOKEN_OTHER, 'V')       ;V
        %TABENT(TOKEN_OTHER, 'W')       ;W
        %TABENT(TOKEN_OTHER, 'X')       ;X
        %TABENT(TOKEN_OTHER, 'Y')       ;Y
        %TABENT(TOKEN_OTHER, 'Z')       ;Z
        %TABENT(TOKEN_OTHER, '[')       ;[
        %TABENT(TOKEN_OTHER, '\')       ;\
        %TABENT(TOKEN_OTHER, ']')       ;]
        %TABENT(TOKEN_OTHER, '^')       ;^
        %TABENT(TOKEN_OTHER, '_')       ;_
        %TABENT(TOKEN_OTHER, '`')       ;`
        %TABENT(TOKEN_OTHER, 'a')       ;a
        %TABENT(TOKEN_OTHER, 'b')       ;b
        %TABENT(TOKEN_OTHER, 'c')       ;c
        %TABENT(TOKEN_OTHER, 'd')       ;d
        %TABENT(TOKEN_GET_ERROR, 'e')   ;e (get error state)
        %TABENT(TOKEN_OTHER, 'f')       ;f
        %TABENT(TOKEN_OTHER, 'g')       ;g
        %TABENT(TOKEN_OTHER, 'h')       ;h
        %TABENT(TOKEN_OTHER, 'i')       ;i
        %TABENT(TOKEN_OTHER, 'j')       ;j
        %TABENT(TOKEN_OTHER, 'k')       ;k
        %TABENT(TOKEN_OTHER, 'l')       ;l
        %TABENT(TOKEN_OTHER, 'm')       ;m
        %TABENT(TOKEN_OTHER, 'n')       ;n
        %TABENT(TOKEN_OTHER, 'o')       ;o
        %TABENT(TOKEN_OTHER, 'p')       ;p
        %TABENT(TOKEN_OTHER, 'q')       ;q
        %TABENT(TOKEN_OTHER, 'r')       ;r
        %TABENT(TOKEN_GET_STATUS, 's')  ;s (get status state)
        %TABENT(TOKEN_OTHER, 't')       ;t
        %TABENT(TOKEN_OTHER, 'u')       ;u
        %TABENT(TOKEN_OTHER, 'v')       ;v
        %TABENT(TOKEN_OTHER, 'w')       ;w
        %TABENT(TOKEN_OTHER, 'x')       ;x
        %TABENT(TOKEN_OTHER, 'y')       ;y
        %TABENT(TOKEN_OTHER, 'z')       ;z
        %TABENT(TOKEN_OTHER, '{')       ;{
        %TABENT(TOKEN_OTHER, '|')       ;|
        %TABENT(TOKEN_OTHER, '}')       ;}
        %TABENT(TOKEN_OTHER, '~')       ;~
        %TABENT(TOKEN_OTHER, 127)       ;rubout
)

; token type table - uses first byte of macro table entry
%*DEFINE(TABENT(tokentype, tokenvalue))  (
        DB      %tokentype
)

SerialTokenTypeTable    LABEL   BYTE
        %TABLE

; token value table - uses second byte of macro table entry
%*DEFINE(TABENT(tokentype, tokenvalue))  (
        DB      %tokenvalue
)

SerialTokenValueTable   LABEL   BYTE
        %TABLE

CODE    ENDS

; the data segment

DATA    SEGMENT PUBLIC  'DATA'

CurrentState        DB          ?   ;current state the serial FSM is in
ErrorVal            DB          ?   ;value of the error (ERROR_NONE if no error)
StatusBuffer        DB      (NUM_STATUS_BYTES)  DUP     (?)   ;buffer of status values
StatusIndex         DW          ?   ;index of StatusBuffer being written
MotorError          DB          ?   ;value of the received error over serial

DATA    ENDS


        END