         NAME    DISPLAY

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   Display                                  ;
;                               Display Functions                            ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file contains functions that allow a user to output strings to the display
; using multiplexing. The functions included are:
;       DisplayInit             - initializes the display by making the digits
;                                 blank and setting the starting digit. (PUBLIC)
;       Display                 - Takes a string and writes its segments to the
;                                 segment buffer. (PUBLIC)
;       DisplayNum              - Takes a decimal number (signed) and writes its
;                                 segments to the segment buffer. (PUBLIC)
;       DisplayHex              - Takes a hexadecimal number (unsigned) and writes
;                                 its segments to the segment buffer. (PUBLIC)
;       MuxDisplay              - Multiplexes the display by displaying a single
;                                 segment at a time and incrementing the digit.
;                                 (PUBLIC)
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision
;     12/9/15   Dennis Shim      updated to work using a shared folder


; local include files
$INCLUDE(../SHARED/GENERAL.INC) ;contains general constants and addresses
$INCLUDE(DISPLAY.INC)           ; contains constants and addresses necessary to
                                ; display ASCII strings on the LED display

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE    SEGMENT PUBLIC 'CODE'



        ASSUME  CS:CGROUP, DS:DGROUP
		
		
		; external functions/tables
        EXTRN   Dec2String:NEAR         ; converts a decimal to a string
        EXTRN   Hex2String:NEAR         ; converts a hexadecimal to a string
        EXTRN   ASCIISegTable:BYTE      ; contains the segments for ASCII chars

; DisplayInit
;
; Description:        This function initializes the variables that the multiplexer
;                     uses to display the strings. The buffer of string segments
;                     is initialized to all blank segments, and the starting
;                     multiplexer index is initialized to be NUM_DIGITS - 1
;
; Operational         
; Description:        The buffer (an array of size BUFFER_SIZE bytes) is
;                     initialized and blanked, so nothing is initially displayed.
;                     The starting multiplexer index is also set to NUM_DIGITS - 1
;                     (the last digit on the display).
;
; Arguments:          none.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   SegBuffer - DS - a buffer containing the segments of the
;                                      strings to be displayed. (CHANGED)
;                     CurrDigit - DS - index of the segment in the buffer to be 
;                                      displayed. (CHANGED)
;
; Local Variables:    index (DI) - index of the segment buffer while looping.
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    none.
; Limitations:        none.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  none.
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision

DisplayInit     PROC        NEAR
                PUBLIC      DisplayInit

                            ; initialize the current digit and buffer loop index
        PUSH    DI          ; preserve register
		
        MOV     CurrDigit, NUM_DIGITS - 1; start on last segment digit
		
        MOV     DI, 0       ; start the index (DI) of buffer at 0

ClearBufferLoop:             ; check if we have finished clearing the buffer
        CMP     DI, BUFFER_SIZE         ; check if we have reached the end of the buffer
        JGE     DisplayInitEnd          ; if so, we have finished clearing the buffer
        ;JL     ClearBufferLoopBody     ; otherwise, keep clearing the buffer

ClearBufferLoopBody:        ; clear one segment of the buffer
        MOV     SegBuffer[DI], BLANKseg ; put a blank segment in the buffer
        INC     DI          ; increment index to the next segment
        JMP     ClearBufferLoop  ; check loop condition

DisplayInitEnd:
        POP 	DI          ; restore register
        RET

DisplayInit     ENDP


; Display
;
; Description:        The function is passed a null-terminated string to output
;                     to the LED display. The string is passed by reference in
;                     ES:SI. The string is in ES so the string is in the code
;                     segment and can thus be constant (as opposed to in DS, where
;                     it would be changed). The first NUM_DIGITS characters of
;                     the string are displayed using the fourteen-segment display.
;                     If the length of the string is less than NUM_DIGITS, the
;                     string will appear left justified, and the rest of the LEDs
;                     will be blanked on the right.
;
; Operational         
; Description:        Set up to loop through the string and write segments to the
;                     SegBuffer. Loop until the index reaches BUFFER_SIZE or
;                     the end of the string (null character). In the loop, get
;                     the correct segment from the ASCIISegTable and write it to
;                     the SegBuffer. Get the segment by adding the address of the
;                     table and twice the ASCII character value (because the table
;                     is a word table), and looking up the segment pattern. After
;                     this loop is finished, loop through the rest of the SegBuffer
;                     and add blank segments.
;
; Arguments:          str - ES:SI - address of the string to be displayed.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   SegBuffer - DS - a buffer containing the segments of the
;                                      strings to be displayed. (CHANGED)
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            none.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    none.
; Limitations:        none.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  none.
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision
;     10/30/15  Dennis Shim      updated comments and fixed code

Display         PROC        NEAR
                PUBLIC      Display

DisplayLoopInit:                        ; prepare registers for the loop
        PUSHA                           ; preserve the registers
        MOV     BX, 0                   ; start the index (BX) of string/buffer at 0
        ;JMP    DisplayLoop             ; begin to loop through the string

DisplayLoop:                            ; check if we should stop writing chars
        CMP     BX, BUFFER_SIZE * 2     ; check if we have reached the end of the buffer
        JGE     DisplayEnd              ; reached the end, stop writing characters
        CMP     BYTE PTR ES:[SI + BX], ASCII_NULL; check if the character == NULL
        JE      BlankBufferLoop         ; if so, we have reached the end of the
                                        ;   string, now blank the rest of the buffer
        ;JNE    DisplayLoopBody         ; otherwise, we still have to put characters
                                        ;   in the buffer 

DisplayLoopBody:                        ; loop through the string and put the
                                        ;   segment corresponding to the character
                                        ;   in the buffer
        MOV     AL, BYTE PTR ES:[SI + BX]; get the ASCII character from the string
		MOV 	CX, BX				    ; temporarily store the index in CX
                                        ;   we have to use BX when performing
                                        ;   addressing operations
	
		MOV 	AH, 0 					 
        SHL     AL, 1                   ; multiply the ASCII value by two to get
										; 	the correct segment because we have
                                        ;   a table of words
        MOV     BX, ASCII_NULL          ; the null character is the first entry
                                        ;   in the table, so we index relative to it
        ADD 	BX, AX 					; get the index for the correct segment,
										; 	which is a word, so we multiply
										; 	the ASCII value by two.
		MOV 	AX, WORD PTR CS:ASCIISegTable[BX]; get the segment from the table

		MOV 	BX, CX 					; retrieve the index from CX
		SHL 	BX, 1					; double index to write to the correct
                                        ;   part of SegBuffer
        MOV     SegBuffer[BX], AX       ; put the segment pattern in the segment
                                        ;   buffer
		MOV 	BX, CX 					; retrieve the index from CX
        INC     BX                      ; increment index to the next character
        JMP     DisplayLoop             ; check the loop conditions again

BlankBufferLoop:                        ; check if we have reached the end of buffer
        CMP     BX, BUFFER_SIZE         ; check if the index reached the buffer size
        JGE     DisplayEnd              ; if so, we are done with the buffer
        ;JL     BlankBufferLoopBody     ; otherwise, continue adding blanks

BlankBufferLoopBody:                    ; put a blank in the buffer
		SHL 	BX, 1					; multiply index by 2 to get correct
                                        ;   index for word array
        MOV     SegBuffer[BX], BLANKseg ; put a blank segment in the buffer
		SHR  	BX, 1					; divide corrected index by 2 to get
                                        ;   original index
        INC     BX                      ; increment the buffer index
        JMP     BlankBufferLoop         ; check the loop condition again

DisplayEnd:
        POPA                            ; restore the registers
        RET

Display         ENDP


; DisplayNum
;
; Description:        The function is passed a 16-bit signed value to output in
;                     decimal to the LED display. The string will not be zero padded
;                     with at most five digits plus sign (at most 6 characters),
;                     which is what Dec2String returns.
;
; Operational         
; Description:        The function Dec2String is called with the number to be
;                     displayed in AX and the string address in SI. SI contains
;                     the address of StringBuffer, where the string is. This converts
;                     the number to a decimal string at address DS:SI. Then, the
;                     Display function is called using the string stored at the
;                     address, which displays the string on the LED display. 
;
; Arguments:          n - AX - 16-bit signed value to be displayed.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   SegBuffer - DS - a buffer containing the segments of the
;                                      strings to be displayed (CHANGED)
;                     StringBuffer - DS - a buffer containing a string that is
;                                         a number converted to ASCII. (CHANGED)
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            The 16-bit signed value in AX will be displayed on the LED
;                     display in decimal.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    none.
; Limitations:        none.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  flags, ES.
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision

DisplayNum      PROC        NEAR
                PUBLIC      DisplayNum

ConvertNum:
        PUSH    SI                  ; preserve register
		PUSH 	BX

        MOV     BX, DS              ; move the data segment address into the 
        MOV     ES, BX              ;   extra segment to get the string in the
                                    ;   correct location (ES:SI)
        LEA     SI, StringBuffer    ; put the address of StringBuffer in SI to 
                                    ;   pass as an argument to Dec2String
        CALL    Dec2String          ; call Dec2String with arguments in AX, SI
                                    ; StringBuffer should now have the string
        CALL    Display             ; call Display with address in SI
                                    ; SegBuffer should now have the number
        POP     BX                  ; restore registers
		POP 	SI
        RET

DisplayNum      ENDP


; DisplayHex
;
; Description:        The function is passed a 16-bit signed value to output in
;                     decimal to the LED display. The string will not be zero padded
;                     with at most four digits (at most 4 characters), which is
;                     what Hex2String returns.
;
; Operational         
; Description:        The function Hex2String is called with the number to be
;                     displayed in AX and the string address in SI. SI contains
;                     the address of StringBuffer, where the string is. This converts
;                     the number to a hexadecimal string at address ES:SI. Then, the
;                     Display function is called using the string stored at the
;                     address, which displays the string on the LED display. 
;
; Arguments:          n - AX - 16-bit unsigned value to be displayed.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   SegBuffer - DS - a buffer containing the segments of the 
;                                      strings to be displayed. (CHANGED)
;                     StringBuffer - DS - a buffer containing a string that is
;                                         a number converted to ASCII. (CHANGED)
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            The 16-bit signed value in AX will be displayed on the LED
;                     display in hexadecimal.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    none.
; Limitations:        none.
; Known Bugs:         none.
; Special Notes:      none.
; 
; Registers Changed:  none.
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision

DisplayHex      PROC        NEAR
                PUBLIC      DisplayHex

ConvertHex:
        PUSH    SI                  ; preserve registers
		PUSH 	BX

        MOV     BX, DS              ; move the data segment address into the 
        MOV     ES, BX              ;   extra segment to get the string in the
                                    ;   correct location (ES:SI)
        LEA     SI, StringBuffer    ; put the address of StringBuffer in SI to 
                                    ;   pass as an argument to Hex2String
        CALL    Hex2String          ; call Dec2String with arguments in AX, SI
                                    ; StringBuffer should now have the string
        CALL    Display             ; call Display with address in SI
                                    ; SegBuffer should now have the number
        POP     BX                  ; restore register
		POP 	SI
        RET

DisplayHex      ENDP


; MuxDisplay
;
; Description:        The function displays a segment pattern on the correct digit
;                     on the LED display. The function multiplexes the display at
;                     a frequeuncy of 1 kHz. The multiplexing loops through each
;                     digit and wraps back to NUM_DIGITS - 1. It multiplexes from
;                     right to left (CurrDigit goes from high to low).
;
; Operational         
; Description:        A segment buffer pattern is outputted to the correct
;                     LED display. The pattern and display are determined by the
;                     CurrDigit, which is decremented after the segment is
;                     displayed. If the CurrDigit is too low (less than zero),
;                     then it wraps to the NUM_DIGITS - 1.
;
; Arguments:          none.
;
; Return Values:      none.
; Global Variables:   none.
; Shared Variables:   SegBuffer - DS - a buffer containing the segments of the 
;                                      strings to be displayed. (CHANGED)
;                     CurrDigit - DS - index of the segment in the buffer to be
;                                      displayed. (CHANGED)
;
; Local Variables:    none.
;
; Inputs:             none.
; Outputs:            A character is displayed on the LED display.
; Error Handling:     none.
; Algorithms:         none.
; Data Structures:    none.
; Limitations:        none.
; Known Bugs:         none.
; Special Notes:      none.
; Revision History:   10/26/15 - Program outlined
; 
; Registers Changed:  none.
;
; Revision History:
;     10/29/15  Dennis Shim      initial revision

MuxDisplay      PROC        NEAR
                PUBLIC      MuxDisplay

DisplayUpdate:
        PUSH    DX                      ; preserve registers
        PUSH    BX
        PUSHF
		
        MOV     BX, CurrDigit			; get the digit to compute SegBuffer index
		SHL 	BX, 1					; multiply the index by 2 to get the correct
                                        ;   index, as SegBuffer is a word array
		MOV     AX, SegBuffer[BX]		; get the segment we want
		
		XCHG 	AL, AH 					; write the high bit out first
		
		MOV     DX, LED_14ADDR          ; get the display address
        ADD     DX, CurrDigit           ; offset the address by digit
        OUT     DX, AL                  ; write the upper part of the segment to
                                        ;   the display
		
		XCHG 	AL, AH					; now write out the low bit
		
        MOV     DX, LED_ADDR            ; get the display address
        ADD     DX, CurrDigit           ; offset the address by digit
        OUT     DX, AL                  ; write the lower part of the segment to
                                        ;   the display

IncrementDigit: 
        DEC     CurrDigit               ; update the current digit
        CMP     CurrDigit, 0            ; check if we should wrap
        JGE     EndMuxDisplay           ; digit not too low, skip wrap
		;JL     WrapDigit               ; digit too low, wrap

WrapDigit:                              ; wrap the digit to the highest index
        MOV     CurrDigit, NUM_DIGITS - 1
        ;JMP    EndMuxDisplay           ; digit is displayed, we are done

EndMuxDisplay:                          ; digit has been displayed
        POPF                            ; restore registers
        POP     BX
        POP     DX                      
        RET

MuxDisplay      ENDP


CODE    ENDS

;the data segment

DATA    SEGMENT PUBLIC  'DATA'

CurrDigit           DW      ?           ; the current digit number
StringBuffer        DB      (BUFFER_SIZE)   DUP     (?) ; buffer containing the string
SegBuffer           DW      (BUFFER_SIZE)   DUP     (?) ; buffer containing the segments

DATA    ENDS

        END
